\section{Similarity Search with ULISSE}
\label{sec:ulisseQuery}

In this section, we present the building blocks of the similarity search algorithms we developed for the \textit{ULISSE} index. 



\subsection{Lower Bounding Euclidean Distance}

The iSAX representation allows the definition of a distance function, which lower bounds the true Euclidean~\cite{shieh2008sax}. 
This function compares the $PAA$ coefficients of the first data series, against the iSAX breakpoints (values) that delimit the symbol regions of the second data series. 

Let $\beta_{u}(S)$ and $\beta_{l}(S)$ be the breakpoints of the iSAX symbol $S$.
We can compute the distance between a $PAA$ coefficient and an iSAX region using:



\begin{equation}
\begin{gathered}
{\small distLB(PAA(D)_{i},iSAX(D')_{i}) = } \\
\begin{cases}
\scriptstyle (\beta_{u}(iSAX(D')_{i}) - PAA(D)_{i})^2&if \scriptstyle \beta_{u} (iSAX(D')_{i}) < PAA(D)_{i} \\
\scriptstyle (\beta_{l}(iSAX(D')_{i}) - PAA(D)_{i})^2&if \scriptstyle \beta_{l}(iSAX(D')_{i})  > PAA(D)_{i} \\
\scriptstyle 0 & {\footnotesize \text{otherwise.}}
\end{cases}	
\end{gathered}
\end{equation}

In turn, the lower bounding distance between two equi-length series $D$,$D'$, represented by $w$ PAA segments and $w$ $iSAX$ symbols, respectively, is defined as:

\begin{equation}
\small
 \label{mindistPAAiSAX}
\begin{gathered}
\qquad mindist_{PAA\_iSAX}(PAA(D),iSAX(D')) = \\   \sqrt{\frac{|D|}{w}} \sqrt{\sum_{i=1}^{w} distLB(PAA(D)_{i},iSAX(D')_{i})}.
\end{gathered}
\end{equation}

We rely on the following proposition~\cite{Lin2007}:

\begin{proposition}
Given two data series $D,D'$, where {\small $|D|=|D'|$,	 $mindist_{PAA\_iSAX}(PAA(D),iSAX(D')) \le ED(D,D')$}. 
\end{proposition}

Since our index contains Envelope representations, we need to adapt Equation~\ref{mindistPAAiSAX}, in order to lower bound the distances between a data series $Q$, which we call query, and a set of subsequences, whose iSAX symbols are described by the Envelope\\ $uENV_{paaENV_{[D,\ell_{min},\ell_{max},a,\gamma,s]}}=[iSAX(L),iSAX(U)]$. 

Therefore, given w, the number of PAA coefficients of $Q$, that are computed using the Envelope PAA segment length $s$ on the longest multiple prefix, we define the following function:

\begin{equation}
\begin{gathered} 
\label{mindistPAAuENV}
\qquad {\small mindist_{ULiSSE}(PAA(Q),uENV_{paaENV_{...}})=}\\
\small \sqrt{s} \small \sqrt{\sum_{i=1}^{w} \begin{cases}
\scriptstyle (PAA(Q)_{i}-\beta_{u}(iSAX(U)_{i}))^2,& \scriptstyle if (*)\\
\scriptstyle (PAA(Q)_{i}-\beta_{u}(iSAX(L)_{i}))^2,& \scriptstyle if (**)\\
\scriptstyle 0 &  {\footnotesize \text{otherwise.}}
	\end{cases}}\\
(*) \scriptstyle \beta_{u} (iSAX(U)_{i}) < PAA(Q)_{i}\\
(**) \scriptstyle \beta_{l} (iSAX(L)_{i}) > PAA(Q)_{i}
\end{gathered} 
\end{equation}


\begin{figure}[tb]
	\centering
	\includegraphics[trim={0cm 15.5cm 20cm 3cm},scale=1]{Chap1_ULISSE/figures/LB_computation_ED.pdf}
	\caption{Given the PAA representation of a query Q (\textit{a}) and $uENV_{ paaENV_{[D,\ell_{min},\ell_{max},a,\gamma,s]}}$ (\textit{b}) we compute their $mindist_{ULiSSE}$. The $iSAX$ space is delimited with dashed lines and the relative breakpoints $\beta_{i}$.}
	\label{ExLBcomp}
\end{figure}

In Figure~\ref{ExLBcomp}, we report an example of $mindist_{ULiSSE}$ computation between a query Q, represented by its PAA coefficients, and an Envelope in the iSAX space.

\begin{proposition}
Given two data series
{\small  $Q$,$D$,\\$mindist_{ULiSSE}(PAA(Q),uENV_{paaENV_{[D,\ell_{min},\ell_{max},a,\gamma,s]}})\le ED(Q,D_{i,|Q|})$, for each $i$ such that $a \le i \le a+\gamma+1$ and $|D|-(i-1) \ge \ell{min}$
}. 
\end{proposition}
 
\begin{proof}(sketch)
We may have two cases, when $mindist_{ULiSSE}$ is equal to zero, the proposition clearly holds, since Euclidean distance is non negative. 
On the other hand, the function yields values greater than zero, if one of the first two branches is true. 
Let consider the first (the second is symmetric). 
If we denote with $D''$ the subsequence in $D$, such that $ \beta_{l} (iSAX(U)_{i}) \le PAA(D'')_{i} \le \beta_{u} (iSAX(U)_{i})$, we know that the upper breakpoint of the $i^{th}$ iSAX symbol, of each subsequence in $D$, which is represented by the Envelope, must be less or equal than $\beta_{u}(iSAX(U)_{i})$. 
It follows that, for this case, Equation~\ref{mindistPAAuENV} is equivalent to\\$distLB(PAA(Q)_{i},iSAX(D'')_{i})$, which yields the shortest lower bounding distance between the $i^{th}$ segment of points in $D$ and $Q$. 
\end{proof}

\subsection{Lower Bounding Dynamic Time Warping}

\begin{figure}[tb]
	\centering
	\includegraphics[trim={0cm 4cm 18cm 3cm},scale=0.7]{Chap1_ULISSE/figures/LB_computation_ex.pdf}
	\caption{\textit{(a)} DTW Envelope ($L^{DTW}$, $U^{DTW}$) of a series $D$. \textit{(b)} $LB_{Keogh}$ distance between DTW Envelope and $D'$. \textit{(c)} $LB_{PAL}$ between the DTW Envelope of $Q$ (prefix of $D$) and the \textit{ULISSE} Envelope of $D'$. The horizontal dashed lines delimit the iSAX breakpoints space.}
	\label{lowerboundingDTW}
\end{figure}


\commentRed{
	
    We present here, how to lower bound the true DTW distance between two data series. In their work, Keogh et al.~\cite{DBLP:journals/kais/KeoghR05} introduced the $LB_{Keogh}$ function, which provides a measure that is always smaller or equal than the true DTW, between two equi-length series.
	To compute this measure, we need to account the valid warping alignments of two data series. Recall that the indexes of a valid path are confined in the Sakoa-Chiba band, where they are at most $r$ points far from the diagonal (Euclidean Distance alignment). Given a data series $D$, we can build an envelope we denote by $dtwENV_{r}(D)$. It is composed by two data series: $L^{DTW}$ and $U^{UDTW}$, which delimit the space generated by the points of $D$ that have indexes in the valid warping paths, constrained by the window $r$.
	Therefore, the $i^{th}$ point of the two envelope sequences are computed as follow: $L_{i}^{DTW} = min(D_{(i-r,2r+1)})$ and $U_{i}^{DTW} = max(D_{(i-r,2r+1)})$. Intuitively, each $i^{th}$ value of $L^{DTW}$ and $U^{DTW}$ report respectively the minimum and the maximum value of the points in $D$ that can be aligned, with the $i^{th}$ position of a matching series. In Figure~\ref{lowerboundingDTW}(a), we report a data series $D$ (plotted using a dashed line), contoured by its $dtwENV_{r}(D)$ envelope ($r=7$).
	
	\noindent{\bf Lower bounding DTW.} We can thus report the $LB_{Keogh}$ distance \cite{DBLP:journals/kais/KeoghR05}, which is computed between a DTW envelope of a series $D$ and a data series $D'$, where $|D|=|D'|$ and the warping window is $r$:

{\small
	\begin{equation}
	\begin{gathered} 
	     \qquad\qquad LB_{Keogh}(dtwENV_{r}(D),D') =\\
	     \qquad\qquad \sqrt{\sum_{i=1}^{|D|} 
		  \begin{cases}
		 ( D'_{i}-U_{i}^{DTW})^2,&  if  D'_{i} > U_{i}^{DTW}\\
		 ( D'_{i}-L_{i}^{DTW})^2,&  if  D'_{i} < L_{i}^{DTW}\\s
		 0 &  \text{otherwise.}
		\end{cases}}
	\label{lbKeogh}
	\end{gathered}
	\end{equation}
}

The $LB_{Keogh}$ distance between $dtwENV_{r}(D)$ and $D'$ is guaranteed to be always smaller or equal than $DTW(D,D')$, computed with warping window $r$.
In Figure~\ref{lowerboundingDTW}(b), we represent graphically the $LB_{Keogh}$ distance between $dtwENV_{r}(D)$ (from Figure~\ref{lowerboundingDTW}(a)), and a new series $D'$. The vertical (blue) lines represent the positive differences between $D'$ and the dtw envelope of $D$, in the equation~\ref{lbKeogh}.
%This quantity is always smaller than the true DTW between $d$ and $d'$. 
Note that, the computation of $LB_{Keogh}$ takes $O(\ell)$ time (linear), whereas the true DTW computation runs in $O(\ell r)$ time using dynamic programming~\cite{DBLP:journals/kais/KeoghR05,DBLP:conf/kdd/RakthanmanonCMBWZZK12}.

\noindent{\bf Lower bounding DTW in ULISSE.} We propose, a new lower bounding measure for the true DTW distance between a data series and all the sequences (of the same length) represented by an \textit{ULISSE} Envelope. To that extent, we firstly introduce a measure based on $LB_{Keogh}$ distance, which is computed between the PAA representation of $dtwENV_{r}(D)$ and the $iSAX$ representation of $D'$. Given $w$, the number of PAA coefficients of each dtw envelope series ($U^{DTW}$,$L^{DTW}$) that is equivalent to the number of iSAX coefficients of $D'$, we have:   

\begin{equation}
\begin{gathered} 
{\small LB_{Keogh_{PAA\_iSAX}}(PAA(dtwENV_{r}(D)),iSAX(D'))=}\\
\sqrt{\frac{|D|}{w}} \sqrt{\sum_{i=1}^{w} 
	\begin{cases}
		\scriptstyle(\beta_{\ell}(iSAX(D')_{i}) - PAA(U^{DTW})_{i} )^2,&  \scriptstyle if (*)\\
		\scriptstyle(PAA(L^{DTW})_{i} - \beta_{u}(iSAX(D')_{i}))^2,& \scriptstyle if (**)\\
		\scriptstyle 0 & {\footnotesize \text{otherwise.}}
	\end{cases}}\\
   \scriptstyle(*) \beta_{\ell}(iSAX(D')_{i}) > PAA(U^{DTW})_{i} )\\
   \scriptstyle(**) PAA(L^{DTW})_{i} > \beta_{u}(iSAX(D')_{i}) 
   \label{lbKeoghPAAISAX}
\end{gathered}
\end{equation}
	
We know that $LB_{Keogh_{PAA\_iSAX}}(PAA(dtwENV_{r}(D)),$\\$iSAX(D')) \le LB_{Keogh}(dtwENV_{r}(D),D')$ as proven by Keogh et al. \cite{DBLP:journals/kais/KeoghR05}.
Given the $PAA$ representation of $dtwENV_{r}(D)$ (of $w$ coefficients), and an \textit{ULISSE} Envelope built on $D'$: $uENV_{paaENV_{[D',\ell_{min},\ell_{max},a,\gamma,s]}})$ = $[L,U]$, we define:
 
\begin{equation}
\begin{gathered} 
{\small LB_{PAL}(PAA(dtwENV_{r}(D)),uENV_{paaENV[D',...]})=}\\
\sqrt{s} \sqrt{\sum_{i=1}^{w} 
	\begin{cases}
	\scriptstyle(\beta_{\ell}(iSAX(L)_{i}) -  PAA(U^{DTW})_{i} )^2,&  \scriptstyle if (*)\\
	\scriptstyle( PAA(L^{DTW})_{i} - \beta_{u}(iSAX(L)_{i}))^2,& \scriptstyle if (**)\\
	\scriptstyle 0 & {\footnotesize \text{otherwise.}}
	\end{cases}}\\
\scriptstyle(*) \beta_{\ell}(iSAX(L)_{i}) > PAA(U^{DTW})_{i} )\\
\scriptstyle(**) PAA(L^{DTW})_{i} > \beta_{u}(iSAX(U)_{i}) 
\label{PalpanasLinardiBound}
\end{gathered}
\end{equation}


\begin{proposition}
Given two data series $D$ and $D'$, where $\ell{min} \le |D| \le \ell{max}$, the distance $LB_{PAL}(PAA(dtwENV_{r}(D)),$\\$uENV_{paaENV[D',\ell_{min},\ell_{max},a,\gamma,s]})$ is always smaller or equal to $DTW(D,D'_{i,|D|})$, for each $i$ such that $a \le i \le a+\gamma+1$ and $|D'|-(i-1) \ge \ell{min}$.
\end{proposition}

Intuitively, the proposition states that the $LB_{PAL}$ function always provides a measure that is smaller than the true DTW distance between $D$ and each subsequence in $D'$ of the same length, represented by $uENV_{paaENV[D',\ell_{min},\ell_{max},a,\gamma,s]})$.	

\begin{proof}(sketch)
 The preposition clearly holds if $LB_{PAL}$ yields zero, since the DTW distance between two series is always positive or equal to zero. We thus test the case, where Equation~\ref{PalpanasLinardiBound} provides a strictly positive measure. In the first case, the $i^{th}$ lower iSAX breakpoint of $L$ ($\beta_{\ell}(iSAX(L)_{i})$) is greater than the $i^{th}$ PAA coefficient of the $U^{DTW}$, namely $PAA(U^{DTW})_{i}$. This denotes that any other $i^{th}$ iSAX coefficient of the series represented by the \textit{ULISSE} Envelope are necessarily farthest to $PAA(U^{DTW})_{i}$. The second case is symmetric. Here, the $\beta_{u}(iSAX(L)_{i})$ coefficient is the closest to $PAA(L^{DTW})_{i}$, and greater than any other $i^{th}$ iSAX coefficient of the series in $D'$. This proves that $LB_{PAL}$ is always smaller or equal to $\underset{i}{\operatorname{argmin}}$($LB_{Keogh_{PAA\_iSAX}}(PAA(dtwENV_{r}(D),$\\$iSAX(D'_{i,|D|}))$).     		
\end{proof}


In Figure~\ref{lowerboundingDTW}(c), we depict a graphical example, which shows the computation of $LB_{PAL}$ between the DTW Envelope that is built around the prefix of $D$ (\textit{153} points) and the \textit{ULISSE} Envelope of the series $D'$. For this latter, the settings is: $a=1$, $\ell_{min}=153$, $\ell_{max}=255$, $\gamma=0$ and $s=51$. In the figure, we represents the iSAX coefficients of the \textit{ULISSE} Envelope, with (gray) rectangles delimited by their breakpoints (dashed horizontal lines). The coefficients of $PAA(U^{DTW})$ and $PAA(L^{DTW})$ are represented by red and green solid segments.   


}   

\subsection{Approximate search}

\commentRed{Similarity search performed on \textit{ULISSE} index relies on Equation~\ref{mindistPAAuENV} (Euclidean distance) and Equation~\ref{PalpanasLinardiBound} (DTW distance) to prune the search space. 
This allows to navigate the tree in order, visiting first the most promising nodes.}

We thus provide a fast approximate search procedure we report in Algorithm~\ref{approximateSearch}. 
In Line~\ref{internalnodepush} (Line~\ref{internalnodepush2} if DTW distance is used), we start to push the internal nodes of the index in a priority queue, where the nodes are sorted according to their lower bounding distance to the query. 
Note that in the comparison, we use the largest prefix of the query, which is a multiple of the $PAA$ segment length, used at the index building stage (Line~\ref{largestMultiple}). 
Then, the algorithm pops the ordered nodes from the queue, visiting their children in the loop of Line~\ref{loopSubtrees}. 
In this part, we still maintain the internal nodes ordered (Lines~\ref{keepOrdered},\ref{keepOrdered2}). 

As soon as a leaf node is discovered (Line~\ref{iterateLeaf}), we check if its lower bound distance to the query is shorter than the \textit{bsf}. 
If this is verified, the dataset does not contain any data series that are closer than those already compared with the query. 
In this case, the approximate search result coincides with that of the exact search.
Otherwise, we can load the raw data series pointed by the Envelopes in the leaf, which are in turn sorted according to their position, to avoid random disk reads.
We visit a leaf only if it contains Envelopes that represent sequences of the same length as the query. 
Each time we compute either the true Euclidean distance (Line~\ref{EDcompt}) or the true DTW distance ((Line~\ref{DTWcompt})), the best-so-far distance (\textit{bsf}) is updated, along with the $R^{a}$ vector. 
Since priority is given to the most promising nodes, we can terminate our visit, when at the end of a leaf visit the $k$ \textit{bsf}'s have not improved (Line~\ref{endVisit}). 
Hence, the vector $R^{a}$ contains the $k$ approximate query answers. 

\begin{algorithm}[!tb]
{\scriptsize
	\SetAlgoLined
	\label{approximateSearch}
	\KwIn{\textbf{int} $k$, \textbf{float []} $Q$, \textbf{\textit{ULISSE} index} I, \textbf{int} r \tcp{warping window}}
	\KwOut{\textbf{float [$k$][$|Q|$]} $R^{a}$, \textbf{float []} \textit{bsf}}
	\scriptsize
	\textbf{float []}  $Q^*$ $\leftarrow$ $PAA(Q_{1,..,\lfloor|Q| / I.s \rfloor})$\; \label{largestMultiple}
	\textbf{float[k]} \textit{bsf} $\leftarrow \{\infty,...,\infty\}$ \;
	\textbf{PriorityQueue} nodes\;
	\ForEach{node \textbf{in} I.root.children()}
	{ 
		\uIf{Euclidean distance search}
		{
			$nodes.push(node,mindist_{ULiSSE}(Q^{*},node))$;\label{internalnodepush}
		}
		\ElseIf{DTW search}
		{
			$nodes.push(node,LB_{PAL}(PAA(dtwENV_{r}(Q)),node))$;\label{internalnodepush2} 
		}		
	} 
	\While{n = nodes.pop()}
	{\label{loopSubtrees}
		\uIf{n.isLeaf() \textbf{and} n.containsSize($|Q|$) }
		{	
			\uIf{n.$lowerBound<$ \textit{bsf}[k] }
			{
				\label{iterateLeaf}
				\tcp{sort according disk pos.}
				\textbf{$\mathbf{uENV}$ []} Envelopes = $sort(n.Envelopes)$\;
				\tcp{iterate the Env. and compute true ED }
				oldBSF $\leftarrow$ \textit{bsf}[k]\;
				\ForEach{E \textbf{in} Envelopes}
				{\textbf{float []} D  $\leftarrow $ readSeriesFromDisk(E)\;
					\For{i $\leftarrow$ E.a \textbf{to} \textbf{min}(E.a+E.$\gamma$+1,$|D|-(|Q|-1)$)}
					{
						\uIf{Euclidean distance search}
						{
							$ED_{updateBSF}$($Q,E.D_{i,|Q|},k,$\textit{bsf}$, R^{a}$);\label{EDcompt}
						}
						\ElseIf{DTW search}
						{
							$DTW_{updateBSF}$($Q,E.D_{i,|Q|},k,$\textit{bsf}$, R^{a}$,$r$);\label{DTWcompt}
						}	
					
				
					}	
				}
				\tcp{if \textit{bsf} has not improved end visit.}
				\If{oldBSF $==$ \textit{bsf}[k]}
				{\label{endVisit}
					\textit{break}\;
				}
			}
			\Else
			{
%				\tcp{No better answers in the dataset,
%				approximate search is exact.}
				break; \tcp{Approximate search is exact.} 
			}
		}
		\Else
		{
				$LBleft$ $\leftarrow 0$, $LBright$ $\leftarrow 0$\;
			\uIf{Euclidean distance search}
			{
				$LBleft$ $\leftarrow$$mindist_{\textit{ULISSE}}(Q^{*},n.left)$\; 
				$LBright$ $\leftarrow$$mindist_{\textit{ULISSE}}(Q^{*},n.right)$\;
			}
			\ElseIf{DTW search}
			{
				$LBleft$$\leftarrow$$LB_{PAL}(PAA(dtwENV_{r}(Q)),n.left)$\;
				$LBright$$\leftarrow$$LB_{PAL}(PAA(dtwENV_{r}(Q)),n.right)$;
			}
		
			$nodes.push(n.left,LBleft)$\;\label{keepOrdered}
			$nodes.push(n.right,LBright)$\;\label{keepOrdered2}
	
		}
	
	}   
} 
	\caption{\textit{ULISSE} $K$-$nn$-$Approx$}	
\end{algorithm}


\subsection{Exact search}
\begin{algorithm}[!tb]
{\scriptsize
	\SetAlgoLined
	\label{exactSearch}
	\KwIn{\textbf{int} $k$, \textbf{float []} $Q$, \textbf{\textit{ULISSE} index} I, \textbf{int} r \tcp{warping window}}
	\KwOut{\textbf{float [$k$][$|Q|$]} $R$ }
	\scriptsize
	\textbf{float []}  $Q^*$ $\leftarrow$ $PAA(Q_{1,..,\lfloor|Q| / I.s \rfloor})$\; \label{largestMultiple2}
	\textbf{float []} \textit{bsf}, \textbf{float [$k$][$|Q|$]} $R$ $\leftarrow$ $K$-$nn$-$Approx(k,Q,I)$ \; 
	\If{\textit{bsf} is not exact}
	{
		\ForEach{E \textbf{in} $I.inMemoryList$}
		{ 
			$LBDist$$\leftarrow 0$\;
			\uIf{Euclidean distance search}
			{
				$LBDist$$\leftarrow$$mindist_{ULiSSE}(Q^{*},E)$;\label{checkLBED}
			}
			\ElseIf{DTW search}
			{
				$LBDist$$\leftarrow$$LB_{PAL}(PAA(dtwENV_{r}(Q)),E)$;\label{checkLBDTW}
			}
			
			\If{ $LBDist$$<$ \textit{bsf}[k]}
			{
				\textbf{float []} D  $\leftarrow $ readSeriesFromDisk(E)\;
				\For{i $\leftarrow$ E.a \textbf{to} \textbf{min}(E.a+E.$\gamma$+1,$|D|-(|Q|-1)$)}
				{
					\uIf{Euclidean distance search}
					{
						$ED_{updateBSF}(Q,E.D_{i,|Q|},k,$\textit{bsf}$, R)$\;
					}
					\ElseIf{DTW search}
					{
						$DTW_{updateBSF}(Q,E.D_{i,|Q|},k,$\textit{bsf}$, R)$\;
					}
				}
			}	
		}
	}
}
\caption{\textit{ULISSE} $K$-$nn$-$Exact$}	
\end{algorithm}

Note that the approximate search described above may not visit leaves that contain answers better than the approximate answers already identified, and therefore, it will fail to produce exact, correct results.
We now describe an exact nearest neighbor search algorithm, which finds the $k$ sequences with the absolute smallest distances to the query. 
%Note that, by exact we mean the subsequences in the collection $C$, having the $k$ shortest distances. 

In the context of exact search, accessing disk-resident data following the lower bounding distances order may result in several leaf visits: this process can only stop after finding a node, whose lower bounding distance is greater than the \textit{bsf}, guaranteeing the correctness of the results. 
%This is probable, especially for short query content lengths (i.e space explosion on short patterns). 
This would penalize computational time, since performing many random disk I/O might unpredictably degenerate. 

We may avoid such a bottleneck by sorting the Envelopes, and in turn the disk accesses. 
%Still, this may consist into a non-negligible cost.    
Moreover, we can exploit exploit the \textit{bsf} provided by approximate search, in order to perform a sequential search with pruning over the sorted Envelopes list (this list is stored across the \textit{ULISSE} index).
Intuitively, we rely on two aspects. 
First, the \textit{bsf}, which can translate into a tight-enough bound for pruning the candidate answers.
Second, since the list has no hierarchy structure, any Envelope is stored with the highest cardinality available, which guarantees a fine representation of the series, and can contribute to the pruning process. 

Algorithm~\ref{exactSearch} describes the exact search procedure. 
In the case of Euclidean distance search, in Line~\ref{checkLBED} we compute the lower bounding distance between the Envelope and the query. On the other hand, when DTW distance we compute the lower bound distance in Line~\ref{checkLBDTW}.      
If the computed lower bound is not better than the $k^{th}$ \textit{bsf}, we do not access the disk, pruning Euclidean Distance computations as well.
We note that, while we are computing the true Euclidean distance, we can speed-up computations using the \textit{Early Abandoning} technique~\cite{DBLP:conf/kdd/RakthanmanonCMBWZZK12}, which is effective especially for Z-normalized data series.



\subsection{Complexity of query answering}

We provide now the time complexity analysis of query answering with \textit{ULISSE}. 
Both the approximate and exact query answering time strictly depend on data distribution as shown in~\cite{DBLP:conf/kdd/ZoumpatianosLPG15}. 
We focus on exact query answering, since approximate is part of it.

\noindent{\bf Best Case.} 
In the best case, an exact query will visit one leaf at the stage of the approximate search (Algorithm~\ref{approximateSearch}), and during the second leaf visit will fulfill the stopping criterion (i.e., the \textit{bsf} distance is smaller than the lower bounding distance between the second leaf and the query). 
Given the number of the first layer nodes (root nodes) $N$, the length of the first leaf path $L$, and its size $S$, the best case complexity is given by the cost to iterate the first layer node and descend to the leaf keeping the nodes sorted in the heap: $O(w(N + L log L))$, where $w$ is the number of symbols checked at each lower bounding distance computation. We recall that computing the lower bound of Euclidean or DTW distance has equal time complexity. Moreover we need to take into account the additional cost of sorting the disk accesses and computing the true distances in the leaf, which is $O(S(logS+\ell_{max}))$ in the case of Euclidean distance, and $O(SlogS+S r \ell_{max})$ for DTW distance, where $r$ is the warping window length.

\noindent{\bf Worst Case.}
The worst case for exact search takes place when at the approximate search stage, the complete set of leaves that we denote with $T$, need to be visited. 
This has a cost of $O(w(N + T L log L))$ plus the cost of computing the true distances, which takes either $O(T(S(logS+\ell_{max})))$ (Euclidean distance), or $O(T(SlogS+ Sr\ell_{max}))$ (DTW distance).
%The search here is thus reduced to a full raw data scan, plus the overhead of disk accesses performed in random fashion.
Note though that this worst case is pathological: for example, when all the series in the dataset are the same straight lines (only sligthly perturbed).
Evidently, the very notion of indexing does not make sense in this case, where all the data series look the same. 
%For instance, we may build or find an ad-hoc dataset, which contains straight lines data series, slightly perturbed. 
%If the query follow such an order, where the \textit{bsf} keeps improving at each leaf visit, the approximate search will not be over before the iteration of the whole tree. This circumstance is not only very improbable; but 
As we show in our experiments on several datasets, in practice, the approximate algorithm always visits a very small number of leaves.
%, taking a linear time with respect to the best case.

\noindent{\bf \textit{ULISSE} K-nn Exact complexity.}
So far we have considered the exact K-nn search with regards to Algorithm~\ref{approximateSearch} (approximate search). 
When this algorithm produces approximate answers, providing just an upper bound \textit{bsf}, in order to compute exact answers we must run Algorithm~\ref{exactSearch} (exact search). 
The complexity of this procedure is given by the cost of iterating over the Envelopes and computing the \textit{mindist}, which takes $O(Mw)$ time, where $M$ is the total number of Envelopes. 
Let's denote with $V$ the number of Envelopes, for which the raw data are retrieved from disk and checked. 
Then, the algorithm takes an additional $O(V\ell_{max})$ time to compute the true Euclidean distances, or $O(Vr\ell_{max})$ to compute the true DTW distances.







