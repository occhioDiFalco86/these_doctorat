\section{Experimental Evaluation}


\noindent{\bf Setup.}
All the experiments presented in this section are completely reproducible: the code and datasets we used are available online~\cite{1}.
We implemented 
%the \textit{ULISSE} 
all algorithms (indexing and query answering) in C (compiled with gcc 4.8.2).
We ran experiments on an Intel Xeon E5-2403 (4 cores @ 1.9GHz), using the x86\_64 GNU/Linux OS environment.

\noindent{\bf Algorithms.}
We compare \textit{ULISSE} to the \emph{Compact Multi-Resolution Index (CMRI)~\cite{DBLP:journals/kais/KadiyalaS08}}, which is the current state-of-the-art index for similarity search with varying-length queries (recall that \emph{CMRI} constructs a limited number of distinct indexes for series of different lengths). 
We note though, that in contrast to our approach, \emph{CMRI} can only support non Z-normalized sequences.
In addition, we compare to the current state-of-the-art algorithms for subsequence similarity search, the \textit{UCR suite}~\cite{DBLP:conf/kdd/RakthanmanonCMBWZZK12}, and \emph{MASS}~\cite{DBLP:conf/icdm/MueenHE14}. 
These algorithms do not use an index, but are based on optimized serial scans, and are natural competitors, since they can process overlapping subsequences very fast.

\noindent{\bf Datasets.}
For the experiments, we used both synthetic and real data.
We produced the synthetic datasets with a generator, where a random number is drawn from a Gaussian distribution $N(0,1)$, then at each time point a new number is drawn from this distribution and added to the value of the last number. This kind of data generation has been extensively used in the past~\cite{DBLP:conf/kdd/ZoumpatianosLPG15}, and has been shown to effectively model real-world financial data~\cite{Faloutsos1994}.

The real datasets we used are astrophysics and seismic data series.
The first contains \textit{100} Million astronomical data series of length 256 (\textit{100}GB), representing celestial objects (ASTRO)\cite{ltv}. 
The second real dataset contains \textit{100} Million seismic data series (\textit{100}GB) of length 256, collected from the IRIS Seismic Data Access repository (SEISMIC)~\cite{SEISMIC}.
In our experiments, we test queries of lengths \textit{160-4096} points, since these cover at least $90\%$ of the ranges explored in works about data series indexing in the last two decades \cite{DBLP:journals/datamine/KeoghK03,DBLP:journals/datamine/BagnallLBLK17,DBLP:journals/datamine/WangMDTSK13}. Moreover, of the 85 datasets in the UCR archive~\cite{UCRArchive}, only four are (slightly) longer than \textit{1,024}.



\subsection{Envelope Building}

In the first set of experiments, we analyze the performance of the \textit{ULISSE} indexing algorithm. In Figure~\ref{indexingExp}.a) we report the indexing time (Envelope Building and Bulk loading operations) when varying $\gamma$. 
We use a dataset containing \textit{5M} series of length \textit{256}, fixing $\ell_{min}=160$ and $\ell_{max}=256$. 
We note that, when $\gamma =0$, the algorithm needs to extract as many Envelopes as the number of master series of length $\ell_{min}$. 
This generates a significant overhead for the index building process (due to the maximal Envelopes generation), but also does not take into account the contiguous series of same length, in order to compute the statistics needed for Z-normalization. 
A larger $\gamma$ speeds-up the Envelope building operation by several orders of magnitude, and this is true for a very wide range of $\gamma$ values (Figure~\ref{indexingExp}.a)).
%already after we set it to \textit{20\%} of its maximum possible value for this case. 
These results mean that the $uENV_{norm}$ building algorithm can achieve good performance in practice, despite its complexity that is quadratic on $\gamma$.

In Figure~\ref{indexingExp}.b) we report an experiment, where $\gamma$ is fixed, and the query length range ($\ell_{max}-\ell_{min}$) varies. 
We use a dataset, with the same size of the previous one, which contains \textit{2.5M} series of length \textit{512}.
The results show that increasing the range has a linear impact on the final running time.

\begin{figure}[tb]
\hspace*{-0.5cm}
	\includegraphics[trim={0cm 13cm 0cm 3cm},scale=0.55]{Chap1_ULISSE/PLOTS/indexingExp.pdf}
	\caption{a) Construction and bulk Loading time (log scale) of Envelopes in 5GB datasets varying $\gamma$ (5M of series of length 256), $\ell_{min}=160$, $\ell_{max}=256$ .
	% (expressed as percentage of $\ell_{max}-\ell_{min}$).
	b) Construction and Bulk Loading time (log scale) of Envelopes in 5GB dataset (2.5M of series of length 512) varying $\ell_{max} - \ell_{min}$ (lengths range), $\gamma=256$, fixed $\ell_{max}=512$.}
	\label{indexingExp}
\end{figure}




\subsection{Exact Search Similarity Queries}

We now test \textit{ULISSE} on exact 1-Nearest Neighbor queries. 
%We repeated this experiment varying the \textit{ULISSE} parameters, depicted in Table~\ref{tableParameters_1}. 
We have repeated this experiment varying the \textit{ULISSE} parameters along predefined ranges, which are (default in bold) $\gamma:[0\%, 20\%, 40\%, 60\%, 80\%, \mathbf{100\%}]$, where the percentage is referring to its maximum value, $\ell_{min}: [96, 128, \mathbf{160}, 192, 224, 256]$, $\ell_{max}: [256]$, dataset series length ($\ell_{S}$): $[\mathbf{256}, 512, 1024, 1536, 2048, 2560]$ and dataset size of $5GB$.
	%:  $[1GB, 2.5GB, 5GB, 10GB, 20GB]$. 
Here, we use synthetic datasets containing random walk data in binary format, where a single point occupies 4 bytes. Hence, in each dataset $C$, where $|C|^{Bytes}$ denotes the corresponding size in bytes, we have a number of subsequences of length $\ell$ given by $N^{seq} = (\ell_{S} - \ell + 1) \times ((|C|^{Bytes}/4)/\ell_{S})$.
For instance, in a \textit{5GB} dataset, containing series of length $256$, we have $\sim$\textit{500 Million} subsequences of length \textit{160}.

We record the average \textit{CPU time}, \textit{query disk I/O time} (time to fetch
data from disk: Total time - CPU time), and \textit{pruning power} (percentage of the total number of Envelopes in the index that do not need to be read), of \textit{100} queries, extracted from the datasets with the addition of Gaussian noise. For each index used, the \textit{building time} and the relative \textit{size} are reported.
Note that we clear the main memory cache before answering each set of queries.
% we consider in the empirical evaluation. To that extent, we want to simulate the execution of a real application, where each query benefits from the data load in the cache by the previous one, when accessing the disk. 
We have conducted our experiments using datasets that are both smaller and larger than the main memory.
%In the experiments, we used queries with lengths between \textit{256-4096}. This choice aims to cover the typical scenarios of pattern discovery and analysis, performed in real datasets~\cite{Shieh2009,DBLP:journals/kais/CamerraSPRK14,Keogh2000}. 
%Throughout the empirical evaluation we utilized query lengths, which are multiples of the PAA segment length, since the index only considers such queries for answering any similarity search query of length in the range [$\ell_{min},\ell_{max}$].\\

In all experiments, we report the cumulative running time of \textit{1000} random queries for each query length.

%To the best of our knowledge we are not aware of indexing solutions, working with Z-normalized series of varying length. We implemented \textit{CMRI} using the same bulk loading algorithm of $\textit{ULISSE}$, in order to keep the comparison fair.    

%\begin{table}[h]
%	\begin{center}
%		%		\begin{tabular}{|c|c|}
%		%			\hline
%		%			\begin{minipage}[c]{0.05\columnwidth}%
%		%				\vspace{1em}
%		%				\centering
%		%				$\gamma$
%		%				\vspace{1em}
%		%			\end{minipage}
%		%			&  	
%		%			0\%, 20\%, 40\%, \textbf{60\%}, 80\%, 100\%
%		%			\\ 	\hline
%		%			\begin{minipage}[c]{0.05\columnwidth}%
%		%				\vspace{0.5em}
%		%				\centering
%		%				$\ell_{min}$
%		%				\vspace{0.5em}
%		%			\end{minipage} 	
%		%			&  	
%		%			96, 128, \textbf{160}, 192, 224, 256
%		%			\\ 	\hline
%		%			\begin{minipage}[c]{0.07\columnwidth}%
%		%				\vspace{0.5em}
%		%				\centering
%		%				$\ell_{max}$
%		%				\vspace{0.5em}
%		%			\end{minipage} 
%		%			& 
%		%			\textbf{256}
%		%			\\ 	\hline
%		%			\begin{minipage}[c]{0.20\columnwidth}%
%		%				\vspace{1em}
%		%				\centering
%		%				$n$\\size series 
%		%				\vspace{1em}
%		%			\end{minipage} 
%		%			&
%		%		
%		%			\begin{minipage}[c]{0.60\columnwidth}%
%		%					\centering\textbf{256}, 512, 768, 1024, 1280,\\ 1536, 1792, 2048, 2304, 2560
%		%			\end{minipage} 
%		%				\\ 	\hline
%		%			\begin{minipage}[c]{0.15\columnwidth}%
%		%				\vspace{0.5em}
%		%				\centering
%		%				Size dataset
%		%				\vspace{0.2em}
%		%			\end{minipage}
%		%			& 
%		%			1 GB, 2.5 GB, \textbf{5 GB}, 10 GB, 20 GB
%		%			\\ 	\hline
%		%		\end{tabular}
%		
%		\includegraphics[trim={0cm 13cm 17cm 3cm},scale=0.65]{Chap1_ULISSE/figures/parameters_experiments}
%		%\vspace*{-0.4cm}
%		\caption{\textit{a)} \label{tableParameters_1}Parameters of \textit{ULISSE} calibration (default values in bold). $\gamma$ is expressed as the percentage of its maximum value: $\ell{max}-\ell{min}$. \textit{b)} Number of subsequences in our datasets.}
%		%\vspace*{-0.6cm}
%	\end{center}
%\end{table}


\begin{figure}[tb]
	\includegraphics[trim={0cm 3.5cm 10cm 3cm},scale=1]{Chap1_ULISSE/PLOTS/1}
	\caption{Query answering time performance, varying $\gamma$ on non Z-normalized data series. \textit{a)} \textit{ULISSE} average query time (CPU + disk I/O). \textit{b)} \textit{ULISSE} average query disk I/O time. \textit{c)} \textit{ULISSE} average query pruning power. \textit{d)} Comparison of \textit{ULISSE} to other techniques (cumulative indexing + query answering time). \textit{e)} Table resuming the indexes' properties.}
	\label{1Exp}
\end{figure}

\begin{figure}[tb]
	\includegraphics[trim={0cm 3.5cm 10cm 3cm},scale=0.55]{Chap1_ULISSE/PLOTS/2}
	\vspace*{-0.4cm}
	\caption{\textit{ULISSE} Indexing and exact queries performance on Z-normalized series, varying sigma. \textit{a)} Average query time (CPU + disk I/O). \textit{b)} Average query disk I/O time. \textit{c)} Average query pruning power. \textit{d)} Comparison to other techniques (cumulative indexing + query answering time). \textit{e)} Table resuming the indexes' properties.}
	\vspace*{-0.4cm}
	\label{2Exp}
\end{figure}

\noindent{\bf Varying $\gamma$.}
We first present results for similarity search queries on $\textit{ULISSE}$ when we vary~$\gamma$, ranging from \textit{0} to its maximum value, i.e., $\ell_{max}-\ell_{min}$. 
In Figure~\ref{1Exp}, we report the results concerning non Z-normalized series (for which we can compare to \textit{CMRI}).
We observe that grouping contiguous and overlapping subsequences under the same summarization (Envelope) by increasing $\gamma$, affects positively the performance of index construction, as well as query answering (Figure~\ref{1Exp}.a,d)).
The latter may seem counterintuitive, since $\gamma$ influences in a negative way pruning power, as depicted in Figure~\ref{1Exp}.c). 
Indeed, inserting more master series into a single $Envelope$ is likely to generate large containment areas, which are not tight representations of the data series.
On the other hand, it leads to an overall number of $Envelopes$ that is several orders of magnitude smaller than the one for $\gamma=0\%$. 
In this last case, when $\gamma=0$, the algorithm inserts in the index as many records as the number of master series present in the dataset (\textit{485}M), as reported in (Figure~\ref{1Exp}.e)).

We note that the disk I/O time on compact indexes is not negatively affected at the same ratio of pruning power. 
On the contrary, in certain cases it becomes faster. 
For example, the results in Figure~\ref{1Exp}.b) show that for query length 160, the $\gamma=100\%$ index is more than 2x faster in disk I/O than the $\gamma=0\%$ index, despite the fact that the latter index has an average pruning power that is $14\%$ higher (Figure~\ref{1Exp}.c)). 
This behavior is favored by disk caching, which translates to a higher hit ratio for queries with slightly larger disk load. 
We note that we repeated this experiment several times, with different sets of queries that hit different disk locations, in order to verify this specific behavior. The results showed that this disk I/O trend always holds. 

While disk I/O represents on average the $3-4$\% of the total query cost, computational time significantly affects the query performance. 
Hence, a compact index, containing a smaller number of $Envelopes$, permits a fast in memory sequential scan, performed by Algorithm~\ref{exactSearch}.
%\commentOr{There is a trade-off between the length of sequential scan (sims), which needs to iterate and compute the mindist on each Envelope, and pruning power. This is more clear on short queries, because there are more Envelopes containing subsequence of that length (e.g 160). On the max length (256) there are exactly the same number of Envelopes to check, in all the indexes. The sweet spot of this trade-off is at $\gamma=$60\%. Concerning Early Abandoning, it is higher, especially when the sigma 40-60\%, but this is not explicable. maybe it is better to leave it away???}  
\begin{comment}
When an $Envelopes$ points raw series, which are longer than the query, we may take advantage from the  perform a higher number of comparisons, since the loaded series contains many overlapping subsequences. 
Ticomputations  This techniques allows to compute in constant time the statistics of overlapping subsequences, which are in turn used to terminate the Euclidean distance computations in advance, if the best-so-far can not be improved.
We furthermore note that, in the case of non Z-normalized data, the \textit{ULISSE} sweet spot is found, when $\gamma$ is at 60\% of its maximum (Figure~\ref{1Exp}.d). On the other hand, in Figure~\ref{2Exp}.d), we may see that the best performance are provided when $\gamma$ is maximized. This indicates, how \textit{Early Abandoning} is more effective on Z-normalized data, as pointed out in \cite{DBLP:conf/kdd/RakthanmanonCMBWZZK12}. 
\end{comment}

%In Figure~\ref{1Exp}(d) we show the cumulative time performance of answering $1,000$ exact search queries of each length (i.e., $4,000$ queries in total). 

In Figure~\ref{1Exp}.d) we show the cumulative time performance (i.e., $4,000$ queries in total), comparing \textit{ULISSE}, \textit{CMRI}, and \textit{UCR Suite}. 
Note that in this experiment, $\textit{ULISSE}$ indexing time is negligible w.r.t. the query answering time. 
$\textit{ULISSE}$, outperforms both \textit{UCR Suite} and \textit{CMRI}, achieving a speed-up of up to 12x.

%Further analyzing the performance of the \textit{CMRI} technique, we observe that a simple window extraction generates a non-negligible indexing time, which is in the same order of magnitude of the whole query answering task. 
Further analyzing the performance of \textit{CMRI}, we observe that it constructs four indexes (for four different lengths), generating more than $2B$ index records!
Consequently, it is clear that the size of these indexes will negatively affect the performance of \textit{CMRI}, even if it achieves reasonable pruning ratios.
%It is clear that, also high pruning capability are nevertheless compromised by the scale of the index, affecting in negative the query CPU and disk I/O time. 
These results suggest that the idea of generating multiple copies of an index for different lengths, is not a scalable solution.


%\noindent{\bf Varying Dataset Size.}\commentOr{NOTE: TO REMOVE. If it will be kept add at the beginning the list of sizes we test and modify the revision where we cite we removed it.} We now analyze, the \textit{ULISSE} performance as the indexed dataset size increases. 
%We depict the results for Z-normalized datasets in Figure~\ref{varDatasetSizeNorm}.
%
%% Here, $\textit{ULISSE}$ indexing time growths linearly, both for Z-normalized and normalized subsequences. This is not the case for the $CMRI$ index. Even though, by using this technique a low number of indexes are generated, still the sliding window dependency implies an important generation of records, as depicted in Figure \ref{varDatasetSize}.\textit{e)}.    
%Concerning $\textit{ULISSE}$ query answering, as the pruning power slightly increases, across the different dataset sizes, for each query length, both disk I/O and CPU time increase linearly as depicted in Figure~\ref{varDatasetSizeNorm}.a,b). \commentOr{We also note that the pruning power in larger datasets tends to increase, as reported in Figure~\ref{varDatasetSizeNorm}.c)}.
%%Figure~\ref{varDatasetSizeNorm}(d) shows the comparison with UCR suite, where we report the query answering time for $1,000$ exact search queries of each length (i.e., $4,000$ queries in total).
%Figure~\ref{varDatasetSizeNorm}.d) shows the comparison to UCR suite, where we report the cumulative query answering time for $1,000$ exact search queries of each length.
%$\textit{ULISSE}$ gracefully scales with the dataset size, completing the task up to 4x faster than UCR suite. 

% \begin{figure}[tb]
% 	\includegraphics[trim={0cm 3.5cm 10cm 3cm},scale=0.6]{Chap1_ULISSE/PLOTS/4}
% 	\caption{\textit{ULISSE} Indexing and exact queries performance on non Z-normalized series, varying Dataset size. \textit{a)} Average query time (CPU + disk I/O). \textit{b)} Average query disk I/O time. \textit{c)} Average query pruning power. \textit{d)} Comparison with the state-of-the-art techniques, running thousands queries of each length (cumulative indexing + query answering time). \textit{e)} Table resuming the indexes properties. For \textit{ULISSE} and $CMRI$ each column in the plot is equivalent to a different index.}
% 	\label{varDatasetSize}
% \end{figure}
%
%\begin{figure}[tb]
%	\includegraphics[trim={0.5cm 9.5cm 10cm 3cm},scale=0.6]{Chap1_ULISSE/PLOTS/5}
%	%\vspace*{-0.4cm}
%	\caption{Query answering time performance, varying the dataset size on Z-normalized data series. \textit{a)} \textit{ULISSE} average query time (CPU + disk I/O). \textit{b)} \textit{ULISSE} average query disk I/O time. \textit{c)} average query pruning power. \textit{d)} Comparison of \textit{ULISSE} to other techniques (cumulative indexing + query answering time). \textit{e)} Table resuming the indexes' properties.}
%	%\vspace*{-0.4cm}
%	\label{varDatasetSizeNorm}
%\end{figure}

\begin{figure*}[tb]
\hspace*{-1cm}
	\centering
%	\includegraphics[trim={1cm 5cm 16cm 3cm},scale=0.48]{Chap1_ULISSE/PLOTS/3_1}
%	\includegraphics[trim={0cm 5cm 16cm 3cm},scale=0.48]{Chap1_ULISSE/PLOTS/3_2}
%	\includegraphics[trim={0cm 8.5cm 18cm 3cm},scale=0.50]{Chap1_ULISSE/PLOTS/3b}
\includegraphics[trim={0cm 7.8cm 2cm 3cm},scale=0.60]{Chap1_ULISSE/PLOTS/3}
	\caption{Query answering time performance of \textit{ULISSE} and \textit{UCR Suite}, varying the data seres size. Average query (CPU time + disk I/O) (\textit{a}) for non Z-normalized, (\textit{c}) for Z-normalized series).
		Cumulative indexing + query answering time (\textit{b}) for non Z-normalized, (\textit{d}) for Z-normalized series).
		\textit{e)} Table resuming the indexes' properties. \textit{f)} Comparison between MASS algorithm, \textit{UCR Suite} and \textit{ULISSE}.}
	%For $\textit{ULISSE}$, each column in the plot is equivalent to a different index.}
	\label{VarLengthSeriesDataset}
\end{figure*}
\begin{figure}[tb]
	\includegraphics[trim={0cm 9cm 10cm 3cm},scale=0.55]{Chap1_ULISSE/PLOTS/7}
	%\vspace*{-0.4cm}
	\caption{Query answering time, varying the range of query length on Z-normalized data series. (\textit{a)} \textit{ULISSE} average query time (CPU + disk I/O). (\textit{b)} \textit{ULISSE} average query disk I/O time. (\textit{c)} \textit{ULISSE} average query pruning power. (\textit{d)} \textit{ULISSE} comparison to other techniques  (cumulative indexing + query answering time).}% (\textit{e)} Table resuming the indexes' properties.} 
	%For $\textit{ULISSE}$ each column in the plot is equivalent to a different index.}
	\label{varRangeQueryNorm}
	%\vspace*{-0.3cm}
\end{figure}
\noindent{\bf Varying Length of Data Series.}
In this part, we present the results concerning the query answering performance of \textit{ULISSE} and \textit{UCR Suite}, as we vary the length of the sequences in the indexed datasets,
%(from $256$ to $2560$). 
as well as the query length (refer to Figure~\ref{VarLengthSeriesDataset}).
In this case, varying the data series length in the collection, leads to a search space growth, in terms of overlapping subsequences, as reported in Figure~\ref{VarLengthSeriesDataset}.e). 
This certainly penalizes index creation, due to the inflated number of Envelopes that need to be generated.
On the other hand, \textit{UCR Suite} takes advantage of the high overlapping of the subsequences during the in-memory scan. 
Note that we do not report the results for $CMRI$ in this experiment, since its index building time would take up to \textit{1 day}. 
In the same amount of time, \textit{ULISSE} answers more than $1,000$ queries.

Observe that in Figures~\ref{VarLengthSeriesDataset}.a) and~.c), \textit{ULISSE} shows better query performance than the UCR suite, growing linearly as the search space gets exponentially larger. This demonstrates that \textit{ULISSE} offers a competitive advantage in terms of pruning the search space that eclipses the pruning techniques \textit{UCR Suite}. 
The aggregated time for answering $4,000$ queries ($1,000$ for each query length) is 2x for \textit{ULISSE} when compared to \textit{UCR Suite} (Figures~\ref{VarLengthSeriesDataset}.b) and~.d)).  


 \begin{figure}[tb]
	\includegraphics[trim={0cm 5.5cm 10cm 3cm},scale=0.55]{Chap1_ULISSE/PLOTS/6}
	\caption{\textit{ULISSE} Indexing and exact queries performance on non Z-normalized series, varying query length range. \textit{a)} Average query time (CPU + disk I/O). \textit{b)} Average query disk I/O time. \textit{c)} Average query pruning power. \textit{d)} Comparison with the state-of-the-art techniques, running thousands queries of each length (cumulative indexing + query answering time). \textit{e)} Table resuming the indexes properties. For \textit{ULISSE} each column in the plot is equivalent to a different index.}
	\label{varRangeQuery}
\end{figure}


\noindent{\bf Varying Range of Query Lengths.}
In the last experiment of this subsection, we investigate how varying the length range [$\ell{min};\ell{max}$] affects query answering performance. 
In Figure~\ref{varRangeQueryNorm}, we depict the results for Z-normalized sequences. 
We observe that enlarging the range of query length, influences the number of Envelopes we need to accommodate in our index.% (refer to Figure~\ref{varRangeQueryNorm}.e)). 
Moreover, a larger query length range corresponds to a higher number of Series (different normalizations), which the algorithms needs to consider for building a single Envelope (loop of line~\ref{loopNonMasterandMasterSeries} of Algorithm~\ref{algoEnvNorm}). 
This leads to large containment areas and in turn, coarse data summarizations. 
In contrast, Figure~\ref{varRangeQueryNorm}.c) indicates that pruning power slightly improves as query length range increases. 
This is justified by the higher number of Envelopes generated, when the query length range gets larger. 
Hence, there is an increased probability to save disk accesses. 
In Figure~\ref{varRangeQueryNorm}.a) we show the average query time (CPU + disk I/O) on each index, observing that this latter is not significantly affected by the variations in the length range.
The same is true when considering only the average query disk I/O time (Figure~\ref{varRangeQueryNorm}.b), which accounts for $3-4$\% of the total query cost. We note that the cost remains stable as the query range increases, when the query length varies between \textit{96}-\textit{192}. For queries of length \textit{224} and \textit{256}, when the range is the smallest possible the disk I/O time increases.
This is due to the high pruning power, which translates into a higher rate of cache misses.  

%In Figure~\ref{varRangeQueryNorm}.a), we show the average query time (CPU + disk I/O) on each  index. 
%In Figure~\ref{varRangeQueryNorm}(d), the aggregated time of $4,000$ queries ($1,000$ for each query length) {\bf ??? we have repeated this 4000/1000 5 times until now; it would be better to write this at the beginning of the experimental section, and also explain there the synthetic and real datasets ???} shows \textit{ULISSE} achieving an up to 2x speed-up over \textit{UCR Suite}. 
In Figure~\ref{varRangeQueryNorm}.d), the aggregated time comparison shows \textit{ULISSE} achieving an up to 2x speed-up over \textit{UCR Suite}.




\subsection{Comparison to Serial Scan Algorithms}


We now perform further comparisons to serial scan algorithms, namely, \emph{MASS} and \textit{UCR Suite}, with varying query lengths.

\emph{MASS}~\cite{DBLP:conf/icdm/MueenHE14} is a recent data series similarity search algorithm that
computes the distances between a Z-normalized query of length $l$ and all the Z-normalized overlapping subsequences of a single sequence of length $n \ge l$. 
\emph{MASS} works by calculating the dot products between the query and $n$ overlapping subsequences in frequency domain, in $logn$ time, which then permits to compute each Euclidean distance in constant time. 
Hence, the time complexity of \emph{MASS} is $O(nlogn)$, and is independent of the data characteristics and the length of the query ($l$).
In contrast, the \textit{UCR Suite} effectiveness of pruning computations may be significantly affected by the data characteristics.
%series values.

We compared \textit{ULISSE} (using the default parameters), MASS and \textit{UCR Suite} on a dataset containing \textit{5M} data series of length \textit{4096}. 
In Figure~\ref{varRangeQueryNorm}.f), we report the average query time (CPU + disk/io) of the three algorithms.

We note that MASS, which in some cases is outperformed by \textit{UCR Suite} and $\textit{ULISSE}$, is strongly penalized, when ran over a high number of non overlapping series. 
The reason is that, although MASS has a low time complexity of $O(nlogn)$, the Fourier transformations (computed on each subsequence) have a non negligible constant time factor that render the algorithm suitable for computations on very long series.


\subsection{Approximate Search Similarity Queries}
\setcounter{figure}{15}
\begin{figure*}[tb]
	\centering
	\includegraphics[trim={1cm 10cm 2cm 3cm},scale=0.63]{Chap1_ULISSE/PLOTS/Big_all}
	\caption{Exact and Approximate similarity search on Z-normalized synthetic and real datasets.
		% (ASTRO, SEISMIC), comparison between $\textit{ULISSE}$ and \textit{UCR Suite}. 
		\textit{a)} Average exact query time (CPU + disk I/O) on synthetic datasets. \textit{b)} Average exact $K-NN$ query time (CPU + disk I/O) on real datasets (100 GB) varying $K$. \textit{c)}  Average disk accesses of $K-NN$ query. \textit{d)} Indexing measures for all datasets.}
	%time of each datasets and indexes info (Number of leaf nodes and Envelopes generated).}
	\label{experimentsBig}
\end{figure*}
\setcounter{figure}{14}
\begin{figure}[htp]
	\centering
	\includegraphics[trim={0cm 13cm 11cm 3cm},scale=0.57]{Chap1_ULISSE/PLOTS/cmri_ulisse_appr}
	\caption{Approximate query answering on non Z-normalized data series. (\textit{a)} Cumulative Indexing + approximate search query time (CPU + disk I/O) of $4,000$ queries ($1,000$ per each query length in [160,192,224,256]). (\textit{b)} Approximate quality: percentage of answers in the relative exact search range.}
	\label{cmriUlisseApprox}
\end{figure}

In this subsection, we evaluate $\textit{ULISSE}$ approximate search. 
Since we compare our approach to CMRI, Z-normalization is not applied. 
Figure~\ref{cmriUlisseApprox}.a) depicts the cumulative query answering time for $4,000$ queries. 
As previously, we note that the indexing time for $\textit{ULISSE}$ is relatively very small. 
On the other hand, the time that CMRI needs for indexing is 2x more than the time during which $\textit{ULISSE}$s has finished indexing and answering $4,000$ queries.

In Figure~\ref{cmriUlisseApprox}.b), we measure the quality of the Approximate search. 
In order to do this, we consider the exact query results ranking, showing how the approximate answers are distributed along this rank, which represents the ground truth. 
We note that CMRI answers have slightly better positions than the $\textit{ULISSE}$ ones. 
This happens thanks to the tighter representation generated by the complete sliding window extraction of each subsequence, employed by CMRI. 
Nevertheless, this small penalty in precision is balanced out by the considerable time performance gains: $\textit{ULISSE}$ is up to 15x faster than CMRI. 
When we use a smaller $\gamma$, (e.g., $20$), $\textit{ULISSE}$ shows its best time performance.
This is due to tighter $Envelopes$ containment area, which permits to find a better best-so-far with a shorter tree index visit. 



\subsection{Experiments with Real Datasets}



In this last part, we test $\textit{ULISSE}$ on three large synthetic datasets of sizes \textit{100}GB, \textit{500}GB, and \textit{750}GB, as well as on two real series collections, i.e., ASTRO and SEISMIC (described earlier). The other parameters are the default ones.
For each generated index and for the \textit{UCR Suite}, we ran a set of 100 queries, for which we report the average exact search time.
In Figure~\ref{experimentsBig}.a) we report the average query answering time ($1-NN$) on synthetic datasets, varying the query length. These results demonstrate that \textit{ULISSE} scales better than \textit{UCR Suite} across all query lengths, being up to 5x faster.
% for the largest dataset, with query $160$. 
%We recall that this collection contains \textit{48} Billion patterns of length \textit{160}.   
%Indexing this dataset required Ulisse about $11$h, thus the cumulative improvement, becomes effective after five UCR suite queries.
In Figure~\ref{experimentsBig}.b), we report the $K-NN$ exact search time performance, varying $K$ and picking the smallest query length, namely \textit{160}. Note that, this is the largest search space we consider in these datasets, since each query has \textit{9.7} billion of possible candidates (subsequences of length \textit{160}).
The experimental results on real datasets confirm the superiority of \textit{ULISSE}, which scales with stable performance, also when increasing the number $K$ of nearest neighbors. Once again it is up to 5x faster than \textit{UCR Suite}, whose performance deteriorates as $K$ gets larger. 
%We solely note one exception, which concerns the search in thee ASTRO dataset, when the pattern is set to length \textit{256}. Here,for $K$ values larger than \textit{1}, the time performance sensibly deteriorates. This behavior is justified by the abrupt decreasing of the low pruning power ratio, which in the.     
%Figure~\ref{experimentsBig}.c) shows that approximate search is several orders of magnitude faster than the exact one, irrespective of the dataset.
In Figure~\ref{experimentsBig}.c) we report the number of disk accesses of the queries considered in Figure~\ref{experimentsBig}.b). Here, we are counting the number of times that we follow a pointer from an envelope to the raw data on disk, during the sequential scan in Algorithm~\ref{exactSearch}.
Note that the number of disk accesses is bounded by the total number of Envelopes, which are reported in Figure~\ref{experimentsBig}.d) (along with the number of leaves and the building time for each index).
%In the worst case across all synthetic and real datasets, \textit{ULISSE} is able to prune 76\% of the envelopes (and corresponding disk accesses), while in the best case more than 99\%.
We observe that in the worst case, which takes place for the ASTRO dataset for $K=100$, we retrieve from disk $\sim$\textit{82}\% of the total number of subsequences. This still guarantees a remarkable speed-up over \textit{UCR Suite}, which needs to consider all the raw series.
%, since it does not implement a pruning strategy beyond the Early abandoning of Euclidean distance computations. 
Moreover, since \textit{ULISSE} can use Early Abandoning during exact query answering, we observe during our empirical evaluation that disposing of the approximate answer distance prior the start of the exact search, permits to abandon on average \textit{20\%} of points more than \textit{UCR Suite} for the same query. 
%This latter is a further reason behind the speed-up that we achieve.
%With disk accesses, we are counting the number of times that we follow a pointer from an envelope in a \textit{ULISSE} leaf to the raw data on disk (refer to Figure~\ref{ULiSSE_index}), where each disk access reads a subsequence of length at most $\ell_{max}$.

%In the last set of experiments, we measure the number of leaf visits and disk accesses, respectively, performed by the approximate search in these experiments. 
%With disk accesses, we are counting the number of times that we follow a pointer from an envelope in a \textit{ULISSE} leaf to the raw data on disk (refer to Figure~\ref{ULiSSE_index}), where each disk access reads a subsequence of length at most $\ell_{max}$.
%Thus, the number of disk accesses is bounded by the total number of envelopes in the index.
%The indexing time, total number of leaves in the index, and total number of envelopes in the index,  are shown in Figure~\ref{experimentsBig}.g).
%We observe that in practice the number of leaves visited is always very small (Figures~\ref{experimentsBig}.d)), and the number of disk accesses is also a very small percentage of the total number of envelopes in the index (Figures~\ref{experimentsBig}.e)).
%Finally, Figure~\ref{experimentsBig}.f), reports the number of disk accesses that the exact search  performs in sequential manner. 
%In the worst case across all synthetic and real datasets, \textit{ULISSE} is able to prune 76\% of the envelopes (and corresponding disk accesses), while in the best case more than 99\%.



