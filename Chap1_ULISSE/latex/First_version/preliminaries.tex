
\section{Problem Formulation and Preliminaries}
\label{sec:prelim}

%\begin{definition}[Data series] 
Let a data series $D = d_1$,...,$d_{|D|}$ be a sequence of numbers $d_i \in  \mathbb{R}$, where $i \in \mathbb{N}$ represents the position in $D$. 
We denote the length, or size of the data series $D$ with $|D|$.
%\end{definition}
%\begin{definition}[Data series collection] 
%\begin{definition}[Subsequence] 
The subsequence $D_{o,\ell}$=$d_o$,...,$d_{o+\ell-1}$ of length $\ell$, is a contiguous subset of $\ell$ points of $D$ starting at offset $o$, where $1 \leq o \leq |D|$ and $ 1 \leq \ell \leq |D|-o+1$.
%\end{definition}
%{\bf ??? Di is data series i, we need a different notation for subsequence, maybe D[]? do we use this notation later in the paper? ???}
A subsequence is itself a data series.
%A data series collection $C$ = $\{D_{1},...,D_{|C|}\}$ is a set of data series.
A data series collection, $C$, is a set of data series. 
%We denote with $C^{i}$ the $i^{th}$ data series in $C$.  
%\end{definition}

We say that a data series $D$ is Z-normalized, denoted $D^{n}$, when its mean $\mu$ is $0$ and its standard deviation $\sigma$ is $1$. 
%For each series $D_i$ in $C$, we define the mean and standard deviation as $\mu_{i}$ and $\sigma_{i}$. 
The normalized version of $D = d_1,...,d_{|D|}$ is computed as follows: $D^{n} = \{\frac{d_1 - \mu}{\sigma},...,\frac{d_{|D|} - \mu}{\sigma}\}$. 
Z-normalization is an essential operation in several applications, because it allows similarity search irrespective of shifting and scaling~\cite{DBLP:journals/datamine/KeoghK03,DBLP:conf/kdd/RakthanmanonCMBWZZK12}.
%Skipping the Z-normalization step may lead, in most of the cases, to have non meaningful matches across a series dataset. \vspace{1em} 

\noindent{\bf Euclidean Distance.} Given two data series $D = d_{1},...,d_{|D|}$ and $D' = d'_{1},...,d'_{|D'|}$ of the same length (i.e., $|D| = |D'|$), we can calculate their Euclidean Distance 
%using the Euclidean metric~\cite{DBLP:conf/sigmod/MorseP07,Shieh2008,DBLP:conf/kdd/RakthanmanonCMBWZZK12}, defined 
as follows: $ED(D,D') = \sqrt{\sum_{i}^{|D|} d(d_{i},d'_{i})}$, where the distance function $d$ is applied to two real values, namely $A$ and $B$, as follows: $d(A,B) = (A-B)^2$.




\commentRed{
	
\noindent{\bf Dynamic Time Warping.} The Euclidean distance is a lock-step measure, which is computed summing up the distances between pairs of points that have the same positions in their respective series.
Therefore, considering the Euclidean distance does not account similarity that is shifted along the points position e.g., along time, in the case of time series.
To that extent, Kruskall and Liberman~\cite{DTW} provided a more elastic measure, namely the Dynamic Time Warping (DTW). Given two data series $d$ and $d'$, the DTW distance is computed by considering the differences between pairs of points $(d(d_{i},d'_{j}))$, where the indexes $i,j$ might be different.
In this manner, a particular alignment of $d$ and $d'$ is performed before to compute the distance. We define a sequence alignment as a vector of indexes pairs $A \in \mathbb{R}^{\ell \times 2}$, where $(i,j) \in A \iff 1 \le i,j \le \ell$, and $\ell$ is the length of the two series. The alignment of the Euclidean Distance is a special case, where the indexes are equal to their position in $A$.   
In the case of two series of length $\ell$, the space of the possible alignments spans the paths, which join two cells in a squared matrix composed by $\ell^2$ cells. In Figure~\ref{matrixWarping}(a) we depict an Euclidean distance alignment of two series of length \textit{4}, which exactly crosses the diagonal of the matrix, joining the cells (\textit{1,1}) and (\textit{4,4}). On the other hand, in the same figure we report another possible alignment that we call warping alignment, which deviates from the diagonal. In general we can call warping path any warping alignment interchangeably.  

\begin{figure}[tb]
	\centering
	\includegraphics[trim={0cm 6cm 17cm 3cm},scale=0.80]{Chap1_ULISSE/figures/matrixWarping}
	\caption{\textit{(a)} Euclidean and Warping alignment in a squared matrix. \textit{(b)} Valid index steps in a warping alignment.}
	\label{matrixWarping}
	%\vspace*{-0.5cm}
\end{figure}

In order to restrict the allowed paths, we can apply the following local constraints on the indexes pairs:
\begin{itemize}
	\item We require that the first and last pair of $A$ correspond respectively to the first and last pairs of points in $d$ and $d'$. If $|d|=|d'|=\ell$, we have $A[1]=(1,1)$ and $A[\ell]=(\ell,\ell)$.
	Furthermore, for any $(a,b),(c,d) \in A \iff (a \neq c) \vee (b \neq d)$. This latter, avoids to consider the same indexes pair twice in a single path. 
	\item Given $k \in \mathbb{N}$, where $1<k\le\ell$, $A[k][0] - A[k-1][0] \le 1$ and $A[k][1] - A[k-1][1] \le 1$ always hold. This restricts each index to move at most of 1 unit in its next alignment position.
    \item Moreover, we always require that $A[k][0] - A[k-1][0] \ge 0$ and $A[k][1] - A[k-1][1] \ge 0$. This guarantees a monotonic movement of the path, towards the last indexes pair. In Figure~\ref{matrixWarping}(b) we depict the three possible steps that each index pair can perform in a valid alignment.     
\end{itemize} 

These constraints permit to bound the length of an alignment between two series of length $\ell$, between $\ell$ and $2 \times \ell-1$.    
Typically, warping paths are also subject to global constraints. We can thus set their maximum deviation from the matrix diagonal. In that regard, Sakoe and Chiba~\cite{Sakoe-Chiba} and Itakura~\cite{Itakura} proposed different warping paths constraints, which restrict the matrix positions that a valid path can visit. 
The \textit{Sakoe-Chiba band}~\cite{Sakoe-Chiba} constraint allows uniformly each index of a warping path, to be at most \textit{r} points far from the diagonal (Euclidean Distance alignment). On the other hand, the \textit{Itakura-parallelogram}~\cite{Itakura} constraint allows to choose different $r$ values depending on the index position $i$. In general, $r$ is called warping window.
      
Given a valid warping path, we represent by $A^{*}$, which satisfies the previously introduced constraints, we can formally define the DTW distance between two series $d$ and $d'$ of the same length $\ell$, as $DTW(d,d') = \underset{A^{*}}{\operatorname{argmin}}(\sqrt{\sum_{i}^{|A^{*}|} d(d_{A^{*}[i][0]},d'_{A^{*}[i][1]})})$.
We note that computing the DTW distance corresponds to find the valid alignment, which minimizes the sum of the distances. 

In Figure~\ref{dtwMeasure}, we consider two series ($d$ and $d'$), which are extracted from two offsets that are \textit{5} points far away, in the same long sequence. In this manner, the prefix of $d$ is equal to the suffix of $d'$, which starts at position \textit{6}. In the plots, the values of $d$ span the right vertical axis, whereas those of $d'$ the left one. If we compute the Euclidean distance, as depicted in Figure~\ref{dtwMeasure}(a), the fixed alignment of points does not capture the similarity of the two series. On the other hand, when computing the DTW distance, the warping path aligns the two similar parts, as reported in Figure~\ref{dtwMeasure}(b). At the bottom of the figure, we also report the warping path, which is constrained by a \textit{Sakoe-Chiba band}.

}



\begin{figure}[tb]
	\centering
	\includegraphics[trim={0cm 4cm 16cm 3cm},scale=0.8]{Chap1_ULISSE/figures/dtwMeasure}
	\caption{\textit{(a)} Euclidean distance alignment between the data series $D$ and $D'$. \textit{(b)} DTW Alignment between $D$ and $D'$. In the bottom part, the path is depicted in the $|D| \times |D'|$ matrix, contoured by the Sakoe-Chiba band.}
	\label{dtwMeasure}
	%\vspace*{-0.5cm}
\end{figure}






The problem we address in this part is the following. 
%We divide the index construction task from the query answering, thus:

\begin{problem}[Variable-Length Subsequences Indexing]
	Given a data series collection $C$, and a series length range $[\ell_{min},\ell_{max}]$, we want to build an index that supports exact similarity search for queries of any length within the range $[\ell_{min},\ell_{max}]$. 
	%We denote such index with the signature $U(C,\ell_{min},\ell_{max})$.
\end{problem}



In our case similarity search is formally defined as follows:

\begin{definition}[Similarity search]
	Given a data series collection $C=\{D^{1},...,D^{C}\}$, a series length range $[\ell_{min},\ell_{max}]$, a query data series $Q$, where $\ell_{min} \le |Q| \le \ell_{max}$, and $k \in \mathbb{N}$, we want to find the set $R=\{D^{i}_{o,\ell}| D^{i} \in C \wedge \ell=|Q| \wedge (\ell+o-1) \le |D^{i}|\}$, where $|R|=k$. We require that $ \forall D^{i}_{o,\ell} \in R$ $\nexists D^{i'}_{o',\ell'}$ $s.t.$ $dist(D^{i'}_{o',\ell'},Q) < dist(D^{i}_{o,\ell},Q)$, where $\ell'=|Q|$, $(\ell'+o'-1) \le |D^{i'}|$ and $D^{i'} \in C$. We informally call $R$, the $k$ \textit{nearest neighbors} set of $Q$. Given two generic series of the same length, namely $d$ and $d'$ the function $dist(d,d') = ED()$ 
	%We denote such index with the signature $U(C,\ell_{min},\ell_{max})$.
\end{definition}

\commentRed{In this work, we perform Similarity Search using consistently Euclidean Distance (ED) or Dynamic Time Warping (DTW), as $dist$ function.}










