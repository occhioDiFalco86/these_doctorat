

\subsection{Indexing the Envelopes}

Here, we define the procedure used to index the Envelopes. 
In that regard, we aim to adapt the $iSAX$ indexing mechanism (depicted in Figure~\ref{FigurePAASAXISAX2}).

\begin{comment}
Therefore, in our specific case, internal and leaf nodes must be representative of a Envelope records bunch. In that regard, a node is assigned with 2 $iSAX$ words, which encodes all the sequences that are in turn encoded by all the records accommodated in the node.
The strategy adopted to group different envelopes in the same node may choose similarity according, either the lower or the upper bound. We consider both cases as valid solutions, since they have the same, data dependent, probability to provide a better grouping. Moreover, we note that the $iSAX$ space is n  
\noindent{\bf Encoding of subsequences.} Intuitively, $L$ and $U$ represent respectively, the lower and upper bounds of the $PAA$ words of all the series extracted by a sliding window on $D$. Formally, a subsequence $S_{a,b}$ of $D$ is said to be \textit{encoded} by a $paaENV_{[D,\ell_{min},\ell_{max},i,\gamma,s]}$, if $a \ge i$, $(a-i) \le \gamma$, $(a-b)\le \ell_{max}$, and when $\forall p_{k} \in PAA(S_{a,b})$: $p_{k} \ge L_{k}$ and $p_{k} \le U_{k}$.   
\end{comment}

Given a $paaENV$, we can translate its $PAA$ extremes into the relative iSAX representation: $uENV_{ paaENV_{[D,\ell_{min},\ell_{max},a,\gamma,s]}}=[iSAX(L),iSAX(U)]$,
where $iSAX(L)$ ($iSAX(U)$) is the vector of the minimum (maximum) $PAA$ coefficients of all the segments corresponding to the subsequences of $D$.

The \textit{ULISSE} Envelope, $uENV$, represents the principal building block of the \textit{ULISSE} index.
Note that, we might remove for brevity the subscript containing the parameters from the $uENV$ notation, when they are explicit.

\begin{figure}[tb]
	\centering
	\includegraphics[trim={0cm 10cm 18cm 3cm},scale=1]{Chap1_ULISSE/figures/Envelope_Building.pdf}
	\caption{ $uENV$ building, with input: data series $D$ of length $60$, $PAA$ segment size $=20$, $\gamma = 20$, $\ell_{min} = 40$ and $\ell_{max} = 60$.}
%\vspace*{-0.4cm}
	\label{BuildEnvelope}
\end{figure}

In Figure~\ref{BuildEnvelope}, we show a small example of envelope building, given an input series $D$. 
The picture shows the $PAA$ coefficients computation of the master series. 
They are calculated by using a sliding window starting at point $a=1$, which stops after $\gamma$ steps.
Note that the Envelope generates a containment area, which embeds all the subsequences of $D$ of all lengths in the range $[\ell_{min},\ell_{max}]$.
















\section{Indexing Algorithm}
\label{sec:ulisseIndexing}


\subsection{Indexing Non-Z-Normalized Subsequences}
We are now ready to introduce the algorithms for building an $uENV$. 
Algorithm~\ref{AlgoEnvelopeNN} describes the procedure for non-Z-normalized subsequences.  
As we noticed, maintaining the running sum of the last $s$ points, i.e., the length of a $PAA$ segment (refer to Line~\ref{runSum}), allows us to compute all the $PAA$ values of the expected envelope in $O(w(\ell_{max}+\gamma))$ time in the worst case, where $\ell_{max}+\gamma$ is the points window we need to take into account for processing each master series, and $w$ is the number of $PAA$ segments in the maximum subsequence length $\ell_{max}$. 
Since $w$, is usually a very small number (ranging between 8-16), it essentially plays the role of a constant factor. 
In order to consider not more than $\gamma$ steps for each segment position, we store how many times we use it, to update the final envelope in the vector, in Line~\ref{updatesSegment}.  
 
\begin{algorithm}[tb]	
	{\small
	\SetAlgoLined
	\label{AlgoEnvelopeNN}
	\KwIn{\textbf{float}[] $D$, \textbf{int} $s$, \textbf{int} $\ell_{min}$, \textbf{int} $\ell_{max}$, \textbf{int} $\gamma$, \textbf{int} $a$ }
	\KwOut{$\mathbf{uENV[iSAX_{min},iSAX_{max}]}$}
	
	\BlankLine
	\textbf{int} w $\leftarrow$ $\lfloor\ell_{max} / s\rfloor$ \;
	\textbf{int} segUpdateList[S] $\leftarrow$ \{0,...,0\}\;\label{updatesSegment}
	\textbf{float} $U[w]\leftarrow\{-\infty,...,-\infty\}$, $L[w]\leftarrow$ $\{\infty,...,\infty\}$\;
	\uIf{$|D|-(i-1) \geq \ell_{min}$ } 
	{
	\textbf{float} paaRSum $\leftarrow$ 0\;
	\tcp{iterate the master series.}
	\For{i $\leftarrow$ a \emph{\KwTo} min($|D|$,$a+\ell_{max}+\gamma$) } 
	{	\tcp{running sum of paa segment}
	   paaRSum $\leftarrow$ paaRSum + D[i]\; 	\label{runSum}
		\If{(j-a) $>$ $s$}
		{
			paaRSum $\leftarrow$ paaRSum - D[i-s]\;\label{runSumUpdate}
		}
		\For{z $\leftarrow$ 1 \emph{\KwTo} min($\lfloor$[i-(a-1)] $/$ s$\rfloor$,w)}
		{
			\If{segUpdatedList[z] $\le$ $\gamma$}
			{
				segUpdateList[z] ++\;
				\textbf{float} paa $\leftarrow$ (paaRSum $/$ s)\;
					$L[z]$ $\leftarrow$ $min(paa,L[z])$\;    
					$U[z]$ $\leftarrow$ $max(paa,U[z])$\;
	  } 	
		}
	}
	$\mathbf{uENV}$ $\leftarrow$ [iSAX($L$),iSAX($U$)];\                                  
	}
	\Else{
		$\mathbf{uENV}$ $\leftarrow \emptyset$\;
	}
}
	\caption{$uENV \, computation$}
\end{algorithm}
















\subsection{Indexing Z-Normalized Subsequences}


In Algorithm~\ref{algoEnvNorm}, we show the procedure that computes an indexable Envelope for Z-normalized sequences, which we denote as $uENV_{norm}$. 
This routine iterates over the points of the overlapping subsequences of variable length (\textit{First loop} in Line~\ref{loopMasterSeries}), and performs the computation in two parts. 
The first operation consists of computing the sum of each $PAA$ segment we keep in the vector $PAAs$ defined in Line~\ref{vectorSegmentSum}. When we encounter a new point, we update the sum of all the segments that contain that point (Lines~\ref{startUpdSeg}-~\ref{endUpdSeg}).
The second part, starting in Line~\ref{loopNonMasterandMasterSeries} (\textit{Second loop}), performs the segment normalizations, which depend on the statistics (mean and std.deviation) of all the subsequences of different length (master and non-master series), in which they appear.
During this step, we keep the sum and the squared sum of the window, which permits us to compute the mean and the standard deviation in constant time (Lines~\ref{mean},\ref{gamma}). We then compute the Z-normalizations of all the $PAA$ coefficients in Line~\ref{Z_normCoefficient}, by using Equation~\ref{setPaaNorm}.  

\begin{algorithm}[!tb]
	{\footnotesize
		\SetAlgoLined
		\label{algoEnvNorm}
		\KwIn{\textbf{float}[] $D$, \textbf{int} $s$, \textbf{int} $\ell_{min}$, \textbf{int} $\ell_{max}$, \textbf{int} $\gamma$, \textbf{int} a }
		\KwOut{$\mathbf{uENV_{norm}[iSAX_{min},iSAX_{max}]}$}
		\BlankLine
		
		\textbf{int} w $\leftarrow$ $\lfloor\ell_{max} / s\rfloor$ \; \label{segmentsNumber}
		\tcp{sum of PAA segments values }
		\textbf{float} PAAs [$\ell_{max} + \gamma - (s-1)$] $\leftarrow$ \{0,...,0\}\; 
		\label{vectorSegmentSum}
		\textbf{float} $U[w]\leftarrow\{-\infty,...,-\infty\}$, $L[w]\leftarrow \{\infty,...,\infty\}$\;
		\uIf{$|D|-(a-1)$ $\geq$ $\ell_{min}$} 
		{
			\textbf{int} nSeg$\leftarrow$ 1\;
			\textbf{float} accSum, accSqSum $\leftarrow$ 0\;
			%\textbf{float} accSumPS, accSqSumPS $\leftarrow$ 0\;
			\tcp{First loop: Iterate the points.}
			\For{i $\leftarrow$ a \emph{\KwTo} min($|D|$,(a+$\ell_{max}$+$\gamma$))} 
			{\label{loopMasterSeries}
				\tcp{update sum of PAA segments values}
				\If{$i-a>$ s}
				{\label{startUpdSeg}
					nSeg++\;
					PAAs[nSeg] $\leftarrow$ PAAs[nSeg-1] - D[i-s]\; 
				}
				PAAs[nSeg] += D[i]\; \label{endUpdSeg}
				\tcp{keep sum and squared sum.}
				accSum 	+= D[i], accSqSum += (D[i])$^2$\;\label{sumSquaredSum}	
				\tcp{the window contains enough points.}		
				\If{i-(a-1) $\geq \ell_{min}$}
				{
					acSAc $\leftarrow$ accSum, 
					acSqSAc $\leftarrow$ accSqSum\;
					\textbf{int} nMse $\leftarrow$ min($\gamma$+1,(i-(a-1)-$\ell_{min})+1$)\;
					\tcp{Second loop: Normalizations of PAA coefficients.}
					\For{j $\leftarrow$ 1 \emph{\KwTo} nMse }
					{\label{loopNonMasterandMasterSeries}
						\textbf{int} wSubSeq $\leftarrow$ i-(a-1)-(j-1) \;			
						\If{wSubSeq $\leq \ell_{max}$}
						{
							\textbf{float} $\mu$ $\leftarrow$acSAc/wSubSeq\; \label{mean}
							\textbf{float} $\sigma$ $\leftarrow$ \label{gamma} $\sqrt{(\frac{acSqSAc}{wSubSeq} -\mu^2)}$\;					
							\textbf{int} nSeg $\leftarrow \lfloor$wSubSeq$\div s \rfloor $\;
							\For{z $\leftarrow$ 1 \emph{\KwTo} nSeg }
							{
								\textbf{float} a $\leftarrow$ PAAs[j+[(z-1)$\times$s]]\; 
								\textbf{float} b $\leftarrow$ s$\times\mu$\;  
								\textbf{float} paaNorm $\leftarrow$ $\frac{((a-b) / \sigma)}{s}$\;	\label{Z_normCoefficient}
								$L[z]$ $\leftarrow$ $min(paaNorm,L[z])$\;    
								$U[z]$ $\leftarrow$ $max(paaNorm,U[z])$\;
								\label{endComputingNormalization}			
							}
						}
						acSAc -= D[j], acSqSAc -= (D[j])$^2$\;
					}
				}	
			}
			
			$\mathbf{uENV_{norm}}$ $\leftarrow$ [iSAX($L$),iSAX($U$)];\                                  
		}
		\Else{
			$\mathbf{uENV_{norm}}$ $\leftarrow \emptyset$\;
		}
	}
	\caption{$uENV_{norm} \, computation$ }

\end{algorithm}


\begin{figure}[tb]
	\centering
	\includegraphics[trim={0cm 9.5cm 17cm 3cm},scale=1]{Chap1_ULISSE/figures/exAlgo2}
	\caption{Running example of Algorithm~\ref{algoEnvNorm}. \textit{Left column}) Points iteration. \textit{Right column}) Statistics update at each step.}
	%\vspace*{-0.4cm}
	\label{exAlgo2}
\end{figure}


In Figure~\ref{exAlgo2}, we show an example that illustrates the operation of the algorithm.
% depicting some steps of a $uENV_{norm}$ computation. 
In \textit{1}, the \textit{First loop} has iterated over \textit{8} points (marked with the dashed square). Since they form a subsequence of length $\ell_{min}$, the \textit{Second Loop} starts to compute the Z-normalized PAA coefficients of the two segments, computing the mean and the standard deviation using the sum ($acSAc$) and squared sum ($acSqAc$) of the points considered by the \textit{First loop} (gray circles).
The second step takes place after that the \textit{First Loop} has considered the $9^{th}$ point (black circle) of the series. Here, the \textit{Second Loop} updates the sum and the squared sum, with the new point, calculating then the corresponding new Z-normalized PAA coefficients. At step \textit{3}, the algorithm considers the second subsequence of length $\ell_{min}$, which is contained in the nine points window.
The \textit{Second Loop} considers in order all the overlapping subsequences, with different prefixes and length.
This permits to update the statistics (and all possible normalizations) in constant time.
The algorithm terminates, when all the points are considered by the \textit{First loop}, and the \textit{Second Loop} either encounters a subsequence of length $\ell{min}$ (as depicted in the step \textit{15}), or performs at most $\gamma$ iterations, since all the subsequences starting at position $a+\gamma+1$ or later (if any) will be represented by other Envelopes.   
%We iterate over those series, which are at most $\gamma$ subsequences of the current master series, in the loop of Line~\ref{loopNonMasterandMasterSeries}. 













\subsubsection{Complexity Analysis} \textit{}\\
 Given $w$, the number of PAA segments in the window of length $\ell_{max}$, and $M=\ell_{max}-\ell_{min}+\gamma$, the number of master series we need to consider, building a normalized Envelope, $uENV_{norm}$, takes $O(M \gamma w$) time.\\ %On the other hand, the time taken by a non normalized Envelope construction, namely $uENV$, is linear, since we only need to iterate over the window of size $\ell_{max} + \gamma$.


%In practice, this complexity may be reduced to $O(\ell_{max}^2)$, since generally $\gamma w$ $>$ $\ell_{max}$, and $M$ is in the order of magnitude as $\ell_{max}$ and $\ell_{max}$. 
%For example, picking $w = 8$, which is a common default value, $\ell_{min} = 128$ and $\ell_{max} = 256$, if we want to build only a single envelope for a given series, whose length is greater or equal to $256$, gamma must be at least $128$.

%Despite the quadratic complexity, which may suggest a negative impact on the indexing operations, we still rely on the benefit of our contribution, concerning the mitigation of the space explosion depicted in Figure~\ref{spaceExplosionVL}. We will show the relative results in the Experimental Evaluation section.














\subsection{Building the index}

\begin{algorithm}[!tb]
	\SetAlgoLined
	{\small
	\label{algoBuildUlisse}
	\KwIn{\textbf{Collection} $C$, \textbf{int} $s$, \textbf{int} $\ell_{min}$, \textbf{int} $\ell_{max}$, \textbf{int} $\gamma$, \textbf{bool} $bNorm$ }
	\KwOut{\textbf{\textit{ULISSE} index} I}
	\ForEach{D \textbf{in} C}
	{
		$\mathbf{int} a' \leftarrow \emptyset$\;
		$\mathbf{uENV}$ $E \leftarrow \emptyset$\;
		\While{true}
		{	
			\uIf{bNorm}
			{
				$E \leftarrow uENV_{norm}(D,s,\ell_{min},\ell_{max},\gamma,a')$\;
			}
			\Else
			{
				$E \leftarrow uENV(D,s,\ell_{min},\ell_{max},\gamma,a')$\;
			}	
			$a' \leftarrow a' + \gamma + 1$ \;
			\If{$E == \emptyset$}
			{
				\textbf{break}\;
			}
			$bulkLoadingIndexing(I,E)$\;\label{bulkloading}
			$I.inMemoryList.add(maxCardinality(E))$\;\label{listEvelope}
			
		}
	}  
}
	\caption{\textit{ULISSE index computation}}	
\end{algorithm}

We now introduce the algorithm, which builds a \textit{ULISSE} index upon a data series collection. 
We maintain the structure of the $iSAX$ index~\cite{DBLP:journals/kais/CamerraSPRK14}, introduced in the preliminaries. 

Each \textit{ULISSE} internal node stores the Envelope $uENV$ that represents all the sequences in the subtree rooted at that node. 
Leaf nodes contain several Envelopes, which by construction have the same $iSAX(L)$. 
On the contrary, their $iSAX(U)$ varies, since it get updated with every new insertion in the node. 
(Note that, inserting by keeping the same $iSAX(U)$ and updating $iSAX(L)$ represents a symmetric and equivalent choice.)
%The internal nodes are split on segments belonging to the $iSAX$ word utilized to insert the Envelopes in the nodes. 

In Figure~\ref{ULiSSE_index}, we show the structure of the \textit{ULISSE} index during the insertion of an Envelope (rectangular/yellow box). 
Note that insertions are performed based on $iSAX(L)$ (underlined in the figure). 
Once we find a node with the same $iSAX(L)=(1-0-0-0)$ (Figure~\ref{ULiSSE_index}, $1^{st} step$)
%, following the first dashed arrow), 
if this is an inner node, we descend its subtree (always following the $iSAX(L)$ representations) until we encounter a leaf.
%, still with the same $iSAX(L)$ (following the $2^{nd}$ dashed arrow). 
During this path traversal, we also update the $iSAX$ representation of the Envelope we are inserting, by increasing the number of bits of the segments, as necessary. 
In our example, when the Envelope arrives at the leaf, it has increased the cardinality of the second segment to two bits: $iSAX(L)=(1-${\bf 10}$-0-0)$, and similarly for $iSAX(U)$ (Figure~\ref{ULiSSE_index}, $2^{nd} step$).
Along with the Envelope, we store in the leaf a pointer to the location on disk for the corresponding raw data series.
We note that, during this operation, we do not move any raw data into the index.

To conclude the insertion operation, we also update the $iSAX(U)$ of the nodes visited along the path to the leaf, where the insertion took place.
In our example, we update the upper part of the leaf Envelope to $iSAX(U)=(1-${\bf 11}$-0-0)$, as well as the upper part of the Envelope of the leaf's parent to $iSAX(U)=(1-${\bf 1}$-0-0)$ (Figure~\ref{ULiSSE_index}, $3^{rd} step$).
This brings the \emph{ULISSE} index to a consistent state after the insertion of the Envelope.

Algorithm~\ref{algoBuildUlisse} describes the procedure, which iterates over the series of the input collection $C$, and inserts them in the index. 
Note that function $bulkLoadingIndexing$ in Line~\ref{bulkloading} may use different bulk loading techniques. In our experiments, we used the iSAX 2.0 bulk loading algorithm \cite{DBLP:conf/icdm/CamerraPSK10}.
% (e.g., the technique proposed in~\cite{DBLP:conf/sigmod/ZoumpatianosIP14}). 
Alongside the index, we also keep in memory (using the raw data order) all the Envelopes, represented by the symbols of the highest $iSAX$ cardinality available (Line~\ref{listEvelope}). 
This information is used during query answering.

\begin{figure}[tb]
	\centering
	\includegraphics[trim={0cm 8cm 11cm 3cm},scale=1]{Chap1_ULISSE/figures/ULiSSE_index.pdf}
	\caption{Envelope insertion in an \textit{ULISSE} index. $iSAX(L)$ is chosen to accommodate the Envelopes inside the nodes.}
	\label{ULiSSE_index}
\end{figure}



\subsubsection{Space complexity analysis}
The index space complexity is equivalent for the case of Z-normalized and non Z-normalized sequences. The choice of $\gamma$ determines the number of $Envelopes$ generated and thus the index size.
Hence, given a data series collection $C=\{D^{1},...,D^{|C|}\}$ the number of extracted Envelopes is given by $N= (\sum_{i}^{|C|} \lfloor \frac{|D^{i}|}{\ell_{min} + \gamma}\rfloor$). 
If $w$ PAA segments are used to discretize the series, each $iSAX$ symbol is represented by a single byte (binary label) and the disk pointer in each Envelope occupies $b$ bytes (in general \textit{8} bytes are used). The final space complexity is $O((2w)bN)$.


