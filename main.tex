% Packages de base
\documentclass[12pt,a4paper]{book}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\setlength{\parskip}{\baselineskip}%
\setlength{\parindent}{0pt}%


% Redimensionner les marges
\usepackage[marginpar=2cm, top=4cm, bottom=5cm]{geometry}


\usepackage[english]{babel}
\newenvironment{vplace}[1][1]
{\par\vspace*{\stretch{#1}}}
{\vspace*{\stretch{1}}\par}

%\FrenchFootnotes
%\AddThinSpaceBeforeFootnotes % A insérer si on utilise le package [french]{babel}. Permet de mieux respecter les conventions françaises des notes de bas de page.


%Packages complémentaires (AMS)
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
%Ecrire des formules chimiques
\usepackage{chemist}
%Si vous souhaitez renvoyer toutes les notes en fin de document (non recommandé pour une thèse), utilisez le package \endnotes
% Pour faire apparaître index, table des figures et liste des tables dans la table des matières.
\usepackage{tocbibind}

% Pour créer un index
\usepackage{makeidx}
\makeindex
% Pour inclure des images
\usepackage{graphicx}
% Pour inclure des PDF
\usepackage{pdfpages}
% Améliore l'esthétique de la police
\usepackage{lmodern}
%Packages pour créer des tableaux 
\usepackage{longtable} % Pour des tableaux dont la longueur dépasse une feuille A4
\usepackage{tabularx} % Pour des tableaux à largeur définie
\usepackage{array} % Pour améliorer la qualité typographique des tableaux.
\usepackage{lipsum} % Texte latin "Lorem ipsum" (utilisé pour les tests) généré automatiquement
%\usepackage{cite}
\usepackage[hidelinks]{hyperref}


\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{comment}
\usepackage{footmisc}
\usepackage{url}
\usepackage[noend,linesnumbered,algoruled,boxed,lined]{algorithm2e}
%\usepackage[usenames,dvipsnames,svgnames,table]{xcolor}


\newtheorem{example}{Example}
\newtheorem{problem}{Problem}
\newtheorem{theorem}{Theorem}
\newtheorem{proposition}{Proposition}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{proof}{Proof}
\newtheorem{property}{Property}
\newtheorem{constraint}{Constraint}

% specific defintions used in the chapters

\newcommand{\midtilde}{\raisebox{-0.25\baselineskip}{\textasciitilde}}

\newcommand{\topkmd}{\textit{Top-k m\textsuperscript{th}} }
\newcommand{\topkfirstd}{\textit{Top-k 1\textsuperscript{st}} }

\newcommand{\VALMOD}{{\textit{VALMOD}} }
\newcommand{\VALMAP}{{\textit{VALMAP}} }


\newcommand{\commentRed}[1]{{\color[rgb]{1,0,0}#1}}
\newcommand{\commentBlue}[1]{{\color[rgb]{0,0,1} ~#1}}

\newcommand{\qedsymbol}{$\blacksquare$}

%\usepackage[backend=biber]{biblatex}
\sloppy

\begin{document}
%Mettre la page de titre correspondante
\input{titre-doctorat}

\pagenumbering{gobble}



\newpage

\noindent
\textbf{Titre: Recherche de similarité de longueur variable pour l'analyse de grands séries temporelles: Appariement de séquences, Recherche de Motifs et Anomalies}
\vskip 1cm
\noindent % Supprime le retrait de paragraphe
\textbf{Résumé (français) :}

Les séries de données ou série chronologique (suite de valeurs numériques représentant l'évolution d'une quantité) sont devenues l’un des types de données les plus importants et les plus populaires, omniprésents dans presque tous les domaines scientifiques.
Au cours des deux dernières décennies, mais de manière encore plus évidente au cours de cette dernière période, l’intérêt porté à ce type de données s’accroît rapidement.
La raison en est principalement due aux récents progrès des technologies de détection, de mise en réseau, de traitement de données et de stockage, qui ont considérablement aidé le processus de génération et de collecte de grandes quantités de séries de données.

La recherche de similarité de séries de données est devenue une opération fondamentale au cœur de plusieurs algorithmes d'analyse et applications liées aux collections de séries de données. De nombreuses solutions à différents problèmes d’exploration de données, telles que le regroupement (clustering), la mise en correspondance des sous-séquences (subsequence matching), l’imputation des valeurs manquantes (imputation of missing values), la découverte de motifs (motif discovery) et la détection d’anomalies (discord discovery) sont basés sur l'utilisation de la recherche de similarité.

À cet égard, toutes les solutions sur mesure pour les problèmes susmentionnés nécessitent la connaissance préalable de la longueur de la série, sur laquelle une recherche de similarité est effectuée.
Dans ce scénario, l'utilisateur doit connaître la longueur des résultats attendus, ce qui est souvent une hypothèse irréaliste.
Cet aspect est donc très important. Dans plusieurs cas, la longueur est un paramètre critique qui influence sensiblement la qualité du résultat final.

En détail, nous avons noté que les index de séries de données permettent d'effectuer une recherche de similarité rapide. Néanmoins, tous les index existants ne peuvent répondre qu'aux requêtes d'une seule longueur (fixées au moment de la construction de l'index), ce qui constitue une limite sévère.
Dans cette thèse, nous proposons d’abord \textit{ULISSE}, la première index de série de données conçue pour répondre aux requêtes de recherche de similarité de \emph{longueur variable}.
Notre contribution est double.
Premièrement, nous introduisons une nouvelle technique de représentation, qui résume efficacement et succinctement plusieurs séquences de différentes longueurs.
Sur la base de l'index proposé, nous décrivons des algorithmes efficaces pour la recherche de similarité approximative et exacte, combinant des visites d'index sur disque et des analyses séquentielles en mémoire.
Notre approche prend en charge les séquences non normalisées et normalisées, et peut être utilisée sans modification avec la distance Euclidienne et le déformation temporelle dynamique (DTW), pour répondre aux requêtes de type: \emph{k-NN} et $\epsilon$-range.
Nous évaluons notre approche de manière expérimentale en utilisant plusieurs jeux de données synthétiques et réels.
Les résultats montrent que \textit{ULISSE} s'est révélé de nombreuses fois plus efficace en terme de coût d'espace et de temps, par rapport aux approches concurrentes.

Par la suite, nous introduisons un nouveau \emph{framework}, qui fournit un algorithme de recherche exacte de motifs (séquences fréquentes) et d'anomalies, qui trouve efficacement tous les motifs et les anomalies de tailles différentes.
L’évaluation expérimentale que nous avons effectuée sur plusieurs ensembles de données réelles montre que nos approches sont jusqu’à des ordres de grandeur plus rapides que les alternatives.
Nous démontrons en outre que nous pouvons supprimer la contrainte irréaliste d’effectuer des analyses en utilisant une longueur prédéfinie, ce qui conduit à des résultats plus intuitifs et exploitables, qui auraient autrement été manqués.

\textbf{Mots-clés (français): Série de données, Recherche de Similarité,  Longueur Variable, Lower-bounding, Subsequence Matching, Récherche de Motifs, Récherche d'Anomalies}
\vskip 1cm

\newpage

\noindent
%\textbf{Title: Variable Length Similarity Search for Very Large Data Series: Subsequence Matching, Motif and Discord Detection}
\vskip 1cm
\noindent



\noindent % Supprime le retrait de paragraphe
\textbf{Abstract:}

Data series (ordered sequences of real valued points, a.k.a. time series) has become one of the most important and popular data-type, which is present in almost all scientific fields. 
For the last two decades, but more evidently in this last period the interest in this data-type is growing at a fast pace. 
The reason behind this is mainly due to the recent advances in sensing, networking, data processing and storage technologies, which have significantly assisted the process of generating and collecting large amounts of data series.

Data series \emph{similarity search} has emerged as a fundamental operation at the core of several analysis tasks and applications related to data series collections. Many solutions to different data mining problems, such as Clustering, Subsequence Matching, Imputation of Missing Values, Motif Discovery, and Anomaly detection work by means of \emph{similarity search}.

Data series indexes have been proposed for fast similarity search. Nevertheless all existing indexes can only answer queries of a single length (fixed at index construction time), which is a severe limitation.
In this regard, all solutions for the aforementioned problems require the prior knowledge of the series length, on which \emph{similarity search} is performed.
Consequently, the user must know the length of the expected results, which is often an unrealistic assumption.
This aspect is thus of paramount importance. In several cases, the length is a critical parameter that heavily influences the quality of the final outcome.  

In this thesis, we propose scalable solutions that enable variable-length analysis of very large data series collections. 
We propose \textit{ULISSE}, the first data series index structure designed for answering similarity search queries of \emph{variable length}. 
Our contribution is two-fold.
First, we introduce a novel representation technique, which effectively and succinctly summarizes multiple sequences of different length. 
Based on the proposed index, we describe efficient algorithms for approximate and exact similarity search, combining disk based index visits and in-memory sequential scans. 
Our approach supports non Z-normalized and Z-normalized sequences, and can be used with no changes with both Euclidean Distance and Dynamic Time Warping, for answering both \emph{k-NN} and $\epsilon$-range queries.
We experimentally evaluate our approach using several synthetic and real datasets. 
The results show that \textit{ULISSE} is several times, and up to orders of magnitude more efficient in terms of both space and time cost, when compared to competing approaches.

Subsequently, we introduce a new framework, which provides an exact and scalable motif and discord discovery algorithm that efficiently finds all motifs and discords in a given range of lengths. 
The experimental evaluation we conducted over several diverse real datasets show that our approaches are up to orders of magnitude faster than the alternatives. 
We moreover demonstrate that we can remove the unrealistic constraint of performing analytics using a predefined length, leading to more intuitive and actionable results, which would have otherwise been missed.
 
\vskip 1cm
\noindent
\textbf{Keywords : Data Series, Similarity Search, Variable Length, Lower-bounding, Subsequence Matching, Motif Discovery, Discord Discovery}
 




%Dédicace
\newpage
\begin{vplace}[0.3]
	\emph{Vita brevis, ars longa, occasio praeceps, experimentum periculosum, iudicium difficile (Hipócrates)}
	\vskip 1cm
	
	\emph{Life is short, and art long, opportunity fleeting, experimentations perilous, and judgment difficult. (Hippocrates)}
	\vskip 1cm
	
	\emph{Art long, vitalité brève, occasion précipitée, expérimentation périlleuse, jugement difficile (Hippocrate)}
\end{vplace}

%Remerciements

\newpage

\section*{Acknowledgment}

The last drop of this delicious Japanese whiskey, which my friend Federico offered me is gone. 
The retailers have already pulled the shutters down, and the bars are full of people talking. 
Tonight Marine is not here with me, but soon she will be back home.
Many other things are far away, and perhaps too far. 

Once again, another Parisian night is going to start; I can feel it. 
I can only hear a few cars on the street, and my living room is quiet.
My cigarette is still smoking in the ashtray, and in a while it will be off.
Now, my thoughts are running fast: \textit{Verba volant, Scripta manent}.

During my entire life, many wise people have been telling me that achieving important goals requires a lot of "good" work and "luck" as well. 
At this point, looking back a bit and considering my personal story, I can surely say that back in 2011, I have been lucky the day I met Themis. He has been not only a great academic supervisor, a mentor full of wisdom, \textit{savoir-faire}, and a big dose of \textit{savoir-être}. 
Themis has been somebody beyond that. 
He is the person who taught me a lot through his actions and his suggestions, both in my academic career and more generally in my life.
Many scientists might agree that good work is the result of excellence, diligence, and perseverance.
I found all of these principles in the guidance of Themis.

\textit{I cannot see anybody helping me better than Themis, when I was not able to set my milestones, in the jungle of my discoveries.}
\textit{I cannot see anybody better than Themis, pushing me that right when motivation was low.}
\textit{I cannot see anybody else like Themis, explaining me with endless patience an ocean of details that make the difference.}
\textit{I cannot justify better a few regrets of mine today. If I had listened to you more, I could have done doubtlessly more. I am sure that this will help me to improve in the future.
Each moment we spent working together was full of insights and great take away messages. It was an honor to be, and I will always be your student. Thank you Themi!}

Going at the other side of the ocean, and deep in the south-west, I cannot forget to mention Eamonn Keogh and his former P.h.D. student Yan Zhu. 
I spent three great months at U.C. Riverside in Keogh's lab. There, I had the possibility to work in a magic environment: the home of the time series mining research. \textit{Thank you guys!}  

Among the faculty members of Paris Descartes, I cannot forget to cite and thank (a lot) Pavlos Moraitis, and Salima Benbernou. 
First of all, for their warm welcome on my first day, which helped me to feel very fast a member of the \textit{LIPADE family}.
Second, for their essential support and help during these last years.

Last but not least (yet among faculties), I would like to thank my advisors and colleagues at I.U.T. Paris Descartes: The director Xavier Sense, Mourad Ouziri, Pascal Poullard, Veronique Heiwy, Hassine Moungla, Jerome Fessy, and Denis Poitrenaud. Thank you for your help, support, time, and trust in me.
I enjoyed this last year I spent with you as a teaching assistant. It was a very enriching and challenging experience!  

I would also like to thank a lot the reviewers of my dissertation: Johann Gamper and Panagiotis Papapetrou, who accepted to review this manuscript respecting a very tight (prohibitive) deadline for the submission of the final report.

I guess that these last four years were the most emotional ones of my life. 
For some unexplainable reasons, they coincide with the period, where I was a Ph.D. student, and for the first time quite far from home.
Since the beginning of this journey, I rapidly understood that a Ph.D. student is not only a Ph.D. student when he/she is in the corner of a big and dusty open space of the lab. 
A Ph.D. student is a Ph.D. student 24 hours per day, and a person who needs to think a lot.
In this sense, critical reading and thinking are the keys to acquiring knowledge and looking for improvements. 
The more we do it, the better we can hope to perform.
Given these premises, it is clear that the environment where I lived and the people who surrounded me have played a fundamental role for me.
For this reason, I want to lovely thank everyone supported me, while I was working at my research.
Hence, I would like to mention all my colleagues at Paris Descartes: Fedrico, Paul, Botao, Sabiha, Wissam, Anna, Cherifa, Heloise, Mohamed, and Khodor.  
\textit{It has been a pleasure to share with you guys the same roof, door, open-space, and interesting discussions at the lab.}

The second half of the day for a Ph.D. student starts when he/she goes back home.
This last year was the most important and full of work for me and I could not be luckier than I was once back home, where I had a lot of wonderful time with Marine.
Her support, help, suggestions, love, and patient have been among the most beautiful gifts I could ever ask.
\textit{I love you ma cocotte!}

I would finally reserve a special thank to my family and friends that have been far, while I was doing my Ph.D.  
I am immensely grateful for your support, encouragement, love, and constant presence.
Even though we cannot see each other as we would, there is always a space in my thoughts for you.
\textit{I never dared to tell you that I miss you all!}

Paris, August 2019

Michele Linardi


\section*{Remerciements}
%\addcontentsline{toc}{chapter}{Remerciements}


Je viens de terminer mon verre de ce délicieux whisky japonais que mon pote Federico m'a offert.
Les commerçants ont déjà baissé leur stores et les cafés sont pleins de gens qui bavardent.
Ce soir, Marine n’est pas là avec moi, mais elle sera bientôt de retour.
Beaucoup d'autres choses sont loin et peut-être trop loin.
Une autre nuit parisienne commence; Je peux le sentir.
Je n'entends que quelques voitures dans la rue et mon salon est calme.
Ma cigarette fume encore dans le cendrier et dans un moment elle sera éteinte.
Maintenant, mes pensées vont vites: \textit{Verba volant, Scripta manent}.

Tout au long de ma vie, de nombreuses personnes m'ont dit que pour atteindre des objectifs importants, il faut beaucoup de "bon" travail et de la "chance" aussi.
En regardant un peu en arrière, je peux sûrement dire qu’en 2011, j’ai eu de la chance le jour où j’ai rencontré Themis.
Il a été un excellent encadrant plein de sagesse, de \textit{savoir-faire}, et une bonne dose de \textit{savoir-être}.
Il m'a également beaucoup appris tant dans ma carrière universitaire que plus généralement dans ma vie.
De nombreux scientifiques conviendront peut-être qu'un travail de qualité est le résultat de l'excellence, de la diligence et de la persévérance.
J'ai trouvé tous ces principes dans les conseils de Thémis.

\textit{Personne ne m'a aidé mieux que Themis dans la jungle de mes découvertes.}
\textit {Personne ne m'a poussé comme il l'a fait lorsque ma motivation était faible.}
\textit {Personne comme Themis ne m'a expliqué avec une patience infinie, un océan de détails qui font vraiment la différence.}
\textit {Je ne peux pas mieux justifier quelques-uns de mes regrets aujourd'hui. Si je t’avais écouté en peux plus, j'aurais sans doute pu faire plus. Je suis sûr que cela m'aidera à m'améliorer à l'avenir. Chaque moment de travail meme les plus difficiles ont été vraiment formateurs. C’était un plaisir d'être ton étudiant et je le serai toujours.}
\textit{Merci Themi!}

En allant de l’autre côté de l’océan dans le profond sud-ouest, je ne peux pas oublier de mentionner Eamonn Keogh et son ancienne doctorante Yan Zhu.
J'ai passé trois mois formidables à l'Université de Riverside chez le labo de Eamonn Keogh. Ici, j’ai eu la possibilité de travailler dans un environnement magique: le temple de la recherche sur l'exploration de séries temporelles. \textit{Merci les gars!}

Parmi les professeurs de Paris Descartes, je ne peux pas oublier de citer et remercier (beaucoup) Pavlos Moraitis et Salima Benbernou.
Tout d’abord, pour leur accueil chaleureux à mon arrivée dans le labo, ce qui m’a permis de me sentir très vite membre de la famille \textit{LIPADE}.
Deuxièmement, pour leur soutien essentiel et leur aide durant ces dernières années.

Je voudrais aussi remercier mes collègues et encadrants de l’IUT Paris Descartes: le directeur Xavier Sense, Mourad Ouziri, Pascal Poullard, Véronique Heiwy, Hassine Moungla, Jérôme Fessy et Denis Poitrenaud. Merci pour votre aide, votre soutien, votre temps et votre confiance en moi.
J'ai énormément apprécié cette année passée avec vous en tant qu'assistant d'enseignement. C’etait une expérience très enrichissante et stimulante!

Je tiens à remercier également les rapporteurs de ma thèse: Johann Gamper et Panagiotis Papapetrou, qui ont accepté de réviser ce manuscrit en respectant un délai très serré pour la présentation du rapport final.

Je suis sûr que ces quatre dernières années ont été les plus émotionnelles prenantes de ma vie.
Pour plusieurs raisons inexplicables, elles coïncident avec la période où j'étais doctorant et pour la première fois assez loin de chez moi.
Depuis le début de ce voyage, j'ai tout de suite compris qu’un doctorant n’est pas seulement un doctorant quand il/elle est dans son bureau à la fac.
Un doctorant reste un doctorant 24 heures sur 24 car il/elle est une personne qui a besoin de réfléchir énormément.
En ce sens, la lecture critique et la réflexion sont les clés pour acquérir des connaissances et rechercher des améliorations.
Plus nous le faisons, mieux nous pouvons espérer de réussir dans notre intention.
Il est donc clair que l'environnement dans lequel j’ai vécu et mon entourage ont été fondamentaux pour moi.
Pour cette raison, je voudrais mentionner et remercier d’abord mes collègues de Paris Descartes: Fedrico, Paul, Botao, Sabiha, Wissam, Anna, Cherifa, Héloïse, Mohamed et Khodor.
\textit{C’était un plaisir de partager avec vous beaucoup de temps et des discussions très intéressantes au labo.}

La seconde moitié de la journée pour un doctorant commence lorsqu'il rentre chez lui.
Cette dernière année a été la plus importante et la plus riche en travail pour moi. Cependant je ne pouvais pas être plus chanceux que je ne l'étais une fois à la maison, où j'ai passé beaucoup de temps merveilleux avec Marine.
Son soutien, son aide, ses suggestions, son amour et sa patience ont été parmi les plus beaux cadeaux que j'ai jamais pu demander.
\textit{Je t'aime ma cocotte!}

Je voudrais enfin réserver un remerciement particulier à ma famille et à mes amis qui ont été loin pendant ces dernières années.
Je vous suis extrêmement reconnaissant pour votre soutien, vos encouragements, votre amour et votre présence constante.
Même si nous ne pouvons pas nous voir comme nous le voudrions, il y a toujours une place pour vous dans mes pensées .
\textit {Je n'ai jamais osé vous le dire mais vous me manquez énormément !}

Paris, Août 2019

Michele Linardi



\newpage


\pagenumbering{arabic}
\thispagestyle{empty}


%Table des matières
\tableofcontents

%Table des figures
\listoffigures



%Liste des tableaux
%\listoftables

%Avertissement
%





 

%
%\vspace{2cm}
%
%\newpage %Pour commencer sur une nouvelle page
%\thispagestyle{empty}
%\begin{center}
%\large{\textbf{Remerciements}}
%
%Love and gratitude first! Especially to whom is reading this manuscript!
%TBD
%\end{center}
%\vspace{2cm}


%Introduction
\chapter{Introduction}
%\addcontentsline{toc}{chapter}{Introduction} %Pour que l'introduction apparaisse dans la table des matières alors qu'elle n'est pas numérotée (* = non numérotée)
\input{introduction_Thesis}

\chapter{Related work}
\label{RW}
\input{Thesis_RW}


\chapter{Scalable Data Series Subsequence Matching}
\label{chapterULISSE}

\input{Chap1_ULISSE/latex/Introduction}
\input{Chap1_ULISSE/latex/preliminaries}
\input{Chap1_ULISSE/latex/Sec3}
\input{Chap1_ULISSE/latex/Sec3_indexing}
\input{Chap1_ULISSE/latex/Sec4}
\input{Chap1_ULISSE/latex/Experiments}

\chapter[Scalable VAriable-Length Similarity Search Suite]{VALS: Scalable VAriable-Length Similarity Search Suite}
\label{chapterULISSEDemo}
\input{Chap1_ULISSE/latex_DEMO/introduction}
\input{Chap1_ULISSE/latex_DEMO/Demo}


\chapter[Motif and Discord Discovery]{Variable Length Motif and Discord Discovery}
\label{chapterMAD}
\input{Chap2_MAD/latex/intro}
\input{Chap2_MAD/latex/problem}
\input{Chap2_MAD/latex/proposedApproach}
\input{Chap2_MAD/latex/proposedApproachDiscord}
\input{Chap2_MAD/latex/empiricalEvaluation}
\input{Chap2_MAD/latex/empiricalEvaluationDiscord}

\chapter[Motif Discovery Suite]{A Suite for Easy and Exact Detection of Variable Length Motifs in Data Series}
\label{chapterVALMODdemo}
\input{Chap2_MAD/latex/demo}

\chapter{Conclusions and Future Work}
\label{conclusion}
\input{conclusion}

\printindex % Imprime l'index
%\printbibliography % Imprime la bibliographie

\bibliographystyle{plain}
\bibliography{bibliographie}

%Annexes
%\appendix
%
%\chapter{Annexe 1}
%\lipsum[1-3]
%\chapter{Annexe 2}
%\lipsum[1-3]


%%Abstract
%\newpage
%\thispagestyle{empty}
%\noindent
%\textbf{Résumé (français) :}
%\vskip 1cm
%\noindent
%\textbf{Title :}
%\vskip 1cm
%\noindent
%\textbf{Abstract :}
%\vskip 1cm
%\noindent
%\textbf{Mots-clés (français) :}
%\vskip 1cm
%\noindent
%\textbf{Keywords :}
%\vskip 2 cm
%
%[Le cas échéant : dernière de couverture, dans le cas d’une impression sur support papier]


\end{document}