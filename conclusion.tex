

Similarity search is one of the fundamental operations for several data series analysis tasks. Even though much effort has been dedicated to the development of  techniques that can speed up similarity search, all existing solutions are limited by the fact that they can only support fixed length results.

In this thesis we describe \emph{exact} solutions for three problems that are based on similarity search, namely subsequence matching, motif and discord discovery. 
We extend the state-of-the-art by proposing algorithms that can operate with \emph{variable-length} sequences, thus, removing this limitation that existed in all studies in the literature.


\section{Subsequence Matching}

To perform efficiently subsequence matching in large data collections, we propose ULISSE, the first index able to answer similarity search queries of variable-length, over both Z-normalized and non Z-normalized sequences, supporting the use of Euclidean and Dynamic Time Warping distances.
The main ULISSE building block is new data series summarization (Envelope), which succinctly represents several contiguous overlapping sequences.
We proposed a new search algorithm, which can answer $K-NN$ queries, and can be easily adapted to the $\epsilon$-range search.  
We experimentally evaluated, our indexing and similarity search algorithms, on synthetic and real datasets, demonstrating that a compact and single structure enables an efficient (in space and time cost) and scalable solution for subsequence matching.  




\subsection{Open Research Problems}

This work has paved the road towards several extensions.
First, we aim to improve the performance of the ULISSE indexing strategy for datasets that contain very long data series (where optimized serial scan techniques have an advantage).  
To that extent, we envision to further improve the space compression capability of ULISSE, finding a deterministic trade-off between the Envelopes size and space pruning capability.

Moreover, we want to explore the possibility to propose new similarity measures, which consider similar candidates according the subsequence features (possibly of variable length). A recent work~\cite{DBLP:conf/icdm/GharghabiIBDK18} has shown that considering similar data series according to their subsequences can effectively improve clustering results, which are performed with Euclidean and DTW distances.
We believe that ULISSE can inherently support this kind of measure, providing a scalable similarity search solution.

Finally, we also plan to study solutions built on top of ULISSE that can exploit multi-core and multi-socket architectures, which can significantly improve performance~\cite{PengFP18}. 




\section{Motif and Discord Discovery}

In the second part of the work, we proposed the first framework (MAD) for variable-length motif and discord discovery. We described a new distance normalization method, as well
as a novel distance lower bounding technique, both of which are necessary for the solution to our problems. We experimentally evaluated our algorithms
by using five real datasets from diverse domains. The results demonstrate the efficiency and scalability of our approach (up to 20x faster than the state of
the art), as well as its usefulness.






\subsection{Open Research Problems}

In terms of future work, we would like to further improve the scalability of the MAD framework, as well as to extend the support to other data mining primitives.
Our goal would be to support efficiently the computation of the complete matrix profile for each subsequence length in the input range.
This would enable us to support more diverse applications, such as discovery of \textit{shapelets}~\cite{YeK09} in data series classification.

% The following is the latent motif idea:
Moreover, we observe that motif discovery tools typically work by discovering motif pairs, which are then expanded to motif sets, just considering a fixed neighborhood space. 
Similarly, this is also true in the case of discord (anomalous patterns) discovery.
%We aim to investigate if a more involved strategy can lead to better results in terms of insights quality.
We note that contrary to the case of motif pairs, there exists no measure for evaluating the quality of a pattern (motif or discord) set. 
In this direction, it seems promising to study new primitives that may describe and rank a set of interesting patterns. 
Following this idea, we would like to extend our framework by proposing a scalable solution for mining and ranking sets of patterns, irrespective of their cardinality.

In general, we showed that relaxing the fixed length (search) constraint in motif and discord discovery is a useful feature. 
In this sense, proposing an effective solution required us to tackle several non-trivial challenges.
We are convinced that despite the hard nature of this problem, our results will allow us to push the limits (and our ambitions) even further. 
Therefore, our research directions point towards parameter-free solutions for data series analysis. 






