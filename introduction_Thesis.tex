

Data series (i.e., ordered sequences of points) are one of the most common data types, present in almost every scientific and social domain (such as meteorology, astronomy, chemistry, medicine, neuroscience, finance, agriculture, entomology, sociology, smart cities, marketing, operation health monitoring, human action recognition and others)~\cite{KashinoSM99,Raza1025Percom,Shasha99,HuijseEPPZ14,Palpanas15}.
If the dimension that imposes the ordering of the sequences is time then we talk about time series. Though, a series can also be defined over other measures (e.g., angle in radial profiles in astronomy, mass in mass spectroscopy in physics, position in genome sequences in biology, etc.). 
In the rest of this these, we use the terms \emph{data series}, \emph{time series}, and \emph{sequence} interchangeably.

Once the data series have been collected, the domain experts face the arduous tasks of processing and analyzing them~\cite{ZoumpatianosICDE2018} in order to gain insights, e.g., by identifying similar patterns, and performing classification, or clustering.
A core operation that is part of all these analysis tasks is \textbf{similarity search}, which has attracted lots of attention because of its importance~\cite{PalpanasCGKZ04,Assent2008,shieh2008sax,CamerraPSK10,WangWPWH13,ZoumpatianosIP15,DBLP:journals/vldb/ZoumpatianosIP16,YagoubiAMP17,PengFP18,KondylakisVLDB18,DBLP:journals/tvcg/GogolouTPB19,DBLP:conf/edbt/GogolouTPB19,T-Store,DBLP:conf/ssdbm/MirylenkaDP17}.
Nevertheless, all existing  and efficient (mostly index-based) similarity search techniques are restricted in that they only support queries of a fixed length, and they require that this length is chosen at index construction.
The same observation holds for techniques proposed to discover motifs~\cite{LiUYG15,ZhuZSYFMBK16} and discords (i.e., anomalous subsequences)~\cite{ZhuZSYFMBK16,YankovKR08}: they all assume a fixed sequence length, which has to be predefined.

Evidently, this is a constraint that penalizes the flexibility needed by analysts, who often times need to analyze patterns of slightly different lengths (within a given data series collection)~\cite{KadiyalaS08,Kahveci2001,RakthanmanonCMBWZZK12,LinardiValmod18,LinardiValmodDemo18}. 
To that extent, we can report several examples (from real user studies), which benefit of multi-length search:

\begin{itemize}
	
	\item In the \emph{SENTINEL-2} mission data, oceanographers are interested in searching for similar coral bleaching patterns\footnote{\scriptsize\url{http://www.esa.int/Our_Activities/Observing_the_Earth}} of different lengths;
	
	\item At Airbus\footnote{\scriptsize\url{http://www.airbus.com/}} engineers need to perform similarity search queries for patterns of variable length when studying aircraft takeoffs and landings~\cite{Airbus};
	
	\item In neuroscience, analysts need to search in Electroencephalogram (EEG) recordings for Cyclic Alternating Patterns (CAP) of different lengths (duration), in order to get insights about brain activity during sleep~\cite{ROSA1999585}.
	
	\item Entomologists want to study insects that feed by ingesting plant fluids, and cause devastating damage to agriculture worldwide~\cite{citrusProduction}. This feeding processes can be recorded and analyzed in order to find repeated patterns that permit to understands and ultimately controlling the pests.
	It turns out that several interesting behaviors in these data occur along different time windows. Here, extracting variable length patterns becomes an essential operation. 
	  
	
\end{itemize}
 

In this thesis, we focus on three core problems that are based on similarity search, i.e., sequence matching, motif and discord discovery, and for which we remove the constraint of having to operate with a pre-determined sequence length.
Our work is the first that proposes efficient and effective solutions for the above three problems when considering sequences of variable-length.
 
In the following, we provide an overview of these three problems, and the corresponding challenges.   








\section{Sequence Similarity Search (or Sequence Matching)}

A sequence similarity search query is the operation that takes as input a data series $Q$ (query) and a parameter $k \in \mathbb{N}$ finding the $k$ most similar\footnote{We provide a formal description of similarity measures in Chapter~\ref{chapterULISSE}} series to $Q$ in a data series collection $C$.
This operation in very large data series collections is notoriously challenging~\cite{DBLP:journals/pvldb/WangWPWH13,DBLP:conf/sigmod/ZoumpatianosIP14,DBLP:conf/sofsem/Palpanas16,DBLP:conf/ieeehpcs/Palpanas17,EchihabiZPB18}, due to the high dimensionality (length) of the data series.

The vast majority of sequence matching solutions (including the state-of-the-art) relies on data summarization and indexing, which permit to perform fast and scalable similarity search~\cite{Faloutsos1994,Rafiei1998,DBLP:conf/vldb/PalpanasCGKZ04,Assent2008,shieh2008sax,DBLP:journals/kais/KadiyalaS08,DBLP:journals/pvldb/WangWPWH13,DBLP:journals/kais/CamerraSPRK14,DBLP:journals/pvldb/DallachiesaPI14,ZoumpatianosIP15,DBLP:journals/vldb/ZoumpatianosIP16,DBLP:conf/icdm/YagoubiAMP17,coconut}.
	
Despite the effectiveness and benefits of the proposed indexing techniques, which have enabled and powered many applications over the years, they are restricted in different ways: either they only support queries of a fixed size, or they do not offer a scalable solution. 
The solutions working for a fixed length, require that this length is chosen at index construction time (it should be the same as the length of the series in the index).
Given these premises, it is clear that a straightforward solution for answering sequence matching queries would be to use one of the available indexing techniques. 
However, in order to support (exact) results for variable-length sequence matching, we would need to:

\begin{itemize}
	\item create several distinct indexes, one for each possible query length;
	\item for each one of these indexes, index all overlapping subsequences (using a sliding window).
\end{itemize}

We illustrate this fact in Figure~\ref{simSearchVL}, where we depict two queries of different lengths ($\ell_1$ and $\ell_2$).

\begin{figure}[tb]
	\centering
	\includegraphics[trim={2cm 13cm 15cm 3cm},scale=1]{Chap1_ULISSE/figures/SS_query_VL.pdf}
	\caption{Indexing for supporting queries of 2 different lengths.}
	\label{simSearchVL}
	%\vspace*{-0.5cm}
\end{figure}
 
Given a data series (from a collection $C$), we denote as $D$ (shown in black), we draw in red the subsequences that we need to compare to each query in order to compute the exact answer. 
Using an indexing technique implies inserting all the subsequences in the index: since we want to answer queries of two different lengths, we are obliged to use two distinct indexes.

\begin{figure}[tb]
	\centering
	\includegraphics[trim={0cm 15cm 18cm 3cm},scale=1]{Chap1_ULISSE/figures/explosionSS.pdf}
	\caption{Search space evolution of variable length sequence matching. Each dataset contains series of length $256$}
	\label{spaceExplosionVL}
	%\vspace*{-0.7cm}
\end{figure}

Nevertheless, this solution is prohibitively expensive, in both space and time. 
Space complexity is increased, since we need to index a large number of subsequences for each one of the supported query lengths: given a data series collection $C={D^{1},...,D^{|C|}}$
%($D^{i}$ refers to the $i^{th}$ Data series) 
and a query length range $[\ell_{min},\ell_{max}]$, the number of subsequences we would normally have to examine (and index) is: 


\begin{equation}
S_{\ell_{min},\ell_{max}} = \sum_{\ell=1}^{(\ell_{max}-\ell_{min})+1}\sum_{i=1}^{|C|}(|D^{i}|-(\ell-1)).
\end{equation}
Figure~\ref{spaceExplosionVL} shows how quickly this number explodes as the dataset size and the query length range increase:
%In each dataset are contained series of length \textit{256}.
considering the largest query length range ($S_{96-256}$) in the $20$GB dataset, we end up with a collection of subsequences (that need to be indexed) more than $2$ orders of magnitude larger than the original dataset.
Computational time is significantly increased as well, since we have to construct different indexes for each query length we wish to support.

In the current literature, a technique based on multi-resolution indexes~\cite{914838,DBLP:journals/kais/KadiyalaS08} has been proposed in order to mitigate this explosion in size, by creating a smaller number of distinct indexes and performing more post-processing.
Nonetheless, this solution works exclusively for \emph{non} Z-normalized series\footnote{Z-normalization transforms a series so that it has a mean value of zero, and a standard deviation of one. This allows the search to be effective, irrespective of shifting (i.e., offset translation) and scaling~\cite{DBLP:journals/datamine/KeoghK03}.} (which means that it cannot return results with similar trends, but different absolute values), and thus, renders the solution useless for a wide spectrum of applications.
Besides, it only mitigates the problem, since it still leads to a space explosion (albeit, at a lower rate), and therefore, it is not scalable, either.

We note that the technique discussed above (despite its limitations) is indeed the current state of the art, and no other technique has been proposed since, even though during the same period of time we have witnessed lots of activity and a steady stream of proposals on the \emph{single-length} similarity search problem (e.g.,~\cite{DBLP:conf/vldb/PalpanasCGKZ04,Assent2008,shieh2008sax,DBLP:conf/icdm/CamerraPSK10,DBLP:journals/pvldb/WangWPWH13,DBLP:journals/vldb/ZoumpatianosIP16,DBLP:journals/vldb/ZoumpatianosIP16,DBLP:conf/icdm/YagoubiAMP17,coconut}). 
This attests to the challenging nature of the first problem we are tackling in this thesis.


To tame the search space explosion, and to propose a new effective solution we follow a key idea: a data structure that indexes data series of length $\ell$, already contains all the information necessary for reasoning about any subsequence of length $\ell'<\ell$ of these series.
Therefore, the problem of enabling a data series index to answer queries of variable-length, becomes a problem of how to reorganize this information that already exists in the index.
To this effect, we want to propose a new summarization technique that is able to represent contiguous and overlapping subsequences, leading to succinct, yet powerful summaries. It has to combine the representation of several subsequences within a single summary, and enable fast (approximate and exact) answers for variable-length sequence matching queries.


\section{Data Series Motif}

Over the last decade, data series motif discovery has emerged as one of the most used primitive for data series mining.
Informally, we can describe motifs, as the most significant patterns that occur in a data series, i.e., subsequences that repeat themselves approximatively in the same manner. 
We report in Figure~\ref{exMotifs} a snippet of an Electrocardiogram recording (ECG), which records the electrical activity of the heart. This data series contains a series of repeated patterns highlighted in red, which represents heartbeat waveforms generated by ventricular contractions. These subsequences naturally represent a group of motifs over these data.   

\begin{figure}[tb]
	\centering
	\includegraphics[trim={0cm 12cm 11cm 3cm},scale=0.7]{figuresGeneral/motifECG}
	\caption{Electrocardiogram recording (ECG), with highlighted heartbeat waveforms motifs (in red)}
	\label{exMotifs}
	%\vspace*{-0.7cm}
\end{figure}


Motif discovery has many applications to a wide variety of domains~\cite{Whitney,DBLP:conf/kdd/YankovKMCZ07}, including classification, clustering, and rule discovery. More recently, there has been substantial progress on the scalability of motif discovery, and now massive datasets can be routinely searched on conventional hardware~\cite{Whitney}. 

Another critical improvement in motif discovery, is the reduction in the number of parameters requiring specification. The first motif discovery algorithm, PROJECTION~\cite{DBLP:conf/kdd/ChiuKL03}, required the user to set seven parameters, and it still only produces answers that are approximately correct. 
Researchers and practitioners have \textit{"chipped"} away at this over the years~\cite{DBLP:conf/sdm/MueenKZCW09,DBLP:conf/ijcai/SariaDK11}, and the current state-of-the-art algorithms only require the user to set a single parameter, which is the desired length of the motifs. 
Surprisingly, the ease with which we can now perform motif discovery has revealed that even this single burden on the user's experience or intuition can be \textit{arduous} along the analysis task pipeline. 
The issue of being restricted to specify length as an input parameter, has been noted in  domains that use motif discovery, such as cardiology~\cite{DBLP:journals/tkdd/SyedSKIG10} and speech therapy~\cite{Wordrecognition}. 

On the other hand, we can still consider the case, in which the user has good knowledge of the data domain.
Also here, searching with one single motif length can be penalizing, especially when the data can contain motifs of various lengths. 

\begin{figure}[tb]
	\centering
	\includegraphics[trim={3cm 2cm 0cm 0cm},scale=0.66]{figuresGeneral/varLengthMotif_Insect}
	\caption{Demonstration of semantically different motifs, of slightly different lengths, extracted from a single dataset.}
	%This figure is a preview of Figure~\ref{fig:insectVarLeMotif}.}
	\label{fig:insectVarLeMotifintro}
	%\vspace*{-0.5cm}
\end{figure}

To that extent, we show an example in Figure~\ref{fig:insectVarLeMotifintro}, where we report the 10-second and 12-second motifs discovered in the Electrical Penetration Graph (EPG) of an insect called Asian citrus psyllid. The first motif denotes the insect's highly technical probing skill as it searches for a rich leaf vein (stylet passage), whereas the second motif is just a simple repetitive ``sucking'' behavior (xylem ingestion). This example shows the utility of variable length motif discovery. An entomologist using classic motif search, for instance at the length of 12 seconds, might have plausibly believed that this insect only engaged in xylem ingestion during this time period, and not realized the insect had found it necessary to reposition itself at least twice.
The two motif pairs are radically different, reflecting two different types of insect activities. In order to capture all useful activity information within the data, a fast search of motifs over all lengths is necessary.

The obvious variable-length motif search is to make the state-of-the-art algorithm search over all lengths in a given range and rank the various length motifs discovered.
We noticed that this strategy poses two challenges:

\begin{itemize}
	\item  As in the case of the sequence matching task, the problem of searching over a much larger solution space in an efficient way is crucial for motif discovery as well.
	\item  Once we enumerate motifs of several different lengths, we need to dispose of a strategy that permits to rank them.
\end{itemize}


\section{Data Series Discord}

Symmetrically to data series motif, another popular and well studied data series primitive, the discord~\cite{DBLP:journals/kais/YankovKR08,Keogh2005,YehZUBDDSMK16,DBLP:conf/edbt/Senin0WOGBCF15,Luo2013ParameterFreeSO}, has been proposed to discover subsequences that represent outliers.
Hence, with discords, we want to represent rare and abnormal patterns that occur in a Data Series datasets.  

\begin{figure}[tb]
	\centering
	\includegraphics[trim={0cm 12cm 11cm 3cm},scale=0.7]{figuresGeneral/discordMarotta}
	\caption{NASA Shuttle Valve data series. The discord, which represents a failure is highlighted in red.}
	\label{MarottaValve}
	%\vspace*{-0.7cm}
\end{figure}

We depict an example in Figure~\ref{MarottaValve}, where a NASA Shuttle Valve data series is reported.
%The valves are used to   fuel on the Space Shuttle.
Specifically, these data are recorded, while conducting cyclic conditions test in laboratory~\footnote{https://cs.fit.edu/$\sim$pkc/nasa/data/}.
In the picture, the series contains measurements of the solenoid, which exhibit a cyclic phase. 
We know (from experts annotations) that the last cycle reports a failure (highlighted in red).
As we note, the shape of this pattern clearly deviates from the previous cyclic patterns. In this case this subsequence is identified as a discord.
   

In the literature, the discord discovery solutions that have been proposed are not as effective and scalable as practice requires. 
The reasons are twofold:
\begin{itemize}
	\item First, they only support fixed-length discord discovery. This rigidity with the subsequence length restricts the search space, and consequently, also the produced solutions and the effectiveness of the algorithm. 
	\item Second, the existing techniques provide poor support for enumerating multiple discords, namely, for the identification of multiple anomalous subsequences. These works have considered only cases with up to \textit{3} anomalous subsequences.
\end{itemize}







\section{Contributions}

Here, we provide the outline of our main contributions. 
%We also report an overview of the thesis organization.

\noindent {\bf [Variable-Length Similarity Search]}
We first study the \text{variable-length} sequence matching query.
We focus on efficiency improvement of similarity search, which is the operation at the core of the solution to this problem.  
In that regard, we propose \textit{$ULISSE$} (ULtra compact Index for variable-length Similarity SEarch in data series), which is the first single-index solution that supports fast answering of variable-length similarity search queries for both non Z-normalized and Z-normalized data series collections. 
\textit{ULISSE} produces exact (i.e., correct) results, and is based on the following key idea: a data structure that indexes data series of length $\ell$, already contains all the information necessary for reasoning about any subsequence of length $\ell'<\ell$ of these series.
Therefore, the problem of enabling a data series index to answer queries of variable-length, becomes a problem of how to reorganize this information that already exists in the index.
To this effect, \textit{ULISSE} proposes a new summarization technique that is able to represent contiguous and overlapping subsequences, leading to succinct, yet powerful summaries: it combines the representation of several subsequences within a single summary, and enables fast (approximate and exact) similarity search for variable-length queries.

The contributions of this part of the thesis can be summarized as follows: 

\begin{itemize}
	\item We introduce the problem of Variable-Length Subsequences Indexing, which calls for a single index that can inherently answer queries of different lengths.
	\item We provide a new data series summarization technique, able to represent several contiguous series of different lengths.
	\item The technique we propose produces succinct, discretized envelopes for the summarized series, and can be applied to both non Z-normalized and Z-normalized data series.
	\item Based on this summarization technique, we develop an indexing algorithm, which organizes the series and their discretized summaries in a hierarchical tree structure, namely, the \textit{ULISSE} index.
	\item We propose efficient exact and approximate K-NN algorithms, suitable for the \textit{ULISSE} index, which can compute the similarity using either Euclidean Distance or Dynamic Time Warping measure.
	\item We perform an experimental evaluation with several synthetic and real datasets. The results demonstrate the effectiveness and scalability of ULISSE to dataset sizes that competing approaches cannot handle.
	\item Finally, we describe a prototype system we developed to support similarity search queries of \emph{variable length}. 
	It employs the \textit{ULISSE} index in order to allow users to interactively run and explore the results of approximate and exact subsequence similarity search in both non Z-normalized and Z-normalized large data series collections.
\end{itemize}


\noindent {\bf [Variable-Length Motif and Discord Discovery]}
Furthermore, we consider the motif and discord discovery problems in conjunction.
Our work wants to improve the efficiency of motif and discord search, we thus propose a solution that significantly extend the state-of-the-art algorithms.

In fact, the actual solution for fixed length motif and discord discovery~\cite{ZhuZSYFMBK16} requires the user to define the length of the desired motif or discord. This mining operation is supported by computation of the \textit{Matrix profile}, which is a meta data series storing the z-normalized Euclidean distance between each subsequence and its nearest neighbor. 
The Matrix profile does not only derive motifs and discords, but also ranks the other subsequences, giving a convenient and graphical representation of their occurrences and proximity.
Unfortunately, this technique comes with an important shortcoming: it does not provide an effective solution for trying several different motif/discord lengths.
Therefore, the analyst is forced to run the algorithm using all possible lengths in a range of interest, and rank the various motifs discovered, picking eventually the patterns that contain the desired insight. 

To that extent, we firstly define the problems of variable-length motif and discord discovery, which significantly extend the usability of the motif and discord discovery operations, respectively.
This premises allow us to build and propose a new data series motif and discord framework.
The contributions of this part of the thesis can be summarized as follows:  

\begin{itemize}
 
	\item A Variable Length Motif Discovery algorithm (VALMOD), which takes as input a data series $T$, and finds the subsequence pairs with the smallest Euclidean distance of each length in the (user-defined) range [$\ell_{min}$, $\ell_{max}$]. VALMOD is based on a novel lower bounding technique, which is specifically designed for the motif discovery problem.
	
	\item Furthermore, we extend VALMOD to the discord discovery problem. We propose a new exact variable-length discord discovery, which aims at finding  the subsequence pairs with the largest Euclidean distances of each length in the (user-defined) range [$\ell_{min}$, $\ell_{max}$]. 
	
	\item We evaluate our techniques using five diverse real datasets, and demonstrate the scalability of our approach. The results show that our solution is up to 20x faster than the state-of-the-art techniques.
	
	\item Furthermore, we present real case studies with datasets from entomology, seismology, and traffic data analysis, which demonstrate the usefulness of our approach in real world user studies.
	
	\item Finally, we present a motif discovery prototype system, which implements the scalable motif discovery algorithm (VALMOD), and uses a newly proposed meta-data structure that helps the user to select the most promising pattern length. 
	We demonstrate how the proposed system efficiently finds all motifs in a given range of lengths, and outputs a length-invariant ranking of motifs.
	
\end{itemize}

\section{Thesis Outline and Publications}

In this thesis, it is important to note that we present the contributions that we can find in a collection of articles (both accepted for publication and under peer review). These papers are first-authored by the thesis writer. 
The manuscript is thus organized in chapters as follows:
\begin{itemize}

\item[] In \textbf{Chapter~\ref{RW}}, we propose the revision of the state-of-the-art methods for the problems we treat in this thesis.

\item[] In \textbf{Chapter~\ref{chapterULISSE}}, we introduce all the details and reports our scalable solution for \text{variable-length} sequence matching query. This work is published in:

	\begin{itemize}
	\item Scalable, Variable-Length Similarity Search in Data Series: The ULISSE Approach. (Michele Linardi, Themis Palpanas) \textbf{PVLDB 11(13), 2018}
%\textit{https://dl.acm.org/citation.cfm?doid=3275366.3284968}
	\item ULISSE: ULtra Compact Index for Variable-Length Similarity Search in Data Series (Michele Linardi, Themis Palpanas) \textbf{ICDE, 2018}
	%\textit{https://ieeexplore.ieee.org/document/8509370}
	\end{itemize}
	Moreover, one further article is under review:
	\begin{itemize}
		\item Scalable Data Series Subsequence Matching with ULISSE (Michele Linardi, Themis Palpanas) 2019
	\end{itemize}

\item[] In \textbf{Chapter~\ref{chapterULISSEDemo}}, we propose the demonstration of VALS (Scalable VAriable-Length Similarity Search Suite), which is a system that permits to utilize and compare the state-of-the arts solutions for subsequence matching in data series. This work is under review:

   \begin{itemize}
   		\item VALS with ULISSE: Variable-Length Similarity Search in Large Data Series Collections (Michele Linardi, Themis Palpanas), 2019
   \end{itemize}

\item[] In \textbf{Chapter~\ref{chapterMAD}}, we propose our solution to data series motif and discord discovery. This work is published in 
	 \begin{itemize}
		\item Matrix Profile X: VALMOD - Scalable Discovery of Variable-Length Motifs in Data Series (Michele Linardi, Yan Zhu, Themis Palpanas, Eamonn Keogh) \textbf{SIGMOD Conference, 2018}
	\end{itemize}
	Morever, the following article is under review:
	\begin{itemize}
		\item Matrix Profile Goes MAD: Variable-Length Motif And Discord Discovery in Data Series (Michele Linardi, Yan Zhu, Themis Palpanas, Eamonn Keogh), 2019
	\end{itemize}

 \item[] In \textbf{Chapter~\ref{chapterVALMODdemo}}, we describe the demonstration of the motif discovery system we implemented. This work is published in:
 \begin{itemize}
 	\item VALMOD: A Suite for Easy and Exact Detection of Variable Length Motifs in Data Series (Michele Linardi, Yan Zhu, Themis Palpanas, Eamonn Keogh) \textbf{SIGMOD Conference, 2018}
 \end{itemize}
 
 \item[] In \textbf{Chapter~\ref{conclusion}}, we conclude and we propose promising research directions for future work.
 
 
\end{itemize}






  