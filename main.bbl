\begin{thebibliography}{100}

\bibitem{}
{Machine Learning in Time Series Databases ( and Everything Is a Time Series !)
  Outline of Tutorial II}.
\newblock {\em Update}, pages 1--31.

\bibitem{DBLP:conf/fodo/AgrawalFS93}
Rakesh Agrawal, Christos Faloutsos, and Arun~N. Swami.
\newblock Efficient similarity search in sequence databases.
\newblock In {\em Foundations of Data Organization and Algorithms, 4th
  International Conference, FODO'93, Chicago, Illinois, USA, October 13-15,
  1993, Proceedings}, 1993.

\bibitem{Assent2008}
Ira Assent, Ralph Krieger, Farzad Afschari, and Thomas Seidl.
\newblock The ts-tree: Efficient time series search and retrieval.
\newblock In {\em EDBT}, 2008.

\bibitem{DBLP:journals/datamine/BagnallLBLK17}
Anthony Bagnall, Jason Lines, Aaron Bostrom, James Large, and Eamonn~J. Keogh.
\newblock The great time series classification bake off: a review and
  experimental evaluation of recent algorithmic advances.
\newblock {\em Data Min. Knowl. Discov.}, 2017.

\bibitem{Beckmann1990}
Norbert Beckmann, Hans-Peter Kriegel, Ralf Schneider, and Bernhard Seeger.
\newblock The r*-tree: An efficient and robust access method for points and
  rectangles.
\newblock SIGMOD, 1990.

\bibitem{DBLP:conf/sdm/BuLFKPM07}
Yingyi Bu, Tat wing Leung, Ada~Wai chee Fu, Eamonn Keogh, Jian Pei, and Sam
  Meshkin.
\newblock Wat: Finding top-k discords in time series database.
\newblock In {\em SDM}, 2007.

\bibitem{DBLP:conf/icdm/CamerraPSK10}
Alessandro Camerra, Themis Palpanas, Jin Shieh, and Eamonn~J. Keogh.
\newblock isax 2.0: Indexing and mining one billion time series.
\newblock In {\em {ICDM} 2010}.

\bibitem{CamerraPSK10}
Alessandro Camerra, Themis Palpanas, Jin Shieh, and Eamonn~J. Keogh.
\newblock isax 2.0: Indexing and mining one billion time series.
\newblock In {\em {ICDM} 2010}, 2010.

\bibitem{Bertolaso}
Edwin Cartlidge.
\newblock Seven-year legal saga ends as italian official is cleared of
  manslaughter in earthquake trial.
\newblock {\em Science}, Oct. 3, 2016.

\bibitem{Chakrabarti2002}
Kaushik Chakrabarti, Eamonn Keogh, and Sharad Mehrotra.
\newblock {Locally adaptive dimensionality reduction for indexing large time
  series databases}.
\newblock 2002.

\bibitem{Chen2007}
Qiuxia Chen, Lei Chen, Xiang Lian, Yunhao Liu, and Jeffrey~Xu Yu.
\newblock Indexable pla for similarity search.
\newblock VLDB, 2007.

\bibitem{DBLP:conf/kdd/ChiuKL03}
Bill~Yuan Chiu, Eamonn~J. Keogh, and Stefano Lonardi.
\newblock Probabilistic discovery of time series motifs.
\newblock In {\em SIGKDD 2003}, pages 493--498, 2003.

\bibitem{Ciaccia1997}
Paolo Ciaccia, Marco Patella, and Pavel Zezula.
\newblock M-tree: An efficient access method for similarity search in metric
  spaces.
\newblock In {\em VLDB}, 1997.

\bibitem{DBLP:journals/pvldb/DallachiesaPI14}
Michele Dallachiesa, Themis Palpanas, and Ihab~F. Ilyas.
\newblock Top-k nearest neighbor search in uncertain data series.
\newblock {\em {PVLDB}}, 8(1):13--24, 2014.

\bibitem{EchihabiZPB18}
Karima Echihabi, Kostas Zoumpatianos, Themis Palpanas, and Houda Benbrahim.
\newblock The lernaean hydra of data series similarity search: An experimental
  evaluation of the state of the art.
\newblock {\em {PVLDB}}, 12(2), 2018.

\bibitem{DBLP:journals/kais/CamerraSPRK14}
Alessandro~Camerra et~al.
\newblock Beyond one billion time series: indexing and mining very large time
  series collections with isax2+.
\newblock {\em KAIS}, 2014.

\bibitem{PhysioNet}
Goldberger et~al.
\newblock Physiobank, physiotoolkit, and physionet: Components of a new
  research resource for complex physiologic signals., 2000 June 13.

\bibitem{KV-match}
Jiaye~Wu et~al.
\newblock Kv-match: A subsequence matching approach supporting normalization
  and time warping.
\newblock {\em ICDE}, 2019.

\bibitem{ltv}
S.~Soldi et~al.
\newblock Long-term variability of agn at hard x-rays.
\newblock {\em Astronomy \& Astrophysics}, 2014.

\bibitem{RakthanmanonCMBWZZK12}
Thanawin~Rakthanmanon et~al.
\newblock Searching and mining trillions of time series subsequences under
  dynamic time warping.
\newblock In {\em {SIGKDD}}, 2012.

\bibitem{ZhuZSYFMBK16}
Yan~Zhu et~al.
\newblock Matrix profile {II:} exploiting a novel algorithm and gpus to break
  the one hundred million barrier for time series motifs and joins.
\newblock In {\em {ICDM} 2016}.

\bibitem{Faloutsos1994}
Christos Faloutsos, M.~Ranganathan, and Yannis Manolopoulos.
\newblock Fast subsequence matching in time-series databases.
\newblock In {\em SIGMOD}, 1994.

\bibitem{DBLP:conf/adma/FuLKL06}
Ada~Wai{-}Chee Fu, Oscar~Tat{-}Wing Leung, Eamonn~J. Keogh, and Jessica Lin.
\newblock Finding time series discords based on haar transform.
\newblock In {\em {ADMA}}, 2006.

\bibitem{DBLP:journals/datamine/GaoL18}
Yifeng Gao and Jessica Lin.
\newblock Exploring variable-length time series motifs in one hundred million
  length scale.
\newblock {\em Data Min. Knowl. Discov.}, 32(5):1200--1228, 2018.

\bibitem{Gao0R16}
Yifeng Gao, Jessica Lin, and Huzefa Rangwala.
\newblock Iterative grammar-based framework for discovering variable-length
  time series motifs.
\newblock In {\em {ICMLA}, 2016}.

\bibitem{DBLP:conf/icdm/GharghabiIBDK18}
Shaghayegh Gharghabi, Shima Imani, Anthony~J. Bagnall, Amirali Darvishzadeh,
  and Eamonn~J. Keogh.
\newblock Matrix profile {XII:} mpdist: {A} novel time series distance measure
  to allow data mining in more challenging scenarios.
\newblock In {\em {IEEE} International Conference on Data Mining, {ICDM} 2018,
  Singapore, November 17-20, 2018}, 2018.

\bibitem{6602387}
C.~Gisler, A.~Ridi, D.~Zufferey, O.~A. Khaled, and J.~Hennebert.
\newblock Appliance consumption signature database and recognition test
  protocols.
\newblock In {\em 2013 WoSSPA)}, pages 336--341, 2013.

\bibitem{DBLP:conf/edbt/GogolouTPB19}
Anna Gogolou, Theophanis Tsandilas, Themis Palpanas, and Anastasia Bezerianos.
\newblock Progressive similarity search on time series data.
\newblock In {\em Proceedings of the Workshops of the {EDBT/ICDT} 2019 Joint
  Conference, {EDBT/ICDT} 2019, Lisbon, Portugal, March 26, 2019.}

\bibitem{DBLP:journals/tvcg/GogolouTPB19}
Anna Gogolou, Theophanis Tsandilas, Themis Palpanas, and Anastasia Bezerianos.
\newblock Comparing similarity perception in time series visualizations.
\newblock {\em {IEEE} Trans. Vis. Comput. Graph.}, 25(1):523--533, 2019.

\bibitem{DBLP:journals/tkdd/GrabockaSS16}
Josif Grabocka, Nicolas Schilling, and Lars Schmidt{-}Thieme.
\newblock Latent time-series motifs.
\newblock {\em {TKDD}}, 11(1):6:1--6:20, 2016.

\bibitem{stressDriver}
Picard~RW. Healey~JA.
\newblock Detecting stress during real-world driving tasks using physiological
  sensors.
\newblock {\em IEEE Transactions in Intelligent Transportation Systems
  6(2):156-166}, June 2016.

\bibitem{HuijseEPPZ14}
Pablo Huijse, Pablo~A. Est{\'{e}}vez, Pavlos Protopapas, Jose~C. Principe, and
  Pablo Zegers.
\newblock Computational intelligence challenges and applications on large-scale
  astronomical time series databases.
\newblock 2014.

\bibitem{SEISMIC}
IRIS.
\newblock {Seismic Data Access} 2016.

\bibitem{Itakura}
F.~{Itakura}.
\newblock Minimum prediction residual principle applied to speech recognition.
\newblock {\em IEEE Transactions on Acoustics, Speech, and Signal Processing},
  1975.

\bibitem{KadiyalaS08}
Srividya Kadiyala and Nematollaah Shiri.
\newblock A compact multi-resolution index for variable length queries in time
  series databases.
\newblock {\em KAIS}, 2008.

\bibitem{DBLP:journals/kais/KadiyalaS08}
Srividya Kadiyala and Nematollaah Shiri.
\newblock A compact multi-resolution index for variable length queries in time
  series databases.
\newblock {\em KAIS}, 2008.

\bibitem{Kahveci2001}
T.~Kahveci and A.~Singh.
\newblock Variable length queries for time series data.
\newblock In {\em ICDEF}, 2001.

\bibitem{914838}
T.~Kahveci and A.~Singh.
\newblock Variable length queries for time series data.
\newblock In {\em Proceedings 17th International Conference on Data
  Engineering}, 2001.

\bibitem{KashinoSM99}
Kunio Kashino, Gavin Smith, and Hiroshi Murase.
\newblock Time-series active search for quick retrieval of audio and video.
\newblock In {\em ICASSP}, 1999.

\bibitem{Keogh2005}
E.~Keogh, J.~Lin, and a.~Fu.
\newblock {HOT SAX: Efficiently Finding the Most Unusual Time Series
  Subsequence}.
\newblock {\em Fifth IEEE International Conference on Data Mining (ICDM'05)},
  pages 226--233, 2005.

\bibitem{Keogh2000}
Eamonn Keogh, Kaushik Chakrabarti, Michael Pazzani, and Sharad Mehrotra.
\newblock Dimensionality reduction for fast similarity search in large time
  series databases.
\newblock {\em KAIS}, 3, 2000.

\bibitem{Keogh2007}
Eamonn Keogh, Stefano Lonardi, Chotirat~Ann Ratanamahatana, Li~Wei, Sang-Hee
  Lee, and John Handley.
\newblock Compression-based data mining of sequential data.
\newblock {\em Data Mining and Knowledge Discovery}, 2007.

\bibitem{DBLP:journals/datamine/KeoghK03}
Eamonn~J. Keogh and Shruti Kasetty.
\newblock On the need for time series data mining benchmarks: {A} survey and
  empirical demonstration.
\newblock {\em Data Min. Knowl. Discov.}, 2003.

\bibitem{PalpanasCGKZ04}
Eamonn~J. Keogh, Themis Palpanas, Victor~B. Zordan, Dimitrios Gunopulos, and
  Marc Cardle.
\newblock Indexing large human-motion databases.
\newblock In {\em VLDB}, 2004.

\bibitem{DBLP:conf/vldb/PalpanasCGKZ04}
Eamonn~J. Keogh, Themis Palpanas, Victor~B. Zordan, Dimitrios Gunopulos, and
  Marc Cardle.
\newblock Indexing large human-motion databases.
\newblock In {\em VLDB}, 2004.

\bibitem{DBLP:journals/kais/KeoghR05}
Eamonn~J. Keogh and Chotirat~(Ann) Ratanamahatana.
\newblock Exact indexing of dynamic time warping.
\newblock {\em Knowl. Inf. Syst.}, 2005.

\bibitem{DWT_indexing}
{Kin-Pong Chan} and {Ada Wai-Chee Fu}.
\newblock Efficient time series matching by wavelets.
\newblock In {\em Proceedings 15th International Conference on Data Engineering
  (Cat. No.99CB36337)}, pages 126--133, March 1999.

\bibitem{KondylakisVLDB18}
Haridimos Kondylakis, Niv Dayan, Kostas Zoumpatianos, and Themis Palpanas.
\newblock Coconut: A scalable bottom-up approach for building data series
  indexes.
\newblock In {\em PVLDB}, 2018.

\bibitem{coconut}
Haridimos Kondylakis, Niv Dayan, Kostas Zoumpatianos, and Themis Palpanas.
\newblock Coconut: A scalable bottom-up approach for building data series
  indexes.
\newblock {\em PVLDB (11)6:677-690}, 2018.

\bibitem{DTW}
JB~Kruskal and Mark Liberman.
\newblock The symmetric time-warping problem: From continuous to discrete.
\newblock {\em Time Warps, String Edits, and Macromolecules: The Theory and
  Practice of Sequence Comparison}, 01 1983.

\bibitem{DBLP:conf/icde/LiUYG15}
Yuhong Li, Leong~Hou U, Man~Lung Yiu, and Zhiguo Gong.
\newblock Quick-motif: An efficient and scalable framework for exact motif
  discovery, 2015.

\bibitem{LiUYG15}
Yuhong Li, Leong~Hou U, Man~Lung Yiu, and Zhiguo Gong.
\newblock Quick-motif: An efficient and scalable framework for exact motif
  discovery {ICDE}.
\newblock 2015.

\bibitem{Lichman:2013}
M.~Lichman.
\newblock {UCI} machine learning repository, 2013.

\bibitem{Lin2007}
Jessica Lin, Eamonn Keogh, Li~Wei, and Stefano Lonardi.
\newblock Experiencing sax: a novel symbolic representation of time series.
\newblock {\em Data Mining and Knowledge Discovery}, 2007.

\bibitem{paperWebpage}
Michele Linardi.
\newblock Valmod support web page, 2017.

\bibitem{LinardiValmod18}
Michele Linardi, Yan Zhu, Themis Palpanas, and Eamonn~J. Keogh.
\newblock {VALMOD:} {A} suite for easy and exact detection of variable length
  motifs in data series.
\newblock In {\em {SIGMOD} Conference 2018}.

\bibitem{LinardiValmodDemo18}
Michele Linardi, Yan Zhu, Themis Palpanas, and Eamonn~J. Keogh.
\newblock {VALMOD:} {A} suite for easy and exact detection of variable length
  motifs in data series.
\newblock In {\em {SIGMOD} Conference 2018}.

\bibitem{Liu2009}
Yubao Liu, Xiuwei Chen, and Fei Wang.
\newblock {Efficient Detection of Discords for Time Series Stream}.
\newblock {\em Advances in Data and Web Management}, pages 629--634, 2009.

\bibitem{Parameter-Free_Discord}
Wei Luo and Marcus Gallagher.
\newblock Faster and parameter-free discord search in quasi-periodic time
  series.
\newblock In Joshua~Zhexue Huang, Longbing Cao, and Jaideep Srivastava,
  editors, {\em Advances in Knowledge Discovery and Data Mining}, 2011.

\bibitem{Luo2013ParameterFreeSO}
Wei Luo, Marcus Gallagher, and Janet Wiles.
\newblock Parameter-free search of time-series discord.
\newblock {\em Journal of Computer Science and Technology}, 2013.

\bibitem{Marzal:1993:CNE:628305.628518}
A.~Marzal and E.~Vidal.
\newblock Computation of normalized edit distance and applications.
\newblock {\em IEEE Trans. Pattern Anal. Mach. Intell.}, 15(9), September 1993.

\bibitem{MinnenIES07}
David Minnen, Charles Lee~Isbell Jr., Irfan~A. Essa, and Thad Starner.
\newblock Discovering multivariate motifs using subsequence density estimation
  and greedy mixture learning.
\newblock In {\em {AAAI} Conference on Artificial Intelligence, 2007}.

\bibitem{DBLP:conf/ssdbm/MirylenkaDP17}
Katsiaryna Mirylenka, Michele Dallachiesa, and Themis Palpanas.
\newblock Data series similarity using correlation-aware measures.
\newblock In {\em Proceedings of the 29th International Conference on
  Scientific and Statistical Database Management, Chicago, IL, USA, June 27-29,
  2017}, 2017.

\bibitem{6426960}
Y.~Mohammad and T.~Nishida.
\newblock Unsupervised discovery of basic human actions from activity recording
  datasets.
\newblock In {\em 2012 IEEE/SICE International Symposium on System Integration
  (SII)}, 2012.

\bibitem{DBLP:conf/aciids/MohammadN14}
Yasser F.~O. Mohammad and Toyoaki Nishida.
\newblock Exact discovery of length-range motifs.
\newblock In {\em Intelligent Information and Database Systems - 6th Asian
  Conference, {ACIIDS} 2014}.

\bibitem{PFA}
Y~Morinaka, Masatoshi Yoshikawa, Toshiyuki Amagasa, and Shunsuke Uemura.
\newblock The l - index: An indexing structure for efficient subsequence
  matching in time sequence databases.
\newblock 01 2001.

\bibitem{DBLP:journals/kais/MueenC15}
Abdullah Mueen and Nikan Chavoshi.
\newblock Enumeration of time series motifs of all lengths.
\newblock {\em Knowl. Inf. Syst.}, 2015.

\bibitem{DBLP:conf/icdm/MueenHE14}
Abdullah Mueen, Hossein Hamooni, and Trilce Estrada.
\newblock Time series join on subsequence correlation.
\newblock In {\em {ICDM} 2014}, 2014.

\bibitem{DBLP:conf/sdm/MueenKZCW09}
Abdullah Mueen, Eamonn~J. Keogh, Qiang Zhu, Sydney Cash, and M.~Brandon
  Westover.
\newblock Exact discovery of time series motifs.
\newblock In {\em {SDM} 2009}.

\bibitem{citrusProduction}
Moss C.~B. Neupane, D. and A.~H.~2016. van Bruggen.
\newblock Estimating citrus production loss due to citrus huanglongbing in
  florida.
\newblock {\em Annual Meeting, Southern Agricultural Economics Association, San
  Antonio, TX.}, 2016.

\bibitem{Airbus}
Alleon Guillaume.~Head of~Operational Intelligence Department~Airbus.
\newblock {Personal communication.}, 2017.

\bibitem{Palpanas15}
Themis Palpanas.
\newblock Data series management: The road to big sequence analytics.
\newblock {\em SIGMOD Rec.}, 2015.

\bibitem{DBLP:conf/sofsem/Palpanas16}
Themis Palpanas.
\newblock Big sequence management: {A} glimpse of the past, the present, and
  the future.
\newblock In {\em {SOFSEM}}, 2016.

\bibitem{DBLP:conf/ieeehpcs/Palpanas17}
Themis Palpanas.
\newblock The parallel and distributed future of data series mining.
\newblock In {\em High Performance Computing {\&} Simulation ({HPCS})}, 2017.

\bibitem{PengFP18}
Botao Peng, Panagiota Fatourou, and Themis Palpanas.
\newblock Paris: The next destination for fast data series indexing and query
  answering.
\newblock In {\em {IEEE} Big Data}, 2018.

\bibitem{DBLP:conf/icde/PopivanovM02}
Ivan Popivanov and Ren{\'{e}}e~J. Miller.
\newblock Similarity search over time-series data using wavelets.
\newblock In {\em Proceedings of the 18th International Conference on Data
  Engineering, San Jose, CA, USA, February 26 - March 1, 2002}, 2002.

\bibitem{Rafiei1998}
Davood Rafiei and Alberto Mendelzon.
\newblock Efficient retrieval of similar time sequences using dft.
\newblock In {\em ICDE}, 1998.

\bibitem{DBLP:conf/kdd/RakthanmanonCMBWZZK12}
Thanawin Rakthanmanon, Bilson J.~L. Campana, Abdullah Mueen, Gustavo E. A.
  P.~A. Batista, M.~Brandon Westover, Qiang Zhu, Jesin Zakaria, and Eamonn~J.
  Keogh.
\newblock Searching and mining trillions of time series subsequences under
  dynamic time warping.
\newblock In {\em {SIGKDD}}, 2012.

\bibitem{Raza1025Percom}
Usman Raza, Alessandro Camerra, Amy~L. Murphy, Themis Palpanas, and Gian~Pietro
  Picco.
\newblock Practical data prediction for real-world wireless sensor networks.
\newblock {\em {IEEE} Trans. Knowl. Data Eng.}, 2015.

\bibitem{DBLP:journals/pvldb/RongB17}
Kexin Rong and Peter Bailis.
\newblock {ASAP:} prioritizing attention via time series smoothing.
\newblock {\em {PVLDB}}, 10(11):1358--1369, 2017.

\bibitem{ROSA1999585}
A.C. Rosa, L.~Parrino, and M.G. Terzano.
\newblock Automatic detection of cyclic alternating pattern (cap) sequences in
  sleep: preliminary results.
\newblock {\em Clinical Neurophysiology}, 1999.

\bibitem{tracedataset}
D.~Roverso.
\newblock Multivariate temporal classification by windowed wavelet
  decomposition and recurrent networks.
\newblock In {\em ANS International Topical Meeting on Nuclear Plant
  Instrumentation, Control and Human-Machine Interface}, 2000.

\bibitem{Sakoe-Chiba}
H.~{Sakoe} and S.~{Chiba}.
\newblock Dynamic programming algorithm optimization for spoken word
  recognition.
\newblock {\em IEEE Transactions on Acoustics, Speech, and Signal Processing},
  1978.

\bibitem{DBLP:conf/ijcai/SariaDK11}
Suchi Saria, Andrew Duchi, and Daphne Koller.
\newblock Discovering deformable motifs in continuous time series data.
\newblock In {\em {IJCAI} 2011}.

\bibitem{Schafer2012}
Patrick Sch\"{a}fer and Mikael H\"{o}gqvist.
\newblock Sfa: A symbolic fourier approximation and index for similarity search
  in high dimensional datasets.
\newblock In {\em EDBT}, 2012.

\bibitem{DBLP:conf/edbt/Senin0WOGBCF15}
Pavel Senin, Jessica Lin, Xing Wang, Tim Oates, Sunil Gandhi, Arnold~P.
  Boedihardjo, Crystal Chen, and Susan Frankenstein.
\newblock Time series anomaly discovery with grammar-based compression.
\newblock In {\em EDBT}, 2015.

\bibitem{Shasha99}
Dennis Shasha.
\newblock Tuning time series queries in finance: Case studies and
  recommendations.
\newblock {\em {IEEE} Data Eng. Bull.}, 1999.

\bibitem{Shieh}
Jin Shieh and Eamonn Keogh.
\newblock {iSAX: Indexing and Mining Terabyte Sized Time Series}.
\newblock In {\em SIGKDD}, pages 623--631, 2008.

\bibitem{Shieh2009}
Jin Shieh and Eamonn Keogh.
\newblock {iSAX: disk-aware mining and indexing of massive time series
  datasets}.
\newblock {\em DMKD}, 19(1):24--57, 2009.

\bibitem{shieh2008sax}
Jin Shieh and Eamonn~J. Keogh.
\newblock \emph{i}sax: indexing and mining terabyte sized time series.
\newblock In {\em KDD}, 2008.

\bibitem{DBLP:conf/recomb/Sinha02}
Saurabh Sinha.
\newblock Discriminative motifs.
\newblock In {\em Proceedings of the Sixth Annual International Conference on
  Computational Biology, {RECOMB} 2002}, pages 291--298, 2002.

\bibitem{DBLP:journals/tkdd/SyedSKIG10}
Zeeshan Syed, Collin~M. Stultz, Manolis Kellis, Piotr Indyk, and John~V.
  Guttag.
\newblock Motif discovery in physiological datasets: {A} methodology for
  inferring predictive elements.
\newblock {\em {TKDD}}, 4(1):2:1--2:23, 2010.

\bibitem{CAP:dataset}
et~al. Terzano.
\newblock Atlas, rules, and recording techniques for the scoring of cyclic
  alternating pattern (cap) in human sleep.
\newblock Sleep Med 2001 Nov, 2(6):537-553.

\bibitem{1}
\url{www.mi.parisdescartes.fr/}\midtilde\url{mlinardi/ULISSE.html}.

\bibitem{Wordrecognition}
J.~Wang, A.~Balasubramanian, L.~Mojica de~la Vega, J.~Green, A.~Samal, and
  B.~Prabhakaran.
\newblock Word recognition from continuous articulatory movement time-series
  data using symbolic representations.
\newblock In {\em Workshop on Speech and Language Processing for Assistive
  Technologies. (SLPAT)}.

\bibitem{DBLP:journals/datamine/WangMDTSK13}
Xiaoyue Wang, Abdullah Mueen, Hui Ding, Goce Trajcevski, Peter Scheuermann, and
  Eamonn~J. Keogh.
\newblock Experimental comparison of representation methods and distance
  measures for time series data.
\newblock {\em Data Min. Knowl. Discov.}, 2013.

\bibitem{WangWPWH13}
Yang Wang, Peng Wang, Jian Pei, Wei Wang, and Sheng Huang.
\newblock A data-adaptive and dynamic segmentation index for whole matching on
  time series.
\newblock {\em {PVLDB}}, 2013.

\bibitem{DBLP:journals/pvldb/WangWPWH13}
Yang Wang, Peng Wang, Jian Pei, Wei Wang, and Sheng Huang.
\newblock A data-adaptive and dynamic segmentation index for whole matching on
  time series.
\newblock {\em {PVLDB}}, 6(10):793--804, 2013.

\bibitem{Dstree}
Yang Wang, Peng Wang, Jian Pei, Wei Wang, and Sheng Huang.
\newblock A data-adaptive and dynamic segmentation index for whole matching on
  time series.
\newblock {\em Proceedings of the VLDB Endowment}, 6:793--804, 08 2013.

\bibitem{Whitney}
CW~Whitney, DJ~Gottlieb, S~Redline, RG~Norman, RR~Dodge, E~Shahar, S~Surovec,
  and FJ~Nieto.
\newblock Reliability of scoring respiratory disturbance indices and sleep
  staging.
\newblock {\em Sleep}, November 1998.

\bibitem{Wu1996EfficientRF}
Daniel Wu, Divyakant Agrawal, Amr~El Abbadi, Ambuj~K. Singh, and Terence~R.
  Smith.
\newblock Efficient retrieval for browsing large image databases.
\newblock In {\em CIKM}, 1996.

\bibitem{YagoubiAMP17}
Djamel~Edine Yagoubi, Reza Akbarinia, Florent Masseglia, and Themis Palpanas.
\newblock Dpisax: Massively distributed partitioned isax.
\newblock In {\em {ICDM}}, 2017.

\bibitem{DBLP:conf/icdm/YagoubiAMP17}
Djamel~Edine Yagoubi, Reza Akbarinia, Florent Masseglia, and Themis Palpanas.
\newblock Dpisax: Massively distributed partitioned isax.
\newblock In {\em ICDM}, pages 1135--1140, 2017.

\bibitem{DBLP:conf/kdd/YankovKMCZ07}
Dragomir Yankov, Eamonn~J. Keogh, Jose Medina, Bill~Yuan{-}chi Chiu, and
  Victor~B. Zordan.
\newblock Detecting time series motifs under uniform scaling.
\newblock In {\em ACM}.

\bibitem{DBLP:conf/icdm/YankovKR07}
Dragomir Yankov, Eamonn~J. Keogh, and Umaa Rebbapragada.
\newblock Disk aware discord discovery: Finding unusual time series in terabyte
  sized datasets.
\newblock In {\em {IEEE} {(ICDM} 2007)}, 2007.

\bibitem{YankovKR08}
Dragomir Yankov, Eamonn~J. Keogh, and Umaa Rebbapragada.
\newblock Disk aware discord discovery: finding unusual time series in terabyte
  sized datasets.
\newblock {\em Knowl. Inf. Syst.}, 2008.

\bibitem{DBLP:journals/kais/YankovKR08}
Dragomir Yankov, Eamonn~J. Keogh, and Umaa Rebbapragada.
\newblock Disk aware discord discovery: finding unusual time series in terabyte
  sized datasets.
\newblock {\em Knowl. Inf. Syst.}, 2008.

\bibitem{YeK09}
Lexiang Ye and Eamonn~J. Keogh.
\newblock Time series shapelets: a new primitive for data mining.
\newblock In {\em KDD}, 2009.

\bibitem{YehZUBDDSMK16}
Chin{-}Chia~Michael Yeh, Yan Zhu, Liudmila Ulanova, Nurjahan Begum, Yifei Ding,
  Hoang~Anh Dau, Diego~Furtado Silva, Abdullah Mueen, and Eamonn~J. Keogh.
\newblock Matrix profile {I:} all pairs similarity joins for time series: {A}
  unifying view that includes motifs, discords and shapelets.
\newblock In {\em {IEEE} 16th International Conference on Data Mining, {ICDM}
  2016}.

\bibitem{YingchareonthawornchaiSRR13}
Sorrachai Yingchareonthawornchai, Haemwaan Sivaraks, Thanawin Rakthanmanon, and
  Chotirat~Ann Ratanamahatana.
\newblock Efficient proper length time series motif discovery.
\newblock In {\em 2013 {IEEE} {ICDM}}.

\bibitem{T-Store}
Kostas Zoumpatianos, Stratos Idreos, and Themis Palpanas.
\newblock T-store: Tunable storage for large sequential data.
\newblock In {\em North East Database Day (NEDB), Boston, MA, USA, January
  2019}.

\bibitem{DBLP:conf/sigmod/ZoumpatianosIP14}
Kostas Zoumpatianos, Stratos Idreos, and Themis Palpanas.
\newblock Indexing for interactive exploration of big data series.
\newblock In {\em SIGMOD}, 2014.

\bibitem{ZoumpatianosIP15}
Kostas Zoumpatianos, Stratos Idreos, and Themis Palpanas.
\newblock {RINSE:} interactive data series exploration with {ADS+}.
\newblock {\em {PVLDB}}, 2015.

\bibitem{DBLP:journals/vldb/ZoumpatianosIP16}
Kostas Zoumpatianos, Stratos Idreos, and Themis Palpanas.
\newblock {ADS:} the adaptive data series index.
\newblock {\em {VLDB} J. 25(6): 843-866}, 2016.

\bibitem{DBLP:conf/kdd/ZoumpatianosLPG15}
Kostas Zoumpatianos, Yin Lou, Themis Palpanas, and Johannes Gehrke.
\newblock Query workloads for data series indexes.
\newblock In {\em KDD}, 2015.

\bibitem{ZoumpatianosICDE2018}
Kostas Zoumpatianos and Themis Palpanas.
\newblock Data series management: Fulfilling the need for big sequence
  analytics.
\newblock In {\em ICDE}, 2018.

\end{thebibliography}
