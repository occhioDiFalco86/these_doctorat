

\section{Data series Indexes and Summarization} 
\label{sec:relWorkIndexes}
Several different kind of methods tackle the sequence matching problem (a.k.a similarity search). In this regard, one of the most considered techniques turns out to be data series indexing.

The literature includes several approaches, which are all based on the same principle: they first reduce the dimensionality of the data series by applying a summarization technique, which provides a compact form of the data (e.g., an index) that permits to prune the raw data space at search time.

Beyond the low representation error, a crucial and desirable property of a summarization is the lower bounding condition.
Given two (real valued) data series, namely $X,Y$, the lower bound condition is formulated as:

\begin{equation}
\label{LB}
D_{reduced}(Summ(X),Summ(Y)) \le D_{raw} (X,Y)
\end{equation}  

We denote, with $D_{reduced}$ the distance between the summaries of $X$ and $Y$ (given by the function $Summ()$).
On the other hand, $D_{raw}$ is the distance computed in the real space of the series values.
In general this latter distance is a metric.
In this work we consider the case of Euclidean distance lower bounding ~\cite{Faloutsos1994,Rafiei1998,shieh2008sax,DBLP:conf/sdm/BuLFKPM07,Assent2008,DBLP:journals/pvldb/WangWPWH13}, which obeys to the triangular inequality condition\cite{shieh2008sax}.
Furthermore, we consider also the case, where $D_{raw}$ is not a metric, e.g., Dynamic Time Warping~\cite{DBLP:journals/kais/KeoghR05}. 
The Equation~\ref{LB} serves the pruning strategy that is used in the proposed solutions to perform efficient subsequence matching.

The summarization techniques proposed in previous works are divided in two groups.
The first one includes those that perform a spectral decomposition of the series.
The most recent and popular methods are:

\begin{itemize}
	\item Discrete Fourier Transform (DFT)~\cite{DBLP:conf/fodo/AgrawalFS93}, which consists into the extraction of the first $c$ DFT coefficients (frequencies) of a data series.  
	
	\item Singular Value Decomposition (SVD)~\cite{Wu1996EfficientRF} compresses data series based on the SVD theorem, which states that a real valued matrix can be decomposed in a spectral form. Only the $k$ first components are used to represent the matrix, following the Principal Component Analysis (PCA). 
	
	\item Discrete Wavelet Transform (DWT)~\cite{DWT_indexing,DBLP:conf/icde/PopivanovM02} was introduced to overcome the limitation of previous approaches, such as the Fourier transform. Specifically, DWT can use an infinite family of basis functions as opposed to DFT, which utilizes only the exponential function. Moreover, DWT transformation is performed in linear time, whereas the fast Fourier transformation has an additional logarithmic factor.
\end{itemize}

We note that, all the aforementioned dimensionality reduction technique have laid the foundation for several state-of-the-art similarity search systems, at least for a decade.
More recently a suite of techniques based on piecewise approximation have been considered and evaluated:

\begin{itemize}
	\item Piecewise Flat Approximation (PFA)~\cite{PFA}. 
	\item Piecewise Linear Approximation (PLA)~\cite{Chen2007}. 
	\item Adaptive Piecewise Linear Approximation (APLA)~\cite{Chakrabarti2002}. 
	\item Piecewise Aggregate Approximation (PAA)~\cite{Keogh2000}. 
	\item Adaptive Piecewise Constant Approximation (APCA)~\cite{Chakrabarti2002}. 
\end{itemize}  

All these works show that simple piecewise-based approximation outperform previous spectral decomposition based techniques by being easy to compute and index, and they moreover satisfy the lower bound condition. 

Several index structure have been adapted, or specifically conceived to perform similarity search such as:

\begin{itemize}
	\item \textbf{$\mathbf{R^{*}}$-tree}~\cite{Beckmann1990}, which is a height-balanced spatial access method that partitions the data space into a hierarchy of nested overlapping rectangles.
	\item \textbf{M-tree}~\cite{Ciaccia1997}, which is a multidimensional, metric-space access method that uses hyper-spheres to divide the data entries according to their relative distances.
	\item \textbf{SFA trie}~\cite{Schafer2012}, which first summarizes the series using DFT, and it organizes them in a trie structure. 
	\item \textbf{DSTree}~\cite{Dstree}, the DSTree is a binary tree that is built upon a data series summarization, which extends the APCA capability providing dynamic segmentation of the data series. 
	\item \textbf{iSAX} (indexable Symbolic ApproXimation)~\cite{Shieh2009,CamerraPSK10,DBLP:journals/kais/CamerraSPRK14,ZoumpatianosIP15,KondylakisVLDB18} is a symbolic data series summarization built upon PAA. The summarization are stored in a hierarchical binary tree structure.
\end{itemize}
 
We revise here the Piecewise Aggregate Approximation (PAA) and the SAX approximation, which are the building block of the iSAX, the state-of-the-art indexing technique for similarity search in data series, and which we also use in our work.
 
\subsection{Piecewise Aggregate Approximation}

The Piecewise Aggregate Approximation (PAA) of a data series $D$, $PAA(D) = \{p_{1},...,p_{w}\}$, represents $D$ in a $w$-dimensional space by means of $w$ real-valued segments of length $s$, where the value of each segment is the mean of the corresponding values of $D$. 
We denote the first $k$ dimensions of $PAA(D)$, ($k \le w$), as $PAA(D)_{1,..,k}$.

\subsection{Symbolic Representation}

We introduce here the $iSAX$ representation of a data series $D$, which stands for \textit{indexable Symbolic Approximation}. The symbolic approximation is denoted by $SAX(D,w,|alphabet|)$, which is the representation of $PAA(D)$ by $w$ discrete coefficients, drawn from an alphabet of cardinality $|alphabet|$~\cite{shieh2008sax}. 

\begin{figure}[tb]
	\centering
	\hspace*{0.45cm}
	\includegraphics[trim={0cm 7cm 10cm 2cm},scale=0.85]{Chap1_ULISSE/figures/iSax_indexingR}
	\caption{Indexing of series D (and an inner node split).}
	\label{FigurePAASAXISAX2}
	%	\vspace*{-0.3cm}
\end{figure}

The main idea of the $iSAX$ representation (see Figure~\ref{FigurePAASAXISAX2}, top), is that the real-values space may be segmented by $|alphabet|-1$ breakpoints in $|alphabet|$ regions that are labeled by distinct symbols: binary values (e.g., with $|alphabet|=4$ the available labels are $\{00,01,10,11\}$). 
$iSAX$ assigns symbols to the $PAA$ coefficients, depending in which region they are located.

The iSAX data series index is a tree data structure~\cite{shieh2008sax,DBLP:journals/kais/CamerraSPRK14}, which hierarchically organizes the $iSAX$ representations of a data series collection. It is composed by three types of nodes (refer to Figure~\ref{FigurePAASAXISAX2}):
\begin{itemize}
\item[i] The root node points to $n$ children nodes (in the worst case $n=2^w$, when the series in the collection cover all possible iSAX representations).
\item[ii] Each inner node contains the iSAX representation of all the series below it.
\item [iii] Each leaf node contains both the iSAX representation \emph{and} the raw data of all the series inside it (in order to be able to prune false positives and produce exact, correct answers). 
\end{itemize}

When the number of series in a leaf node becomes greater than the maximum leaf capacity, the leaf splits: it becomes an inner node and creates two new leaves, by increasing the cardinality of one of the segments of its iSAX representation. 
The two refined $iSAX$ representations (new bit set to \textit{0} and \textit{1}) are assigned to the two new leaves. 

The similarity search algorithm, which is built upon the $iSAX$ index have state-of-the-art performance in solving the similarity search problem. 
It provides \textit{ultra fast} approximate search, since the structure of the index permits to visit first the most promising node (the one with the same $iSAX$ representation of the query).
Based on the same principle, the index permits to perform exact search.
To that extent, the search algorithm visits in order the leaf nodes, which contain the most similar representation to the query. 
In the case of $K-NN$ query answering, since the $iSAX$ representation respects the lower bounding condition (both for Euclidean and Dynamic Time Warping distance), the search is over when the \textit{best-so-far} distance is smaller than the actual lower bounding distance (computed by Equation~(\ref{LB})).
The pruned candidates (those that are not considered) are guaranteed to contain no false negatives.
 
In general, we note that the $iSAX$ technique, but also all the approaches mentioned above share a common limitation: they can only work for a fixed, predetermined data series length, which has to be decided before the index creation.

Specifically, we know that Equation~(\ref{LB}) holds \emph{iff} the summarized series are of the same length.
Hence, in this setting, the content of the query must be of fixed (predefined) length.

This limitation has been already studied. In the next part we present the details of the available solutions that we have been proposed so far.
In the next part, we denote the query-by-content problem, where the query can be of arbitrary length as Variable Length Similarity Search.


\subsection{Indexing Techniques for Variable Length Similarity Search} 

At first, we note that Variable Length Similarity Search has been proposed only in the $\epsilon$-range search variant, where the search outcome contains all the subsequence that have distance smaller or equal to $\epsilon$.
This means that \textit{to the best of our knowledge} no indexing techniques for exact $K-NN$ search of variable length is available in the literature.

Faloustos et.al~\cite{Faloutsos1994} proposed an indexing technique for variable length similarity search query called I-adaptive index, which is the first (seminal) approach that treats this problem.

In details, the I-adaptive index is built extracting the subsequences of a fixed length, which are grouped in MBRs (Minimum Bounding Rectangles) that form the building blocks of a R-tree.
The authors presented two search methods, Prefix Search and Multiple Search, which work for arbitrary length queries.  
The first uses an index search using a fixed prefix of the query sequence. On the other hand, Multiple Search splits the query sequence in non-overlapping subsequences of fixed length and performs queries for each of these subsequences.

In a later work, Kahveci and Singh~\cite{914838}, proposed MRI (Multi Resolution Index), which is the first technique based on the construction of multiple indexes for variable length similarity search query. In this work, the authors clearly shown the limitation of Prefix Search and Multiple Search. The main disadvantage of these two approaches turns out to be the poor exploitation of the whole query sequence, since in the first, only the prefix subpart is used to perform a range query. If the prefix length sensibly differs from the entire query sequence length, the search space we need to consider can explode exponentially. The Multiple Search instead, segments the query in equal length parts, and perform a separate search with each segment, refining the search range at the end of each query. 
In practice, each query has a similar probability to prune the search space, with no real benefit deriving from the range refinement.
To that extent, storing subsequences at different resolution (building indexes for different series lengths) provides an effective improvement of Multiple Search, since a greater part of a single query is used to answer the query, considering furthermore multiple windows at different length. This provides more elasticity over the query length variability, improving search efficiency.

Kadiyala and Shiri in their work~\cite{DBLP:journals/kais/KadiyalaS08} have redesigned the MRI construction, exploiting the overlapping of the subsequences at different resolutions.
This avoids to consider unnecessary subsequences, at the index building stage, drastically decreasing the indexing size and construction time.
This new indexing technique, called Compact Multi Resolution Index (CMRI), has a space requirement, which is 99\% smaller the one of MRI. The authors moreover, redefined the Multiple Search in order to access the disk only at the 
end of the multiple queries performed, optimizing also the range search proposed in MRI.

As a matter of fact, CMRI is the state-of-the-art Multi Resolution indexing technique for answering similarity search query of variable length. This solution is shown to be a strong and incremental contribution over the previous works. 


More recently, Wu et al.~\cite{KV-match} have proposed the KV-Match index, which supports $\epsilon$-range similarity search queries of variable length, using both Z-normalized Euclidean and DTW distances. 
The idea of this technique is similar to the CMRI one, since many indexes are built for different subsequence window lengths, which are considered at query time using multiple query segments. 
We note that for Z-normalized sequences, this method provides exact answers only for \textit{constrained} $\epsilon$-range search. 
To this effect, two new parameters that constrain the mean and the standard deviation of a valid result are considered at query answering time.

\subsection{Sequential Scan techniques for Similarity Search}

Recent works have shown that similarity search based on sequential scans can be performed efficiently~\cite{DBLP:conf/kdd/RakthanmanonCMBWZZK12,}.
These techniques aim to prune the search space exploiting the overlapping of the query candidates (subsequences).
To that extent, two different methods have been proposed:

\begin{itemize}
	\item UCR Suite (Rakthanmanon et al.~\cite{DBLP:conf/kdd/RakthanmanonCMBWZZK12}), which is an optimized serial scan algorithm for subsequence similarity search on a single long series. UCR Suite applies the following optimizations: (a) early-abandoning consists into stopping (abandon) the distance computations as soon as the partially calculated distance is greater than the best-so-far distance in $K-NN$ search; (b) query points-reordering, which consists into sorting in descending order the points of the query according their absolute values. Specifically, the authors note that, when the data series are Z-normalized (mean of each series equal to \textit{zero} and standard deviation equal to \textit{one}), the points that are the farthest away from zero are likely to contribute the most to the final distance quantity. This heuristic allows to abandon earlier the distance computations (c) lower bound of DTW distance, here the author propose a multi-step lower bounding computation, which takes linear time, and permits to prune DTW distance calculations. 
	
	\item MASS algorithm~\cite{DBLP:conf/icdm/MueenHE14}, which performs distance calculation in Frequency domain, namely on the Fourier transformation of the data series.
	This permits to compute the distance between a data series query $Q$ and all the subsequences of length $|Q|$ in a series $D$ in $O(|D|log(D))$ time. The complexity of the proposed algorithm does not depend to the length of $Q$.

\end{itemize}

The sequential scan techniques are mostly beneficial when the dataset consists of a single, very long data series, and queries are looking for potential matches in small subsequences of this long data series. 
Such approaches, in general, do not provide a large benefit when the dataset is composed of a large number of small data series, namely when the candidates do not overlap (have points in common).


\subsection{Summary}

\begin{figure}[tb]
	\centering
	\hspace*{0.45cm}
	\includegraphics[trim={0cm 12cm 10cm 2cm},scale=0.8]{Chap1_ULISSE/figures/index_summ}
	\caption{Summary of current state of the art solutions for answering similarity search query in data series collections.}
	\label{IndSumm}
\end{figure}

In Figure~\ref{IndSumm}, we report a summary of the current state of the art solutions for answering similarity search query in data series collections. 
For each line in the table, we report the type of index and the summarization technique applied to reduce the data series dimensionality, along with the kind of search (approximate and exact), the considered distance measures, the support of Z-Normalized query, and the possibility to issue queries of variable-length.   
We note that no indexing solution supports Z-Normalized similarity search queries of variable length.
To that extent, UCR Suite represents a complete solution, which is based on sequential raw data scan and distances computation pruning.         
In this work, we want to propose a solution that can also draw the benefit from data series summarization and indexing.


\section{Motif and Discord Discovery} 

While research on data series similarity measures and data series query-by-content date back to the early 1990s~\cite{DBLP:conf/sofsem/Palpanas16}, \textit{data series motifs} and \textit{data series discords} were both introduced just fifteen and twelve years ago, respectively~\cite{DBLP:conf/kdd/ChiuKL03,DBLP:conf/adma/FuLKL06}.
Following their definition, there was an explosion of interest in their use for diverse applications. 
There exist analogies between \textit{data series motifs} and sequence \textit{motifs} (in DNA), which have been exploited. 
For example, discriminative motifs in bioinformatics~\cite{DBLP:conf/recomb/Sinha02} inspired discriminative data series motifs (i.e., data series shapelets)~\cite{citrusProduction}. 
Likewise, the work of Grabocka et al.~\cite{DBLP:journals/tkdd/GrabockaSS16} on generating idealized motifs, is similar to the idea of consensus sequence (or canonical sequence) in molecular biology.
The literature on the general data series motif and discord search has been recently studied and referenced in several different recent studies~\cite{ZhuZSYFMBK16,YehZUBDDSMK16}.
In the next parts we present the relevant techniques, reporting their characteristics and their \textit{modus-operandi} as well.

\subsection{Motif Discovery Techniques}

The QUICK MOTIF~\cite{DBLP:conf/icde/LiUYG15} and STOMP~\cite{ZhuZSYFMBK16} algorithms represent the state of the art for fixed-length motif discovery. 
QUICK MOTIF works building a summarized representation of the data using Piecewise Aggregate Approximation (PAA), and arranges these summaries in Minimum Bounding Rectangles (MBRs) in a Hilbert R-Tree index.
The algorithm then prunes the search space based on the MBRs.
On the other hand, STOMP is based on the computation of the \textit{matrix profile}, in order to discover the best matches for each subsequence.  
The smallest of these matches is called the motif pair. 
In general, we observe that both the above approaches solve a restricted version of the problem: they discover motif sets of cardinality two (i.e., motif pairs) of a fixed, predefined length. 

The main idea of our work is to remove these limitations proposing a general and efficient solution, which can evaluate more candidates of various lengths.
To that extent, we note that there are only three studies that deal with issues of variable length motifs, and attempt to address them~\cite{MinnenIES07,Gao0R16,YingchareonthawornchaiSRR13,DBLP:journals/datamine/GaoL18}. 
While these studies are pioneers in demonstrating the \textit{utility} of variable length motifs, they cannot serve as practical solutions to the task at hand for two reasons: 
(i) they are all approximate, while we need to produce exact results; and (ii) they require setting many parameters (most of which are unintuitive).
Approximate algorithms can be very useful in many contexts, if the amount of error can be bounded, or at least known. 
In certain cases, such as when analyzing seismological data, the threat of litigation, or even criminal proceedings~\cite{Bertolaso}, would make any analyst reluctant to use an approximate algorithm.

The other work to explicitly consider variable length motifs is MOEN~\cite{DBLP:journals/kais/MueenC15}. 
Its operation is based on the distance computation of subsequences of increasing length, and a corresponding pruning strategy based on upper and lower bounds of the distance computed for the smaller length subsequences.
Unlike the algorithms discussed above, MOEN is exact and requires few parameters. 
However, it has been tuned for producing only a single motif pair for each length in the range.


\subsection{Discord Discovery Techniques}

Exact discord discovery is a problem that has attracted lots of attention. 
The approaches that have been proposed in the literature can be divided in the following two different categories.
First, the \textit{index-based} solutions, i.e., Haar wavelets~\cite{DBLP:conf/adma/FuLKL06,DBLP:conf/sdm/BuLFKPM07} and SAX~\cite{Keogh2005,Keogh2007,DBLP:conf/edbt/Senin0WOGBCF15}, where series are first discretized and then inserted in an index structure that supports fast similarity search. 
Second, the \textit{sequential scan} solutions~\cite{YehZUBDDSMK16,Liu2009,DBLP:conf/adma/FuLKL06,Parameter-Free_Discord,DBLP:conf/icdm/YankovKR07,ZhuZSYFMBK16}, which consider the direct subsequence pairwise distance computations, and the corresponding search space optimization.

Indexing techniques are based on the discretization of the real valued data series, with several user defined parameters required for this operation. 
In general, selecting and tuning these parameters is not trivial, and the choices made may influence the behavior of the discord discovery algorithm, since it is strictly dependent on the quality of the data representation. 
In this regard, the most recent work in this category, GrammarViz~\cite{DBLP:conf/edbt/Senin0WOGBCF15}, proposes a method of \topkfirstd discord search based on grammar compression of data series represented by discrete SAX coefficients.
These representations are then inserted in a hierarchical structure, which permits to prune unpromising candidates subsequences.
The intuition is that rare patterns are assigned to representations that have high \textit{Kolmogorov complexity}. This means that a rare SAX string is not compressible, due to the lack of repeated terms.

The state of the art for the sequential scan methods is represented by STOMP, since computing the matrix profile permits to discover, in the same fashion as motifs, the \topkfirstd discords. 
Surprisingly, there exists just one work that addresses the problem of $m^{th}$ discord discovery~\cite{DBLP:conf/icdm/YankovKR07}. 
The authors of this work, proposed the Disk Aware discords Discovery algorithm (DAD), which is based on a smart sequential scan performed on disk resident data.
This algorithm is divided in two parts. 
The first is discord candidate selection, where it identifies the sequences, whose nearest neighbor distance is less than a predefined range. 
The second part, which is called refinement, is applied in order to find the exact discords among the candidates. 
Despite the good performance that this algorithm exhibits in finding the first discord, when $m$ is greater than one, it becomes hard to estimate an effective range.
In turn, this leads to scalability problems, due to the explosion of the number of distances to compute.

In summary, while there exists a large and growing body of work on the motif and discord discovery problems, 
the idea, which motivates this work is to offer the first scalable, parameter-light, \textit{exact} variable-length algorithm in the literature for solving both these problems. 










