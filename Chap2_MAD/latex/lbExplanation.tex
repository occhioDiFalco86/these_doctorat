\subsection{The Lower Bound Distance Profile}
 \label{sec:lowwerbounddistanceprofile}
Before introducing the lower bound distance profile, let us first investigate its basic element: the lower bound Euclidean distance.

%\noindent{\bfThe Lower Bound Euclidean Distance}
Assume that we already know the z-normalized Euclidean distance $d_{i,j}^\ell$ between two subsequences of length $\ell$: $T_{i,\ell}$ and $T_{j,\ell}$, and we are now estimating the distance between two longer subsequences of length $\ell+k$: $T_{i,\ell+k}$ and $T_{j,\ell+k}$. Our problem can be stated as follows: given $T_{i,\ell}$, $T_{j,\ell}$ and $T_{j,\ell+k}$ (but not the last $k$ values of $T_{i,\ell+k}$), is it possible to provide a lower bound function $LB(d_{i,j}^{\ell+k})$, such that $LB(d_{i,j}^{\ell+k}) \le d_{i,j}^{\ell+k}$?
This problem is visualized in Figure \ref{LBRanking} .

\begin{figure}[tb]
	\centering
	\includegraphics[trim={-1cm 3cm 0cm 3cm},scale=0.3]{Chap2_MAD/figures/ArbitraryPointsSUB}
	\caption{Increasing the subsequence length from $\ell$ to $\ell+k$.}
	\label{LBRanking}	
\end{figure}



One may assume that we can simply set $LB(d_{i,j}^{\ell+k}) = d_{i,j}^\ell$ by assuming that the last $k$ values of $T_{i,\ell+k}$ are the same as the last $k$ values of $T_{j,\ell+k}$. However, this is not an answer to our problem, as we need to evaluate z-normalized Euclidean distances, which are not simple Euclidean distances. The mean and standard deviation of a subsequence can change as we increase its length, so we need to re-normalize both $T_{i,\ell+k}$ and $T_{j,\ell+k}$.
Assume that the mean and standard deviation of $T_{x,y}$ are $\mu_{x,y}$ and $\sigma_{x,y}$, respectively (i.e. $T_{j,\ell+k}$ corresponds to $\mu_{j,\ell+k}$ and $\sigma_{j,\ell+k}$). Since we do not know the last $k$ values of $T_{i,\ell+k}$, both $\mu_{i,\ell+k}$ and $\sigma_{i,\ell+k}$ are unknown and can thus be regarded as variables. We recall that $t_{i}$ denotes the $i^{th}$ point of a generic sequence $T$ (or a subsequence $T_{a,b}$), we thus have the following:



\[ d_{i,j}^{\ell+k}  \ge  \substack{min \\ \mu_{i,\ell+k},\sigma_{i,\ell+k}}  \sqrt{\sum_{p=1}^{\ell}(\frac{t_{i+p-1}-\mu_{i,\ell+k}}{\sigma_{i,\ell+k}} - \frac{t_{j+p-1}-\mu_{j,\ell+k}}{\sigma_{j,\ell+k}})^2 } \]



\[= \substack{min \\ \mu_{i,\ell+k},\sigma_{i,\ell+k}} \frac{\sigma_{j,\ell}}{\sigma_{j,\ell+k}}  \sqrt{\sum_{p=1}^{\ell}(\frac{t_{i+p-1}-\mu_{i,\ell+k}}{\frac{\sigma_{i,\ell+k}\sigma_{j,\ell}}{\sigma_{j,\ell+k}}} - \frac{t_{j+p-1}-\mu_{j,\ell+k}}{\sigma_{j,\ell}})^2 } \]

Here, we substitute the variables $\mu_{i,\ell+k}$ and $\sigma_{i,\ell+k}$, respectively with $\mu'$ and $\sigma'$. Hence, we obtain:


\begin{equation}
\label{eq:lowerBoundFormula}
= \substack{min \\ \mu',\sigma'} \frac{\sigma_{j,\ell}}{\sigma_{j,\ell+k}}  \sqrt{\sum_{p=1}^{\ell}(\frac{t_{i+p-1}-\mu'}{\sigma'} - \frac{t_{j+p-1}-\mu_{j,\ell}}{\sigma_{j,\ell}})^2 }
\end{equation}

Clearly, the minimum value shown in Eq.~\ref{eq:lowerBoundFormula} can be set as $LB(d_{i,j}^{\ell+k})$.
We can obtain $LB(d_{i,j}^{\ell+k})$ by solving $\frac{\partial LB(d_{i,j}^{\ell+k})}{\partial \mu'}=0$ and $\frac{\partial LB(d_{i,j}^{\ell+k})}{\partial \sigma'}=0$:

\begin{equation}
	\label{eq:lowerBoundFinalFormula}
	LB(d_{i,j}^{\ell+k})=\begin{cases}
	\sqrt{{\ell}} \frac{\sigma_{j,\ell}}{\sigma_{j,\ell+k}} & \text{if $q_{i,j} \leq 0$}\\
	\sqrt{{\ell}(1- q^2_{i,j})} \frac{\sigma_{j,\ell}}{\sigma_{j,\ell+k}} & \text{otherwise}
	\end{cases}
\end{equation}

where $q_{i,j} = \frac{\sum_{p=1}^{\ell} \frac{(t_{j+p-1} t_{i+p-1})}{\ell} - \mu_{i,\ell}\mu_{j,\ell}}{\sigma_{i,\ell}\sigma_{j,\ell}}$.

$LB(d^{\ell+k}_{i,j})$ yields the minimum possible z-normalized Euclidean distance between $T_{i,\ell+k}$ and $T_{j,\ell+k}$, given $T_{i,\ell}$, $T_{j,\ell}$ and $T_{j,\ell+k}$ (but not the last $k$ values of $T_{i,\ell+k})$.
Now that we have obtained the lower bound Euclidean distance between two subsequences, we are able to introduce the lower bound distance profile.
%\vspace{0.1cm}\\
%\noindent{\bf The Ranked Lower Bound Distance Profile}

Using Eq.~\ref{eq:lowerBoundFinalFormula}, we can evaluate the lower bound Euclidean distance between $T_{j,\ell+k}$ and every subsequence of length $\ell+k$ in $T$. By putting the results in a vector, we obtain the lower bound distance profile $LB(D_j^{\ell+k})$ corresponding to subsequence $T_{j,\ell+k}$: $LB(D_j^{\ell+k})$ = $LB(d_{1,j}^{\ell+k})$, $LB(d_{2,j}^{\ell+k})$, ...,$LB(d_{n-\ell-k+1,j}^{\ell+k})$.
If we sort the components of $LB(D_j^{\ell+k})$ in an ascending order, we can obtain the ranked lower bound distance profile: $LB_{ranked} (D_j^{\ell+k})= LB(d_{r_1,j}^{\ell+k}), LB(d_{r_2,j}^{\ell+k}),\\...,LB(d_{r_{n-\ell-k+1},j}^{\ell+k})$, where $LB(d_{r_1,j}^{\ell+k}) \le LB(d_{r_2,j}^{\ell+k}) \le ... \\\le LB(d_{r_{n-\ell-k+1},j}^{\ell+k})$.
%\vspace{0.1cm}\\

We would like to use this ranked lower bound distance profile to accelerate our computation. Assume that we have a best-so-far pair of motifs with a distance $dist_{BSF}$.
If we examine the $p^{th}$ element in the ranked lower bound distance profile and find that $LB(d_{r_p,j}^{\ell+k}) > dist_{BSF}$, then we do not need to calculate the exact distance for  $d_{r_p,j}^{\ell+k}, d_{r_{p+1},j}^{\ell+k},...,d_{r_{n-\ell-k+1},j}^{\ell+k}$ anymore, as they cannot be smaller than $dist_{BSF}$.
Based on this observation, our strategy is as follows. We set a small, fixed value for $p$. Then, for every $j$, we evaluate whether $LB(d_{r_p,j}^{\ell+k}) > dist_{BSF}$ is true: if it is, we only calculate $d_{r_1,j}^{\ell+k}, d_{r_2,j}^{\ell+k},...,d_{r_{p-1},j}^{\ell+k}$. If it is not,  we compute all the elements of $D_j^{\ell+k}$. We update $dist_{BSF}$ whenever a smaller distance value is observed. In the best case, we just need to calculate $\mathcal{O}(np)$ exact distance values to obtain the motif of length $l+k$.
Note that the order of the ranked lower bound distance profile is preserved for every $k$. That is to say, if $LB(d_{a,j}^{\ell+k}) \le LB(d_{b,j}^{\ell+k})$, then $LB(d_{a,j}^{\ell+k+1}) \le LB(d_{b,j}^{\ell+k+1})$. This is because the only component in Eq.~\ref{eq:lowerBoundFinalFormula} related to $k$ is $\sigma_{j,\ell+k}$. When we increase $k$ by 1, we are just performing a linear transformation for the lower bound distance: $LB(d_{i,j}^{\ell+k+1})=LB(d_{i,j}^{\ell+k}) \sigma_{j,\ell+k}/\sigma_{j,\ell+k+1}$. Therefore, we have $LB(d_{r_p,j}^{\ell+k+1})=LB(d_{r_p,j}^{\ell+k}) \sigma_{j,\ell+k}/\sigma_{j,\ell+k+1}$ , and the ranking is preserved for every $k$.


