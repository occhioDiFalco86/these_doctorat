%\section{Introduction}

Data series motif discovery represents one of the most useful primitives for data series mining, with applications to many domains, such as robotics, entomology, seismology, medicine, and climatology, and others. The state-of-the-art motif discovery tools still require the user to provide the motif length.  
Yet, in several cases, the choice of motif length is critical for their detection. 
Unfortunately, the obvious brute-force solution, which tests all lengths within a given range, is computationally untenable, and does not provide any support for ranking motifs at different resolutions (i.e., lengths).     

In this chapter, we present a prototype system, which implements the scalable motif discovery algorithm (VALMOD) that efficiently finds all motifs in a given range of lengths, and outputs a length-invariant ranking of motifs. 
Furthermore, the prototype supports the analysis process by means of a newly proposed meta-data structure that helps the user to select the most promising pattern length. We illustrate in detail the steps of the proposed approach, showcasing how our algorithm and corresponding graphical interfaces enable users to efficiently identify the correct motifs.


%\section{State of the art motif discovery.}
%
%Over the last decade, data series motif discovery has emerged as perhaps the most used primitive for data series data mining, and it has many applications to a wide variety of domains~\cite{Whitney,DBLP:conf/kdd/YankovKMCZ07}, including classification, clustering, and rule discovery. More recently, there has been substantial progress on the scalability of motif discovery, and now massive datasets can be routinely searched on conventional hardware~\cite{Whitney}. 
%The state-of-the art algorithm~\cite{ZhuZSYFMBK16} only requires the user to set a single parameter, which is the desired length of the motifs. Moreover, the motif mining is supported by the \textit{Matrix profile} output, which is a meta data series storing the z-normalized Euclidean distance between each subsequence and its nearest neighbor. The Matrix profile does not exclusively provide the motif, i.e., the subsequence pair with the smallest distance, but also permits to rank and filter out the other pairs, giving also a convenient and graphical representation of their occurrences and proximity. In order to categorize motifs, we call the $k$ subsequences, with the $k$ smallest best match distances, top-$k$ motif pairs.      

\section{Motif discovery of different lengths.}

Exact Motif discovery has merely become a single input parameter problem, namely the length of the patterns we want to mine.
Unfortunately, this technique comes with an important lack. It does not provide an effective solution for trying several motif length in a range.
If one has no cues about an effective fixed length, the simplest solution would be to run the algorithm over all lengths in the range and rank the various motifs discovered, picking eventually the patterns, which contain the desired insight. 
Clearly, this possibility is not optimal for at least two reasons; the scalability, since finding motif of one fixed length takes $O(n^2)$ time, and also because it does not provide an effective way to compare motifs of different lengths.
In Chapter~\ref{chapterMAD} we introduced \VALMOD, the first approach for mining top-$k$ motif pairs of variable length, which is up to orders of magnitude faster/more scalable than the alternatives that have been proposed in the literature.


\begin{figure}[tb]
	\centering
	\includegraphics[trim={0cm 11cm 3cm 3.8cm},scale=0.60]{Chap2_MAD/figures/mp_valmap}
	\caption{\textit{Left}) \textit{(a)} Snippet of ECG recording with highlighted motifs of length \textit{50}, \textit{(b)} Matrix profile computed with subsequence length \textit{50}. \textit{(c)} Index profile, reporting the offsets of the best match. \textit{Right}) \textit{(d)} Snippet of ECG recording with highlighted motifs of length \textit{400}, \textit{(e)} VALMAP $MP^n$, \textit{(f)} VALMAP Length profile.}
	\label{mpECG}
	\vspace*{-0.3cm}	
\end{figure}


Here, in order to show the superiority of variable-length motif discovery, we consider the following example.
In Figure~\ref{mpECG} (left) we depict a snippet of an Electrocardiogram (ECG) recording in (a), paired with its Matrix profile, computed with fixed subsequence length: $\ell=50$ in (b). 
Note that each value in the Matrix profile corresponds to a point in the data, which is the representative starting point of a subsequence of length $\ell$.
Hence, given a data series $D$ of length $|D|$, a Matrix profile records the $|D|-\ell+1$ nearest neighbor distances, avoiding trivial matches. 
In Figure~\ref{mpECG}.(c) we plot the Index profile, which contains the offsets of the best matches.

Looking at the Matrix profile in this example, we note four deep valleys, which suggest the presence of very close matches, namely the motifs. Starting from the Matrix profile, it suffices to follow the dotted lines upwards, in order to detect the motifs, and downwards for finding the position of each subsequence best match.   
Despite the motifs (heartbeats) are easily detectable \textit{to the naked eye}, since the snippet is relatively short, the highlighted motifs in Figure~\ref{mpECG}.(a) (red/orange subsequences), just report the second half of a ventricular contraction, giving thus a partial and unsatisfactory result. 


%\section{Overview of VALMOD Motif Management}
%\label{sec:VALMOD Motif Management}
%
%As introduced in Chapter~\ref{chapterMAD}, our algorithm, VALMOD (Variable Length Motif Discovery), given a data series $D$, starts by computing the Matrix profile using the smallest subsequence length, namely $\ell_{min}$, within a specified input range $[\ell_{min},\ell_{max}]$.
%The key idea of our approach is to minimize the work that needs to be done for succeeding subsequence lengths ($\ell_{min}+1$, $\ell_{min}+2$, $\ldots$, $\ell_{max}$).
%Here, we present an overview of the main components and the idea of our algorithm showing a short example in Figure~\ref{ExampleVALMOD}. 
%
%
%\begin{figure}[tb]
%	\centering
%	\includegraphics[trim={0cm 0cm 0cm 4cm}, scale=0.5]{Chap2_MAD/figures/Ex_2}
%	\includegraphics[trim={2cm 7cm 0cm 9.3cm}, scale=0.5]{Chap2_MAD/figures/Ex_3}
%	\caption{ \textit{(a)} ECG snippet with distance profile of subsequence $D_{160,600}$, \textit{(b)} Partial distance profiles computation for length \textit{601}.}
%	\label{ExampleVALMOD} 
%	\vspace*{-0.4cm}
%\end{figure} 
%
%We start to consider the data series $D$ in (a) (snippet of ECG recording). To compute the Matrix profile, \VALMOD considers all the contiguous subsequences of length $\ell{min}$, computing for each one the \textit{Distance profile} in $O(|D|)$ time. This latter, contains the z-normalized Euclidean distance between a subsequence and all the other in $D$. In Figure~\ref{ExampleVALMOD}.(a) we report a distance profile for the subsequence $D_{160,600}$ (the subscript denotes offset=\textit{160} and length=\textit{600}). The minimum distance of each distance profile is a point of the Matrix profile.
%
%We moreover introduced a new lower bounding distance\cite{VALMOD}, which lower bounds the true Euclidean distances between longer subsequences in the distance profiles. 
%We initially compute this lower bound from scratch, using as a base the true Euclidean distances computation of subsequences with length \textit{600}. For the larger lengths, we update the lower bound, considering only the variation generated by the trailing points in the longer subsequences. This measure enjoys an important property: if we rank the subsequences according to this measure (ascending order), the same rank will be preserved along all the lower bound updates. 
%We want to exploit this property, in order to prune computation. Hence, when the distance profiles are computed (in this example for length=\textit{600}), we keep in memory the $p$ Euclidean distances, which have the smallest lower bounding distance (LB); this is done for each distance profile. 
%We show in Figure~\ref{ExampleVALMOD}.(b)  how the algorithm proceeds for the length \textit{601}. Instead of computing from scratch the whole distance profiles, we consider just the elements we stored in the previous step. Here, each distance profile is denoted as \textit{partial distance profile}. We proceed computing the true Euclidean distances of each partial distance profile, updating the relative LB (this result is depicted in Figure~\ref{ExampleVALMOD}.(b). After this operation, we may have two cases: if in a new computed distance profile the minimum true distance (\textit{minDist}) is shorter than the maximum lower bound (\textit{maxLB}), we know that no elements, among those not computed, can be smaller than minDist. In this case a partial distance profile becomes a \textit{valid distance profile}, as in the case of the subsequence $D_{160,601}$.
%On the other hand, when \textit{maxLB} is smaller than \textit{minDist}, as in the case of subsequence $D_{620,601}$, no true minimum distance is found within the distance profile.
%At the end of this process, we pick the minimum \textit{maxLB} of all the non-valid distance profile, which is denoted as \textit{minLBAbs}. Hence, all the \textit{mindist} in the valid (parital) distance profiles, smaller than \textit{minAbsLB} are considered top-$k$ motif distances.
%If no \textit{mindist} are smaller than \textit{minAbsLB}, we recompute only the distance profiles, which have the \textit{maxLB} smaller than the smallest \textit{mindist} found, since only those may contain better matches than the already computed ones.  
%We keep extracting in this way, the top-$k$ motifs of each length, until $\ell{max}$.


\section{VALMAP data structure.}

In the previous chapter, we introduced a motif rank that weights the subsequences importance according to the ratio distance-length.
Furthermore, we want to know, whether and how the motif pairs changes, helping the user to extract the desired insights at the correct length. 
To that extent, we introduce a new meta-data, called Variable Length Matrix Profile (\VALMAP), maintaining the same logic and structure of the Matrix profile depicted in Figure~\ref{mpECG} (top), with the difference that this new structure carries length normalized distances and it is coupled with a new vector called \textit{Length profile}, which contains the lengths of the subsequences.
More formally, given a data series $D$, and a range of subsequence lengths, whose extremes are denoted by $\ell_{min}$ and $\ell_{max}$, we define \VALMAP as a triple $\langle MP^{n} \in \mathbb{R}^{|D|-\ell_{min} + 1}, IP \in \mathbb{N}^{|D|-\ell_{min} + 1},  LP \in \mathbb{N}^{|D|-\ell_{min} + 1} \rangle$, where $MP^{n}$ is the Matrix profile containing length normalized distances, whereas $IP$ and $LP$ are the relative Index and Length Profile.
If we consider just a fixed length, \VALMAP will coincide with the length normalized version of the Matrix profile, with a flat Length profile.
This is basically the structure that \VALMOD builds, considering subsequences of length $\ell_{min}$.
In the second stage, we can update \VALMAP using the top-$k$ motif pairs, computed for each length until $\ell_{max}$. 
We thus consider each ($D_{i,\ell_{min}+1} , D_{j,\ell_{min}+1} $) $\in$ top-$k$ motif pairs, where $i,j$ are the subsequences offsets, $\ell_{min}+1$ their lengths and $d^{n}_{i,j}$ their length normalized Euclidean distance. Note that in a motif pair the right subsequence is the one with the absolute shortest distance to the one at the left. 
Hence, \textit{VALMAP}, $MP^{n}[i]$ is updated with $d^{n}_{i,j}$ if $d^{n}_{i,j} < MP^{n}[i]$, which was containing the distance between $D_{i,\ell_{min}}$ and its best match. If this update takes place, the Index and Length profile are respectively assigned with $j$, the offset of the new best match, and $\ell_{min}+1$ the new length.
The update operation takes place for each top-$k$ motif pair of any length between $\ell_{min}$ and $\ell_{max}$.
Once the algorithms ends, \VALMAP contains a picture of the motif pairs showing, at which length the last update takes place. If a motif pair is updated, this implies that a longer pattern represent a better match and thus it might reveal either a new event or the same event lasting longer.


\noindent{\bf Example of VALMAP Expressiveness.} In order to show the expressiveness of \textit{VALMAP}, we ran \VALMOD on the ECG data snippet previously considered, showing the \VALMAP structure in Figure~\ref{mpECG} (right). We use the following input parameter: $\ell_{min}=50$ and $\ell_{max}=400$.  
We note that \VALMAP reports the motif with the shortest length normalized distance of length \text{56}, which is the same partial event detected by the Matrix profile in the fixed length case, at the top of the picture.  
If we look at the Length profile in Figure~\ref{mpECG}.(f), we observe that, at an earlier time than the discovered motifs pair, a sequence of contiguous updates took place, as we reported. The subsequences concerned have distances almost as short as the one of the best motifs in \VALMAP~, thus, remaining longer and possibly valid matches.

In Figure~\ref{mpECG}.(d) we depict and highlight the motif pair of length \textit{400}. Immediately, we can note that, the subsequences in red, which compose this motif, are a better representation of a recurrent heartbeat. In fact, the two typical components (\textit{Artia and Ventricles contract}) are correctly detected.  



\begin{figure}[tb]
	\centering
	\includegraphics[trim={5cm 5.5cm 16cm 4cm},scale=0.5]{Chap2_MAD/figures/arch_flow.pdf}
	\caption{Architecture of VALMOD system. }
	\label{fig:archFlow}
	\vspace*{-0.3cm}
\end{figure}




\section{System Description}

We now describe the architecture of the prototype we propose, depicted also in Figure~\ref{fig:archFlow}.
The input is represented by a data series of interest.
As a starting point, the user has the possibility to inspect the data and also setting the desired parameter (lengths range [$\ell_{min}$,$\ell_{max}$]).
Afterwards, she can run the \VALMOD algorithm, which is a part of the system back-end we implemented in C. 
Once terminated, \VALMOD outputs the \VALMAP meta-data. This latter is thus sent to the front-end, implemented in Python. 
Here, the user can interact with the system analyzing the showcased elements, such as:
\begin{itemize}
	\item the checkpoints of the VALMAP, namely all the updates occurred from the length $\ell{min}$ till the desired length, selected with a dedicated slider. 
	\item all the top-$k$ motifs of variable length, which \VALMAP reports. 
	\item expand a selected motif pair to the relative Motif Set, containing all the similar subsequences of the pair in the data. 
\end{itemize}
%In Figure~\ref{fig:screenShot} we show a screen-shot of the VALMAP analysis in our demonstration.









\section{Prototype System}

We now present the functionality of our prototype.
We depict a screen-shot of our application in Figure~\ref{fig:screenShot}, where the user imports (top of Figure~\ref{fig:screenShot}) a dataset containing the recordings of the global active electric power in France for the period 2006-2008 (GAP)~\cite{Lichman:2013}.

\begin{figure}[!tb]
	\centering
	\includegraphics[trim={1cm 9.5cm 16cm 3cm},scale=0.8]{Chap2_MAD/figures/valmod_screenShot_1.pdf}
	\caption{GUI interface of the prototype, which implements \VALMOD. }
	\label{fig:screenShot}
	\vspace*{-0.3cm}
\end{figure}


\noindent{\bf Traditional Motif discovery VS VALMOD.} Once the user imports a dataset, she can opt to find motifs without having any knowledge of their lengths, just by inspecting the data themselves.
Thereafter, the user can experience the VALMOD support in finding motif pairs that can be of variable length, understanding the quantity and quality of the insights that are not achievable with a simple raw data visual analysis.
She can thus run our algorithm selecting the desired motif length range at the top of the interface (Figure~\ref{fig:screenShot}). 
Our prototype disposes of two panels, which report the \VALMAP structure and the Length Profile respectively, once the motif discovery terminates, as we depict in Figure~\ref{fig:screenShot2}. Here, we run motif discovery, with range $\ell_{min} = 256$ and $\ell_{min} = 1024$.
We show the first \VALMAP checkpoint, which permits to discover motifs of length $256$; the $Top-1$ motif pair is reported in red.  


\begin{figure}[!tb]
	\centering
	\includegraphics[trim={1cm 9.5cm 16cm 3cm},scale=0.8]{Chap2_MAD/figures/valmod_screenShot_2.pdf}
	\caption{GUI interface of the prototype, \VALMAP and Length profile structure panels. }
	\label{fig:screenShot2}
	\vspace*{-0.3cm}
\end{figure}


\noindent{\bf Need for Variable Length Motifs.} 
\begin{figure}[!tb]
	\centering
	\includegraphics[trim={1cm 9.5cm 16cm 3cm},scale=0.8]{Chap2_MAD/figures/valmod_screenShot_3.pdf}
	\caption{GUI interface of the prototype, mining motifs of variable length.}
	\label{fig:screenShot3}
	\vspace*{-0.3cm}
\end{figure}
We tested our prototype, which implements \VALMOD on different real datasets, including ECG and ASTRO (celestial objects data), GAP (global active power) as well as datasets coming from the domains of \textit{Entomology} and \textit{Seismology}. 
In these cases, but also in general the user can understand the importance of using variable length motif detection (with the support of \textit{VALMAP}), in order to identify patterns of interesting behavior exhibiting themselves as sequences of different lengths.
We can thus use the checkpoints slider, as depicted in Figure~\ref{fig:screenShot3}, to test the presence of motif pairs that are longer than $\ell{min}$.
We note that at length $490$, the length profile contains several updates on different subsequences offset, which have (globally) small distances at the respective offsets in VALMAP (circles in Figure~\ref{fig:screenShot3}).
This suggests the presence of a motif pair of length $490$, which are reported in red.
Beyond the \textit{best} motif pairs (those with absolute smallest distances), the user can also iterate and visualize (in order) the rest of the motifs ranked by \textit{VALMAP}, as we depict in Figure~\ref{fig:screenShot4}.    


\begin{figure}[!tb]
	\centering
	\includegraphics[trim={1cm 10cm 16cm 3.5cm},scale=0.8]{Chap2_MAD/figures/valmod_screenShot_4.pdf}
	\caption{GUI interface of the prototype, $Top-k$ motifs iteration. \textit{(a)} Top-1 motif of length $256$. \textit{(b)} Top-1 motif of length $490$. }
	\label{fig:screenShot4}
	\vspace*{-0.3cm}
\end{figure}

\noindent{\bf VALMOD VS Competitors.} Using our prototype, the user has also the possibility to compare \VALMOD to alternative approaches used for motif discovery. 
Specifically, she can note the \VALMOD time performance improvement in variable length motif discovery.
Beyond time performance, the user can also observe that the competitors do not provide an effective solution to compare motifs of different lengths.
We face this limitation when running the state-of-the-art motif discovery algorithm. 
We depict the result in Figure~\ref{fig:screenShot5}, where the matrix profile is computed for one fixed length, namely $256$ points, leading to discovery of motifs of only this length. 


\begin{figure}[!tb]
	\centering
	\includegraphics[trim={1cm 12cm 16cm 3cm},scale=0.8]{Chap2_MAD/figures/valmod_screenShot_5.pdf}
	\caption{GUI interface of the prototype, state-of-the-art motif mining (Matrix profile). }
	\label{fig:screenShot5}
	\vspace*{-0.3cm}
\end{figure}

\section{Conclusions}

In this work, we present a motif discovery prototype system based on the \textit{VALMOD} algorithm, which efficiently finds data series motif of variable length.
As opposed to the other approaches, our system provides a new meta data-series (\VALMAP), which ranks motif pairs of variable length, using a new length normalized distance. 
Our solution provides enriched insights, which help to detect not only the correct resolution (length) of an interesting event, but also the occurrences of repeated patterns with different meanings, which are typical in numerous domains.




