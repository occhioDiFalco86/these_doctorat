\section{Problem Definition} 
\label{sec:MAD_problem}

We begin by defining the data type of interest, data series:

\begin{definition}[Data series]
A data series $T \in \mathbb{R}^n $ is a sequence of real-valued numbers $t_i\in\mathbb{R}$ $[t_1,t_2,...,t_n]$, where $n$ is the length of $T$.	
\end{definition}

We are typically not interested in the global properties of a data series, but in the local regions known as subsequences:

\begin{definition}[Subsequence]
A subsequence $T_{i,\ell} \in \mathbb{R}^\ell$ of a data series $T$ is a continuous subset of the values from $T$ of length $\ell$ starting from position $i$. Formally, $T_{i,\ell} = [t_i, t_{i+1},...,t_{i+\ell-1}]$.	
\end{definition}


\subsection{Motif Discovery}

In this work, a particular local property we are interested in is data series motifs.
A data series motif pair is the pair of the most similar subsequences of a given length, $\ell$, of a data series:
 
\begin{definition}[Data series motif pair]
$T_{a,\ell}$ and $T_{b,\ell}$ is a motif pair iff $dist(T_{a,\ell},T_{b,\ell})  \le dist(T_{i,\ell},T_{j,\ell})$
$\forall i,j \in[1,2,...,n-\ell+1]$, where $a \neq b$ and $i \neq j$, and dist is a function that computes the z-normalized Euclidean distance between the input subsequences \cite{DBLP:conf/kdd/ChiuKL03,DBLP:conf/sdm/MueenKZCW09,Wordrecognition,Whitney,DBLP:conf/kdd/YankovKMCZ07}.
\end{definition}



Note, that if we remove the motif pair from the dataset, the pair with the second smallest distance will become the new motif pair. 
In this way, we can produce a ranked list of subsequence pairs, which we call motif pairs of length $\ell$. 
%The $i$-th pair in this list, is called the $i$-th motif pair. 

\begin{comment}
We can moreover generalize the definition of data series motif by considering a motifs rank:
\begin{definition}[$k^{th}$-data series motif pair]
A $k^{th}$-data series motif pair of length $m$, is the $k^{th}$ most similar subsequence pair of a data series. Formally, \{$T_{a,m}$,$T_{b,m}$\} is the $k^{th}$-motif pair iff there exist a subsequence pairs set $S$ of size $k-1$, where \{$T_{a,m}$,$T_{x,m}$\}, \{$T_{b,m}$,$T_{x,m}$\}  $\notin S$, for $a \ne b$, $1\le x \le|T|-m+1$ and $\forall$\{$T_{i,m}$,$T_{j,m}$\} $\in S$, \{$T_{w,m}$,$T_{z,m}$\} $\notin S$, dist($T_{i,m}$,$T_{j,m}$)$\le$dist($T_{a,m}$,$T_{b,m}$)$\le$dist($T_{w,m}$,$T_{z,m}$) \cite{DBLP:conf/sdm/MueenKZCW09}.   
\end{definition}
\end{comment}
\begin{comment}
\begin{definition}[Range data series motif]
The range motif of length $m$, is the maximal subsequences set with range $r$. Formally, given a data series $T$, $S$ is a range motif set if $\forall T_{i,m},T_{j,m}\in S$, $dist(T_{i,m},T_{j,m}) \le 2r$ and  $\forall T_{i,m}\in S$, $T_{x,m}\notin S$, such that $1 \le x \le |T|-m+1$, $ dist(T_{i,m},T_{x,m})$ > $2r$ \cite{DBLP:conf/sdm/MueenKZCW09}.   
\end{definition}
\end{comment}

We store the distance between a subsequence of a data series with all the other subsequences from the same data series in an ordered array called a distance profile.

\begin{definition}[Distance profile]
A distance profile $D \in \mathbb{R}^{(n-\ell+1)}$ of a data series $T$ regarding subsequence $T_{i,\ell}$ is a vector that stores $dist(T_{i,\ell},T_{j,\ell})$, $\forall j \in [1,2,...,n-\ell+1]$, where $i \ne j$.
\end{definition}


One of the most efficient ways to locate the exact data series motif is to compute the matrix profile \cite{YehZUBDDSMK16,ZhuZSYFMBK16}, which can be obtained by evaluating the minimum value of every distance profile in the time series.

\begin{definition}[Matrix profile]
A matrix profile $ MP \in \mathbb{R}^{(n-\ell+1)}$ of a data series $T$ is a meta data series that stores the z-normalized Euclidean distance between each subsequence and its nearest neighbor, where $n$ is the length of $T$ and $\ell$ is the given subsequence length. The data series motif can be found by locating the two lowest values in $MP$.
\end{definition}

%Figure \ref{dataSeriesTandMP} illustrates a matrix profile on a small dataset.

%\begin{figure}[h]
% 	\includegraphics[trim={0 6cm 0 4.5cm},scale=0.30]{Chap2_MAD/figures//MP_EX}
% 	\caption{A data series T and its self-join matrix profile P.}
% 	\label{dataSeriesTandMP}	
%\end{figure}

To avoid trivial matches [4], in which a pattern is matched to itself or a pattern that largely overlaps with itself, the matrix profile incorporates an ``exclusion-zone'' concept, which is a region before and after the location of a given query that should be ignored. The exclusion zone is heuristically set to $\ell$/2.
The recently introduced STOMP algorithm~\cite{ZhuZSYFMBK16} offers a solution to compute the matrix profile $MP$ in $\mathcal{O}(n^2)$ time. This may seem untenable for data series mining, but several factors mitigate this concern. First, note that the time complexity is independent of $\ell$, the length of the subsequences. Secondly, the matrix profile can be computed with an anytime algorithm, and in most domains, in just $\mathcal{O}(nc)$ steps the algorithm converges to what would be the final solution~\cite{YehZUBDDSMK16} ($c$ is a small constant). Finally, the matrix profile can be computed with GPUs, cloud computing, and other HPC environments that make scaling to at least tens of millions of data points trivial~\cite{ZhuZSYFMBK16}.


We can now formally define the problems we solve.

\begin{problem}[Variable-Length Motif Pair Discovery]
\label{def:ValMotif}
Given a data series $T$ and a subsequence length-range $[\ell_{min},...,\ell_{max}]$, we want to find the data series motif pairs of all lengths in $[\ell_{min},...,\ell_{max}]$, occurring in $T$. 
%Formally, $\ell_{max}\ge \ell_{min}$.
\end{problem}

One naive solution to this problem is to repeatedly run the state-of-the art motif discovery algorithms for every length in the range. However, note that the size of this range can be as large as $\mathcal{O}(n)$, which makes the naive solution infeasible for even middle-size data series. We aim at reducing this $\mathcal{O}(n)$ factor to a small value.

Note that the motif pair discovery problem has been extensively studied in the last decade~\cite{YehZUBDDSMK16,ZhuZSYFMBK16,DBLP:journals/kais/MueenC15,DBLP:conf/icde/LiUYG15,DBLP:conf/sdm/MueenKZCW09,DBLP:conf/aciids/MohammadN14,6426960}.
The reason is that if we want to find a collection of recurrent subsequences in $T$, the most computationally expensive operation consists of identifying the motif pairs~\cite{ZhuZSYFMBK16}, namely, solving Problem~\ref{def:ValMotif}. 
Extending motif pairs to sets incurs a negligible additional cost (as we also show in our study). 



Given 
%the $i^{th}$ 
a motif pair $\{T_{\alpha,\ell},T_{\beta,\ell}\}$, the data series motif set $S^{\ell}_{r}$, with radius $r\in \mathbb{R}$, is the set of subsequences of length $\ell$, which are in distance at most $r$ from either $T_{\alpha,\ell}$, or $T_{\beta,\ell}$. 
More formally:

\begin{definition}[Data series motif set]
Let $\{T_{\alpha,\ell},T_{\beta,\ell}\}$ be a motif pair of length $\ell$ of data series $T$. 
The motif set $S^\ell_r$ is defined as: $S^\ell_r= \{T_{i,\ell} | dist(T_{i,\ell} ,T_{\alpha,\ell})$ $< r \lor dist(T_{i,\ell} ,T_{\beta,\ell}) < r \}$.
\end{definition}

The cardinality of $S^\ell_r$, $|S^\ell_r|$, is called the frequency of the motif set.   

Intuitively, we can build a motif set starting from a motif pair.
Then, we iteratively add into the motif set all subsequences within radius $r$. 
We use the above definition to solve the following problem (optionally including a constraint on the minimum frequency for motif sets in the final answer).

\begin{problem}[Variable-Length Motif Sets Discovery]
\label{def:ValMotifSet}
Given a data series $T$ and a length range $[\ell_{min},\ldots,\ell_{max}]$, 
%and $K\in \mathbb{N}$, which is the maximum number of motif sets we want to compute, 
%and a function $f:\mathbb{R} \rightarrow \mathbb{R^+}$, called ranging function, 
%we want to find the set: $S^*= \{S^k_r, 1\le k \le K|
we want to find the set $S^*= \{S^\ell_r |
%r=f(x), \exists \ell'$ such that $\ell_{min}\le\ell'\le\ell_{max}, \forall T_{a,\ell} \in S_r$ such that $\ell'=\ell \}$. 
%(S^k_r$ is a motif set$)\wedge (K \le |T|-\ell_{min}+1 )\}$.
S^\ell_r$ is a motif set, $\ell_{min}\le\ell\le\ell_{max}\}$. 
%(\forall T_{a,\ell} \in S^k_r, \exists \ell', \ell_{min} \le\ell'\le\ell_{max},$ such that $\ell'=\ell) 
%($ something about the topk $) \}$. 
%In addition, we require that if $S^k_r,S^{k'}_{r'}\in S^* \Rightarrow S^k_r \cap S^{k'}_{r'} = \emptyset$.
In addition, we require that if $S^\ell_r,S'^{\ell'}_{r'}\in S^* \Rightarrow S^\ell_r \cap S'^{\ell'}_{r'} = \emptyset$.
%Given the namely the subsequences pairs having the $k$ smallest distances, a radius factor $D$ and a (optional) minimum frequency threshold $F$, we want to find all the motif sets, each one containing one of the best $k$ motif pairs. Formally, $\forall m \in M^k$, with $dist(m)$ their Euclidean distance, $\exists S$, a motif set of range $r=(D \times dist(m))$, such that $ m \in S$. Moreover, given $S,S'$ two motif sets, if $m \in S$, $ m' \in S'$ and $m,m' \in M^k$ $\Rightarrow S \cap S' = \emptyset$. Since we are in a variable length setting, if $m$ and $m'$ have different subsequence length we consider equality based on the offsets. If $F$ is defined, $\exists S \Leftrightarrow 2 \le F \le |S|$.
\end{problem}

Thus, the variable-length motif sets discovery problem results in a set, $S^*$, of 
%the top-$K$ 
motif sets.
%, ranked in increasing distance. 
The constraint at the end of the problem definition restricts each subsequence to be included in at most one motif set.
Note that in practice we may not be interested in all the motif sets, but only in those with the $k$ smallest distances, leading to a $top$-$k$ version of the problem.
In our work, we provide a solution for the $top$-$k$ problem (though, setting $k$ to a very large value will produce all results).

%The Top k-Motif Sets, are the sets with the $k$ smallest ranges. The proximity of subsequences is used here as a quality measure. Moreover the range $r$ is not a straight user define parameters, but it is built considering the natural ranking of the data (distance of the best $k$ pairs) and then adjusted by the user input radius factor $D$. This allows a more fine grained motif discovery, given the fact that an appropriate range $r$ is typically hard to set a priori.  











\subsection{Discord Discovery}


In order to introduce the problem of discord discovery, we first define the notion of \emph{best match}, or \emph{nearest neighbor}.
	
\begin{definition}[$m^{th}$ best match]
%		The subsequence $T_{j,\ell}$ is called the $m^{th}$ (non-trivial) match of $T_{i,\ell}$, if there exist an array of index $A \in \mathbb{N}^{m-1}$, where for each pair $a,a'\in A \implies a \ne a'$, and $1\le a,a'\le |T-\ell+1|$. Moreover if $a\in A \iff dist(T_{i,\ell},T_{a,\ell})< dist(T_{j,\ell},T_{a,\ell})$ and $T_{a,\ell}$ is not a trivial match of $T_{i,\ell}$. For each $b \notin A $, where $1\le b \le |T-\ell+1|$, $dist(T_{i,\ell},T_{b,\ell}) > dist(T_{i,\ell},T_{j,\ell})$ and $T_{b,\ell}$ is not a trivial match of $T_{i,\ell}$. {\bf ??? I cannot follow, we need to discuss this def ???}
Given a subsequence $T_{i,\ell}$, we say that its $m^{th}$ best match, or Nearest Neighbor ($m^{th}$ NN) is $T_{j,\ell}$, if  $T_{j,\ell}$ has the $m^{th}$ shortest distance to $T_{i,\ell}$, among all the subsequences of length $\ell$ in $T$, excluding trivial matches. 
%We implicitly assume that the $m^{th}$ best match of a generic subsequence $T_{i,\ell}$, has always the same length $\ell$. 
\end{definition}

In the distance profile of $T_{i,\ell}$, the $m^{th}$ smallest distance, is the distance of the $m^{th}$ best match of $T_{i,\ell}$.
We are now in the position to formally define the discord primitives, we use in our work.
	 
\begin{definition}[$m^{th} discord$~\cite{Keogh2005}]
	The subsequence $T_{i,\ell}$ is called the $m^{th}$ discord of length $\ell$, if its $m^{th}$ best match is the largest among the best match distances of all subsequences of length $\ell$ in $T$.   
\end{definition}
	
	
Intuitively, discovering the $m^{th} discord$ enables us to find an isolated group of $m$ subsequences, which are far from the rest of the data.  
Furthermore, we can rank the $m^{th} discords$, according to their $m^{th}$ best matches. 
This allows us to define the \topkmd discords.

\begin{definition}[\topkmd discords]
	We call the $k$ subsequences, with the $k$ largest distances to their $m^{th}$ best matches, the \topkmd discords.   
\end{definition}

\begin{figure}[tb]
	\centering
	\includegraphics[trim={0 14.5cm 21cm 3cm},scale=1.1]{Chap2_MAD/figures/Topk_discords}
	\caption{A dataset with 12 subsequences (of the same length $\ell$) depicted as points in 2-dimensional space. We report the \topkmd discords.}
	\label{topkmdiscords}	
\end{figure}

In Figure~\ref{topkmdiscords}, we plot a group of \textit{12} subsequences (represented in a 2-dimensional space), and we depict three \topkmd discords (groups of red/dark circles). 
Remember that $m$ represents the number of anomalous subsequences in a discord group. 
On the other hand, $k$ ranks the discords and implicitly the groups, according to their $m^{th}$ best match distances, in descending order (e.g., $Top-1$ $1^{st}$ discord and $Top-1$ $2^{nd}$).     	
	
Given these definitions, we can formally introduce the following problem: 
\begin{problem}[Variable-Length \topkmd Discord Discovery]
\label{def:Variable-Length Discord discovery}
Given a data series $T$, a subsequence length-range $[\ell_{min},...,\ell_{max}]$ and the parameters $a,b \in \mathbb{N^{+}}$ we want to enumerate the \topkmd discords for each $k \in \{1,..,a\}$ and each $m \in \{1,..,b\}$, and for all lengths in $[\ell_{min},...,\ell_{max}]$, occurring in $T$.
\end{problem}

Observe that solving the Variable-Length \topkmd Discord Discovery problem is relevant to solving the Variable-Length Motif Set Discovery problem: in the former case we are interested in the subsequences with the most distant neighbors, while in the latter case we seek the subsequences with the most close neighbors.
Therefore, the Matrix Profile, which contains all this information, can serve as the basis to solve both problems.
































