\section{Introduction}

{\bf ??? copy the abstract ???}











\section{VALMOD Demo}


\begin{figure*}[tb]
	\centering
	\includegraphics[trim={0cm 11cm 2cm 3.8cm},scale=0.65]{Chap2_MAD/figures/mp_valmap}
	\caption{\textit{Left}) \textit{(a)} Snippet of ECG recording with highlighted motifs of length \textit{50}, \textit{(b)} Matrix profile computed with subsequence length \textit{50}. \textit{(c)} Index profile, reporting the offsets of the best match. \textit{Right}) \textit{(d)} Snippet of ECG recording with highlighted motifs of length \textit{400}, \textit{(e)} VALMAP $MP^n$, \textit{(f)} VALMAP Length profile.}
	\label{mpECG}
	\vspace*{-0.3cm}	
\end{figure*}


\noindent{\bf VALMAP.} While the proposed motif rank weights the subsequences importance according to the ratio distance-length, we want to know also, whether and how the motif pairs changes, helping the user to extract the desired insights at the correct length. 
To that extent, we introduce a new meta-data, called Variable Length Matrix Profile (\VALMAP), maintaining the same logic and structure of the Matrix profile depicted in Figure~\ref{mpECG} (top), with the difference that this new structure carries length normalized distances and it is coupled with a new vector called \textit{Length profile}, which contains the lengths of the subsequences.
More formally, given a data series $D$, and a range of subsequence lengths, whose extremes are denoted by $\ell_{min}$ and $\ell_{max}$, we define \VALMAP as a triple $\langle MP^{n} \in \mathbb{R}^{|D|-\ell_{min} + 1}, IP \in \mathbb{N}^{|D|-\ell_{min} + 1},  LP \in \mathbb{N}^{|D|-\ell_{min} + 1} \rangle$, where $MP^{n}$ is the Matrix profile containing length normalized distances, whereas $IP$ and $LP$ are the relative Index and Length Profile.
If we consider just a fixed length, \VALMAP will coincide with the length normalized version of the Matrix profile, with a flat Length profile.
This is basically the structure that \VALMOD builds, considering subsequences of length $\ell_{min}$.
In the second stage, we can update \VALMAP using the top-$k$ motif pairs, computed for each length until $\ell_{max}$. 
We thus consider each ($D_{i,\ell_{min}+1} , D_{j,\ell_{min}+1} $) $\in$ top-$k$ motif pairs, where $i,j$ are the subsequences offsets, $\ell_{min}+1$ their lengths and $d^{n}_{i,j}$ their length normalized Euclidean distance. Note that in a motif pair the right subsequence is the one with the absolute shortest distance to the one at the left. 
Hence, \VALMAP, $MP^{n}[i]$ is updated with $d^{n}_{i,j}$ if $d^{n}_{i,j} < MP^{n}[i]$, which was containing the distance between $D_{i,\ell_{min}}$ and its best match. If this update takes place, the Index and Length profile are respectively assigned with $j$, the offset of the new best match, and $\ell_{min}+1$ the new length.
The update operation takes place for each top-$k$ motif pair of any length between $\ell_{min}$ and $\ell_{max}$.
Once the algorithms ends, \VALMAP contains a picture of the motif pairs showing, at which length the last update takes place. If a motif pair is updated, this implies that a longer pattern represent a better match and thus it might reveal either a new event or the same event lasting longer.


\noindent{\bf Example of VALMAP Expressiveness.}
In order to show the expressiveness of \VALMAP, we ran \VALMOD on the ECG data snippet previously considered, showing the \VALMAP structure in Figure~\ref{mpECG} (right). We use the following input parameter: $\ell_{min}=50$ and $\ell_{max}=400$.  
We note that \VALMAP reports the motif with the shortest length normalized distance of length \text{56}, which is the same partial event detected by the Matrix profile in the fixed length case, at the top of the picture.  
If we look at the Length profile in Figure~\ref{mpECG}.(f), we observe that, at an earlier time than the discovered motifs pair, a sequence of contiguous updates took place, as we reported. The subsequences concerned have distances almost as short as the one of the best motifs in \VALMAP~, thus, remaining longer and possibly valid matches.

In Figure~\ref{mpECG}.(d) we depict and highlight the motif pair of length \textit{400}. Immediately, we can note that, the subsequences in red, which compose this motif, are a better representation of a recurrent heartbeat. In fact, the two typical components (\textit{Artia and Ventricles contract}) are correctly detected.  



\begin{figure}[tb]
	\centering
	\includegraphics[trim={5cm 5.5cm 16cm 4cm},scale=0.45]{Chap2_MAD/figures/arch_flow.pdf}
	\caption{Architecture of VALMOD system. }
	\label{fig:archFlow}
	\vspace*{-0.3cm}
\end{figure}


\section{System Description}

We now describe the architecture of our system, depicted also in Figure~\ref{fig:archFlow}.
The input is represented by a data series of interest.
As a starting point, the user has the possibility to inspect the data and also setting the desired parameter (lengths range [$\ell_{min}$,$\ell_{max}$]).
Afterwards, she can run the \VALMOD algorithm, which is a part of the system back-end we implemented in C. 
Once terminated, \VALMOD outputs the \VALMAP meta-data. This latter is thus sent to the front-end, implemented in Python. 
Here, the user can interact with the system analyzing the showcased elements, such as:
\begin{itemize}
	\item the checkpoints of the VALMAP, namely all the updates occurred from the length $\ell{min}$ till the desired length, selected with a dedicated slider. 
	\item all the top-$k$ motifs of variable length, which \VALMAP reports. 
	\item expand a selected motif pair to the relative Motif Set, containing all the similar subsequences of the pair in the data. 
\end{itemize}
In Figure~\ref{fig:screenShot} we show a screen-shot of the VALMAP analysis in our demonstration.
\begin{figure}[tb]
	\centering
	\includegraphics[trim={0cm 10cm 21cm 3.3cm},scale=1.10]{Chap2_MAD/figures/screenShot.pdf}
	\caption{GUI interface showing the interaction with VALMAP. }
	\label{fig:screenShot}
	\vspace*{-0.3cm}
\end{figure}

\section{Demonstration}

We now present the scenarios of our demonstration.
\noindent{\bf Need for Variable Length Motifs.} 
We will showcase variable length motif discovery using \VALMOD on different real datasets~\cite{VALMOD}, including ECG and ASTRO, as well as 
datasets coming from the domains of \textit{Entomology} and \textit{Seismology}. 
In these two particular cases, the user can understand the importance of using variable length motif detection (with the support of \VALMAP), in order to identify patterns of interesting behavior exhibiting themselves as sequences of different lengths.

\noindent{\bf Traditional Motif discovery VS VALMOD.} In this scenario, we will challenge the user to find the motifs without having any knowledge of their lengths, just by inspecting the data themselves When this takes place, the user can experience the VALMOD support in finding motif pairs that can be of variable length, understanding the quantity and quality of the insights that are not achievable with a simple raw data visual analysis.

\noindent{\bf VALMOD VS Competitors.} In this scenario, the user can compare \VALMOD to alternative approaches used for motif discovery. 
Specifically the audience will note the performance improvement, concerning fixed and variable length motif discovery, and the increased expressiveness provided by \VALMAP.


