
\section{Discord Discovery}
\label{sec:approachDiscord}
We now describe our approach to solving the Variable-Length \topkmd Discord Discovery problem. 
First, we explain some useful notions, and we then present our discord discovery algorithm.











\subsection{Comparing Discords of Different Lengths}

Before introducing the algorithm that identifies discords (from the $Top$-$1$ $1^{st}$ to the \topkmd one), we define the data structure that allows us to accommodate them. 
We can represent this structure as a $k \times m$ matrix, which contains the best match distance and the offset of each discord. 

More formally, given a data series $T$, and a subsequence length $\ell$ we define:
$dkm_{\ell} = \begin{bmatrix}
\langle d,o \rangle_{1,1} & .. & \langle d,o \rangle_{1,m} \\
.. & .. & ..\\
\langle d,o \rangle_{k,1} & .. &\langle d,o \rangle_{k,m}
\end{bmatrix}$, where a generic pair $\langle d,o \rangle_{i,j}$ contains the offset $o$ and the corresponding distance $d$ of the $Top$-$i$ $j^{th}$ discord of length $\ell$ ($1 \le i \le k$ and $ 1 \le j \le m$).
In $dkm_{\ell}$, rows rank the discords according to their positions ($m^{th}$ discords), and the columns according to their best match distance ($Top$-$k$).
For each pair $\langle d,o \rangle_{a,b}$,  $\langle d',o' \rangle_{a',b'} \in dkm_{\ell}$, we require that $T_{o,\ell}$ and $T_{o',\ell}$ are not trivial matches. 
	
Since we want to compute $dkm_{\ell}$ for each length in the range $[\ell_{min},\ell_{max}]$, we also need to rank discords of different lengths. In that regard, we want to obtain a unique matrix that we denote by $dkm_{\ell_{min},\ell_{max}}$.
Therefore, we can represent a discord  by the triple $\langle d^*,o^*,\ell^* \rangle_{i,j}$ $\in$ $dkm_{\ell_{min},\ell_{max}}$, where $d^*$ is the $i^{th}$ greatest length normalized $j^{th}$ best match distance. More formally: $d^* = max\{\frac{d}{\sqrt{\ell{min}}} : d \in dkm_{\ell_{min}}(i,j),..., \frac{d}{\sqrt{\ell{max}}} : d \in dkm_{\ell_{max}}(i,j) \}$. Each triple is also composed by the offset $o^*$ and the length $\ell^{*}$ of the discord, where $\ell_{min}\le \ell^{*}\le \ell_{max}$.   
	
By multiplying by the $1/\sqrt{\ell}$ ratio each distance, we want to favor the selection of shorter discords. This strategy is based on the following fact: if we compare two \topkmd discord subsequences of different lengths, but equal best match distances, the shorter subsequence is the one with the highest point-to-point dissimilarity to its best match.
Consequently, we promote the shorter subsequence as the more anomalous one.




\subsection{Discord Discovery Algorithm}


We now describe our algorithm for the \topkmd discords discovery problem.
We note that we can still use the lower bound distance measure, as in the motif discovery case.
This allows us to efficiently build $dkm_{\ell}$, for each $\ell$ in the $[\ell_{min},\ell_{max}]$ range, incrementally reusing the distances computation performed. The final outcome of this procedure is the  $dkm_{\ell_{min},\ell_{max}}$ matrix, which contains the variable length discord ranking.
In this part, we introduce and explain the algorithms, which permit us to efficiently obtain $dkm_{\ell}$ for each length.
We report the whole procedure in Algorithm~\ref{computeDkmAlllength}.
	
	
	\begin{algorithm}[!tb]
		{\small
		\KwIn{\textbf{DataSeries} $T$, \textbf{int} $\ell_{min}$, \textbf{int} $\ell_{max}$, \textbf{int} $k$, \textbf{int} $m$ , \textbf{int} $p$}
		\KwOut{\textbf{Matrix} $dkm_{\ell_{min},\ell_{max}}$}	
		\textbf{MaxHeap[]} $listDP$=$ComputeMatrixProfile(T,\ell_{min},p)$\; \label{computeMatrixProfileLmin}
		\textbf{int} $nDp$ = $(|T|-\ell_{min})+1$\;
		\textbf{Matrix} $dkm_{\ell_{min},\ell_{max}}=\{ \{\langle -\infty,-\infty,-\infty \rangle,...,\langle -\infty,-\infty ,-\infty \rangle \},..., \{...\} \}$\;
		\textbf{Matrix} $dkm_{\ell_{min}}=\{ \{\langle -\infty,-\infty \rangle,...,\langle -\infty,-\infty \rangle \},..., \{...\} \}$\;
		\If{$p>=m$}
		{
		\tcp{iterate the partial distance profiles in listDP\\and compute $dkm_{\ell_{min}}$}
		\For{$i$ $\leftarrow$ $1$ \emph{\KwTo} $nDp$ } 
		{\label{iteratePartialDistanceProfile}
			\uIf {$T_{i,\ell_{min}}$ has no Trivial matches in $dkm_{\ell_{min}}$}
			{
				$UpdateFixedLengthDiscords$($dkm_{\ell_{min}}$, $listDP[i]$,$i$,$k$,$m$ )\;\label{updateRankingSingleLength}
			}	
		}
		$UpdateVariableLengthDiscords$($dkm_{\ell_{min}}$, $dkm_{\ell_{min},\ell_{max}}$,$k$,$m$ )\; \label{updateVariableLengthDiscords}
		\tcp{compute $dkm_{\ell_{nextL}}$ for each length, pruning distance computations}
		\For{$nextL$ $\leftarrow$ $\ell_{min}+1$ \emph{\KwTo} $\ell_{max}$ } 
		{\label{iteratenexLengths}
			\textbf{ \textbf{Matrix} $dkm_{nextL}=\{ \{\langle -\infty,-\infty \rangle,...,\langle -\infty,-\infty \rangle \},..., \{...\}\}$\;}
			$nDp$ = $(|T|-nextL)+1$\;
			$dkm_{\ell_{nextL}}$=$Topkm$\_$nextLength$( $T$,$nDp$,$listDP$,$nextL$,$k$,$m$)\; \label{nextLengthDiscord}
			$UpdateVariableLengthDiscords$($dkm_{\ell_{nextL}}$, $dkm_{\ell_{min},\ell_{max}}$,$k$,$m$)\;
		}
		}	
}	 
		\caption{$Topkm$\_$DiscordDiscovery$ (\textit{Compute \topkmd Discords of variable lengths}) \label{computeDkmAlllength}}
	\end{algorithm}	
	
	\noindent{\bf Smallest Length Discords.} We start to find discords of length $\ell_{min}$, namely the smallest subsequence length in the range. We can thus run Algorithm~\ref{computeMatrix} in line~\ref{computeMatrixProfileLmin}, which computes the list of partial distance profiles of each subsequence of length $\ell_{min}$ ($listDP$), in the input data series $T$. 
	Each partial distance profile contains the $p$ smallest nearest neighbor distances of each subsequence. To that extent, we set $p \ge m$ in Algorithm~\ref{computeMatrix} ($ComputeMatrixProfile$).
	
	We then iterate the subsequences of $T$ in line~\ref{iteratePartialDistanceProfile}, using the index $i$.
	For each subsequence $T_{i,\ell{min}}$ that has no trivial matches in $dkm_{\ell_{min}}$, we invoke the routine $UpdateFixedLengthDiscords$ (line~\ref{updateRankingSingleLength}), which checks if $T_{i,\ell{min}}$ can be placed in $dkm_{\ell_{min}}$ as a discord.
	When $dkm_{\ell_{min}}$ is built, we update the variable length discords ranking ($dkm_{\ell_{min},\ell_{max}}$ matrix in line~\ref{updateVariableLengthDiscords}), using the procedure $UpdateVariableLengthDiscords$.
	
	In the loop of line~\ref{iteratenexLengths}, we iterate the discord lengths greater than $\ell_{min}$. Since we want to prune the search space, we consider the list of distance profiles in $listDP$, which also contains the lower bound distances of the $p$ ($p>m$) nearest neighbors of each subsequence. In that regard, we invoke the routine $Topkm$\_$nextLength$ (line~\ref{nextLengthDiscord}).
	Before we introduce the details, we describe the two routines we introduced, which allow to rank the discords. 
	
	
	\noindent{\bf Ranking Fixed Length Discords.} In algorithm~\ref{algoUpdateKMDiscords}, we report the pseudo-code of the routine $UpdateFixedLengthDiscords$.
	This algorithm accepts as input the matrix $dkm_{\ell}$ to update, and a partial distance profile of the subsequence with offset $off$.
	It starts iterating the rows of $dkm_{\ell_{min}}$ in reverse order (line~\ref{iterate_rows}). 
	This is equivalent to considering the discords from the $m^{th}$ one to the $1^{st}$. 
	Hence, at each iteration we get the $j^{th}$ nearest neighbor of $T_{off,\ell_{min}}$ from its partial distance profile in line~\ref{getJth}.
	Subsequently, the loop in line~\ref{iterate_column} checks if the $j^{th}dist$ is among the $k$ largest ones in the $j^{th}$ column of $dkm_{\ell_{min}}$. If it is true, the smallest elements in the column are shifted (line~\ref{shift_ranking}) and $T_{off,\ell_{min}}$ is inserted as the $Top$-$i$ $j^{th}$ discord (line~\ref{udpateRanking}).
	
	
	
	\begin{algorithm}[!tb]
		{\small
		\KwIn{\textbf{Matrix} $dkm_{\ell}$, \textbf{MaxHeap} $minMDist$, \textbf{int} $off$, \textbf{int} $k$, \textbf{int} $m$}
		\For{$j$ $\leftarrow$ $m$ \emph{\textbf{down to}} $1$}
		{\label{iterate_rows}
			\textbf{double} $j^{th}dist$ $\leftarrow$ $minMDist.getMax(j)$\;  \label{getJth}
			\For{$i$ $\leftarrow$ $1$ \emph{\textbf{\KwTo}} $k$}
			{\label{iterate_column}
				$<d,o>_{i,j}=dkm_{newL}[i][j]$\;
				\uIf{ $j^{th}dist > d$} 
				{\label{updateDiscord}
					$shiftRankingTopK$($dkm_{newL}[i][j]$)\label{shift_ranking}\;
					\tcp{update the ranking with the new $Top$-$i$ $j^{th}$ discord $T_{off,\ell}$}
					$dkm_{\ell}[i][j]$ $\leftarrow$ $\langle j^{th}dist,off\rangle$\label{udpateRanking}\;
					\textbf{return};\
				}
			}
		}
			}	
		\caption{$UpdateFixedLengthDiscords$ (\textit{Update $dkm_{\ell}$)} \label{algoUpdateKMDiscords}}
		
	\end{algorithm}
	
	\noindent{\bf Ranking Variable Length Discords.} Once we dispose of the matrix $dkm_{\ell}$, we can invoke the procedure $UpdateVariableLengthDiscords$ for each length $\ell \in \{\ell_{min},...,\ell_{max}\}$ (Algorithm~\ref{algoUpdateVLKMDiscords}), in order to incrementally produce the final variable length discord ranking we store in $dkm_{\ell_{min},\ell_{max}}$.
	This algorithm accepts as input and iterates over the matrix $dkm_{\ell_{min},\ell_{max}}$. A position (discord) is updated if the length normalized best match distance of the discord in the same position of $dkm_{\ell}$ is larger (line~\ref{updatePosition}).
	
	\begin{algorithm}[!tb]
		{\small
		\KwIn{\textbf{Matrix} $dkm_{\ell_{min},\ell_{max}}$, \textbf{Matrix} $dkm_{\ell}$, \textbf{int} $k$, \textbf{int} $m$}
		
		
		\For{$i$ $\leftarrow$ $1$ \emph{\textbf{\KwTo}} $k$}
		{\label{iterate_columnVarLength}
			\For{$j$ $\leftarrow$ $1$ \emph{\textbf{\KwTo}} $m$}
			{\label{iterate_rowsVarLength}
				$<d,o>_{i,j}=dkm_{newL}[i][j]$\;
				$<d^*,o^*,l^*>_{i,j}=dkm_{\ell_{min},\ell_{max}}[i][j]$\;
				\tcp{if length normalized distance is greater or equal for length $\ell$, update the rank. }
				\uIf{($(d  / \sqrt{\ell})$ $>=$ $d^*$ )}
				{
					$dkm_{\ell_{min},\ell_{max}}[i][j] =$ $\langle (d  / \sqrt{\ell}), o, \ell \rangle$ \label{updatePosition}
				}	
			}
		}
	}
		
		\caption{$UpdateVariableLengthDiscords$ (\textit{Update $dkm_{\ell_{min},\ell_{max}}$)} \label{algoUpdateVLKMDiscords}}
	\end{algorithm}
	
	
	
	%	Subsequently, we aim at pruning the search space for the discords of length $newL>\ell_{min}$. Hence, we consider the partial distance profiles stored in the trailing part of Algorithm~\ref{computeMatrix}. Observe that the number of elements stored in the partial distance profile must be at least $m$, since to enumerate \topkmd discords we need to know at least the $m$ largest nearest neighbors for each subsequence.
	%	
	%	Algorithm~\ref{algoUpdateKMDiscords} shows the pseudo-code of the routine, which takes as input a partial distance profile $minMDist$ that contains the distances to the $m^{th}$ nearest neighbor of a generic subsequence $T_{off,newL}$, and updates the $dkm_{newL}$ matrix.
	
	
	
	%As a first step, this routine checks if $T_{i,\ell}$, namely the potential discord, has a trivial match in $dkm_{\ell}$ (line~\ref{checkTMUpdate}). If this is verified, the update does not take place. Otherwise 
	%	
	%	The procedure starts to iterate $dkm_{\ell_{min}}$ from the $m^{th}$ column in line~\ref{iterate_rows}, since the algorithm starts in turn to pop the discords with the largest $m^{th}$ distances from the partial distance profile (line~\ref{updatePopMth}). When iterating a matrix column (loop of line~\ref{iterate_column}), if a distance is larger than an element of the column (line~\ref{updateDiscord}), the other discords in the column with smaller distances are shifted in the lower positions (line~\ref{shift_ranking}), and the update takes place (line~\ref{udpateRanking}). In this manner, the \topkmd ranking for each column is preserved. 
	%	
	
	
	\noindent{\bf Greater Length Discords.} In Algorithm~\ref{computeKMDiscordAlgo}, we show the pseudo-code of the routine $Topkm$\_$nextLength$. It starts performing the same loop of line~\ref{ComputeSubMP} in Algorithm~\ref{algo1}, iterating over the partial distance profiles (line~\ref{loopDPDiscord}), and updating the true Euclidean distances for the new length ($newL$) and the lower bounds (line~\ref{constantDistLB_discord}) for the subsequent length ($newL+1$). 
	Since we need to know the distances from each subsequence to their $m$ nearest neighbors, for each subsequence $T_{i,newL}$ that does not have trivial matches in $dkm_{newL}$, we check if the $m^{th}$ smallest distance is smaller than the maximum lower bound in the partial distance profile (line~\ref{checkMExact}). If this is true, we have the guarantee that the partial distance profile $minMDist$ contains the exact $m$ nearest neighbor Euclidean distances.
	Hence, in line~\ref{updateRankingDiscord}, we can update the matrix $dkm_{newL}$.
	On the other hand, if the distances are not verified to be correct, we keep $minMDist$ in memory, which becomes a non-valid partial distance profile, along with the offset of the corresponding subsequence (line~\ref{nonValidminMDist}).
	Once we have considered all the partial distance profiles, we need to iterate the non-valid partial distance profiles (line~\ref{checknonValidminMDist}).
	
	We therefore recompute those that contain at least one true Euclidean distance greater than the distances in the last row of $dkm_{newL}$. The correctness of this choice is guaranteed by the fact that the distances of a non-valid partial distance profile can be only larger than the non-computed ones.
	Hence, if the condition of line~\ref{checkOnlyGreater} is not verified, no updates in $dkm_{newL}$ can take place.  
	Otherwise, we recompute the non-valid distance profile starting at line~\ref{MASSscratch_discords} from scratch. Note that when we re-compute a distance profile, we globally update the corresponding position of the partial distance profiles $listDP$ (line~\ref{updatelistDP}) and $dkm_{newL}$ in the vector as well (line~\ref{updateDiscFormScratch}). 
	
	
	
	
	
	\begin{algorithm}[!tb]
		{\footnotesize
			{\KwIn{\textbf{DataSeries} $T$, \textbf{int} $nDp$, \textbf{MaxHeap[]} $listDP$, \textbf{int} $newL$, \textbf{int} $k$, \textbf{int} $m$, \textbf{int} $p$}
				\KwOut{\textbf{Matrix} $dkm_{newL}$}	
				\textbf{Matrix} $dkm_{newL}=\{ \{\langle -\infty,-\infty \rangle,...,\langle -\infty,-\infty \rangle \},..., \{...\} \}$\;
				\textbf{List $\langle$\textbf{MaxHeap,int}$\rangle$ } $nonValidMindistList$\; 
				\tcp{iterate over the partial distance profiles in listDP}
				\For{$i$ $\leftarrow$ $1$ \emph{\KwTo} $nDp$ } 
				{\label{loopDPDiscord}
					\textbf{MaxHeap} $minMDist$ $\leftarrow$ \textbf{new MaxHeap}($p$)\;
					\textbf{double} $minDist$ $\leftarrow$ $\inf$\;
					\textbf{int} $ind$ $\leftarrow$ $0$\;
					\textbf{double} $maxLB$ $\leftarrow$ \textbf{popMax}($listDP[i]$)\;
					\tcc{update the partial distance profile for the length newL (true Euclidean and lower bounding distance )}
					\For{\textbf{each entry} $e$ $in$ $listDP[i]$}
					{\label{loopsingleDP_discord}
						$e.dist$, $e.LB$ $\leftarrow$ $updateDistAndLB(e,newL)$\; \label{constantDistLB_discord}
						%$minDist$  $\leftarrow$ \textbf{min}($minDist$,$e.dist$))\;
						\tcp{the m shortest neighbor distances are stored in minMDist}
						$minMDist$.push($e.dist$)\;
						%\If{$minDist == e.dist$}{$ind=e.offset$\;}
					}
					\tcp{check if the $m^{th}$ shortest distance of this partial distance profile is the true $m^{th}$ shorthest.}
					$mDist$ = $minMDist.getMax(m)$\;
					\uIf {$T_{i,newL}$ has no Trivial matches in $dkm_{newL}$}
					{
						\uIf{$mDist < maxLB$} 
						{\label{checkMExact}
							\tcc{the discord ranking can be updated, without computing the whole distance profile }
							$UpdateFixedLengthDiscords$($dkm_{newL},minMDist$,$i$,$k$,$m$)\label{updateRankingDiscord}\;
						}
						\Else 
						{
							\label{nonvalid_discords}
							\tcc{minMDist might not be exact, store the partial distance profile in memory. }
							$nonValidMindistList$.add($<minMDist$,$i>$)\; \label{nonValidminMDist}
						}
					}
				}
				\For{\textbf{each} $<minMDist$, $i>$ \textbf{in} $nonValidMindistList$}	
				{\label{checknonValidminMDist}
					\uIf {$T_{i,\ell}$ has no Trivial matches in $dkm_{\ell}$}
					{
						\For{$j$ $\leftarrow$ $m$ \emph{\textbf{down to}} $1$}
						{
							$mDist$ = $minMDist.getMax(j)$\;
							\uIf{$mDist > dkm_{newL}[k][j].d$ }
							{\label{checkOnlyGreater}
								$QT$ $\leftarrow$ $SlidingDotProduct$($T_{i,newL}$, $T$)\; \label{MASSscratch_discords}
								\textbf{double} $s$ $\leftarrow$ $sum$($T_{ind,\ell}$);
								\textbf{double} $ss$ $\leftarrow$ $squaredSum$($T_{i,newL}$)\; 
								$D(T_{ind,\ell}) \leftarrow CalcDistProfAndLB$($QT$,$T_{i,newL}$, $T$, $s$, $ss$)\;
								$UpdatePartialDistanceProfile(listDP[i],D(T_{ind,\ell})$\label{updatelistDP}) \;
								$UpdateFixedLengthDiscords$($dkm_{newL},listDP[i]$,$i$,$k$,$m$)\;\label{updateDiscFormScratch}
								\textbf{break}\;
							} 
						}
					}
				}
			}
		} % small font size
		\caption{ $Topkm$\_$nextLength$ (Compute \topkmd Discords of greater lengths)\label{computeKMDiscordAlgo}}
	\end{algorithm}	
	
	
	
	



\noindent{\bf Complexity Analysis.} The time complexity of Algorithm~\ref{computeDkmAlllength} ($Topkm$\_$DiscordDiscovery$) mainly depends on the use of $ComputeMatrixProfile$ algorithm, which always takes $\mathcal{O}(n^2\log(p))$ to compute the partial distance profiles for the $n$ subsequences of length $\ell_{min}$ in $T$.

In order to compute the exact \topkmd discord ranking in $dkm_{\ell}$, the routine $UpdateFixedLengthDiscords$ takes $\mathcal{O}(km)$ time in the worst case. Recall that this latter algorithm is called only for subsequences that do not have trivial matches in $dkm_{\ell}$.
Checking if two subsequences are trivial matches takes constant time, if for each $dkm_{\ell}$ update, we store the $\ell$ trivial match positions.
Given a series $T$, and the discord (subsequence) length $\ell$, we can represent by $S = \frac{|T|}{l/2}$ , the number of subsequences that are not trivial matches with one another. 
Therefore, updating the discord rank of each length has a worst case time complexity of $\mathcal{O}((\ell_{max} - \ell_{min}) \times S \times \ell \times k \times m \times \log(m))$, where the $\log(m)$ factor represents the time to get the $m^{th}$ largest distance in the partial distance profile (line~\ref{getJth} of Algorithm~\ref{algoUpdateKMDiscords}). 
%In all the common settings of discord discovery, the overall complexity is linear in terms of $|T|$. {\bf what exactly is the last sentence? which are the common settings? do we need this sentence? ???}
Similarly, the construction of the variable length discord ranking in $dkm_{\ell_{min},\ell_{max}}$ takes: $\mathcal{O}((\ell_{max} - \ell_{min}) \times k \times m)$.

Observe also that the time performance of the $Topkm$\_$nextLength$ algorithm depends on the Euclidean distance computations pruning. If all the partial distance profiles contain the correct nearest neighbor's distances, computing the discords of each length greater than $\ell_{min}$ takes $\mathcal{O}(n \times p \times \log(m))$ time, with $n$ equal to the number of subsequences in $T$. 
The worst case takes place when for each subsequence that can update $dkm_{\ell}$ (i.e., $S$), the complete distance profile is re-computed (Algorithm~\ref{computeKMDiscordAlgo}, line~\ref{MASSscratch_discords}); in this case the algorithm takes $\mathcal{O}(n^2 \times \log(n) \times p \times \log(m))$.
In our experimental evaluation, we show that in all the cases we tested, the percentage of the recomputed distance profiles is indeed very low.        
               
%\\
%	%\tcp{if SubMP does not contain the motif distance (bBestM = false), compute the whole non valid distance profiles, if it is faster then computeMatrixProfile (nDp / 2 = true)}
%%\If{$!bBestM$ \textbf{and} $nonValidDP.size() < (nDp / p) $}


