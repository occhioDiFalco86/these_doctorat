
\subsection{Discord Discovery }
{
In this last part, we conduct the experimental evaluation concerning discord discovery. 
In the following experiments, we use the same datasets as before.

We identify two state-of-the-art competitors to compare to our approach, the Motif And Discord (MAD) framework.
The first one, DAD (Disk Aware Discord Discovery)~\cite{DBLP:conf/icdm/YankovKR07}, implements an algorithm suitable for enumerating the \emph{fixed-length} $m^{th}$ discords of a data series collection stored on a disk.
We adapted this algorithm, as suggested by the authors, in order to extract discords from data series loaded in main memory. 
The second approach, GrammarViz~\cite{DBLP:conf/edbt/Senin0WOGBCF15}, is the most recent technique, which discovers \topkfirstd discords. It operates by means of grammar rules compression, which further operate on a summarized data series representation, in order to find the rare segments of the data (discords) in a reduced search space.
To the best of our knowledge, there exist no techniques capable of finding the \topkmd ranked variable-length discords as MAD, using a single execution of an algorithm.

\noindent{\bf $\mathbf{M^{th}}$ Discord Discovery.} 
In Figures~\ref{DAD_MAD}(a)-(b), we present the performance comparison between MAD and DAD for finding the $m^{th}$ discords, when we vary $m$, for all datasets.
(All other parameters are set to their default values, as listed in Table~\ref{tableParameters}.)
 
Since DAD discovers fixed-length $m^{th}$ discords, we report its execution time \emph{only} for the first length in the range, namely $\ell_{min}$.
We observe that MAD, which enumerates the $m^{th}$ discords of \textit{100} lengths ($\ell_{min}=1024$, $\ell_{max}=1124$) is still one order of magnitude faster than these DAD performance numbers, for all datasets, when $m$ is larger or equal to \textit{5}.
Moreover, the performance trend of MAD remains stable over all datasets, whereas DAD has different execution times.
%, which are clearly data dependent, as we note in the EMG and ASTRO dataset.
We observe that the computational time of DAD depends on the subsequence length, since it computes Euclidean distances in their entirety (only applying early abandoning based on the best so far distance). 
How effective this early abandoning mechanism is, depends on the characteristics of the data.
On the other hand, our algorithm computes all distances for the first subsequence length in constant time, and then prunes entire distance computations for the larger lengths. 
       
\begin{figure}[tb]
	\centering
	\includegraphics[trim={1cm 9cm 5cm 0cm},scale=0.70]{Chap2_MAD/figures/plots/DAD_MAD}
	\caption{\textit{(a),(b)} DAD (one length) and MAD (\textit{100} lengths) \topkmd discords discovery time. (c) Percentage of \textit{non-valid} partial distance profiles recomputed.}
	\label{DAD_MAD}
\end{figure}
   

In Figure~\ref{DAD_MAD}(c), we report the percentage of non-valid distance profiles that are recomputed, over the total number of distance profiles considered during the entire task of variable-length discord discovery.
We note that the number of re-computations is limited to no more than \textit{0.10\%}, in the worst case. 
This demonstrates the high computation pruning rate achieved by our algorithm, justifying the considerable speed-up achieved.     










\noindent{\bf \topkfirstd Discord Discovery.} 
In Figure~\ref{Gviz_MAD}, we depict the performance comparison between GrammarViz and MAD. 
Therefore, we consider \topkfirstd discords discovery, as previously introduced.
(We maintain the same parameters setting in this experiment.)
      
\begin{figure}[tb]
	\centering
	\includegraphics[trim={1cm 10cm 6cm 0cm},scale=0.70]{Chap2_MAD/figures/plots/Gviz_MAD}
	\caption{\textit{(a),(b)} GrammarViz and MAD (\textit{100} lengths) \topkfirstd discords discovery time. \textit{(c)} Percentage of \textit{non-valid} partial distance profiles recomputed.}
	\label{Gviz_MAD}
\end{figure}

First, we note that GrammarViz outperforms MAD in the first three datasets, for $k$ smaller or equal to \textit{5}, as depicted in Figure~\ref{Gviz_MAD}(a). 
Nevertheless, the experiment shows that MAD scales better over the number of discovered \topkfirstd discords, as its execution time increases only by a small constant factor.
A different trend is observed for GrammarViz, whose performance significantly deteriorates as $k$ increases from 1 to 6.

Moreover, this technique is highly sensitive to the dataset characteristics, as we observe in Figure~\ref{Gviz_MAD}(b), where the two noisy datasets, i.e., EMG and ASTRO, are considered.
This is a direct consequence of the data summarization sensitivity to the data characteristics, which then influences the ability to prune distance computations. 

In Figure~\ref{Gviz_MAD}(c), we report the percentage of non-valid distance profiles that MAD needed to recompute. 
In this case, too, this percentage is very low. 

To conclude, since GrammarViz is a variable length approach that selects the most promising discord lengths according to the distribution of the data summarization (by picking the lengths of the series, whose discrete versions represent a rare occurrence), we report in Figure~\ref{Gviz_MAD}(d) the number of lengths, for which discords are found. 
%{\bf ??? which fig? ???}
%{\bf ??? explain better why this is important, what these results mean for the algos ???}
We observe that our framework always enumerates and ranks discords of all lengths in the specified input range, based on the exact Euclidean distances of the subsequences. 
%This always allows to perform a complete exploratory analysis over the discords length. 
On the other hand, GrammarViz selects the most promising length based on the discrete version of the data, and only identifies the exact \topkfirstd discords for \textit{3} (out of \textit{100}) different lengths in the best case.
%{\bf ??? MAD is exact, and grammarviz is approximate, right? we never mention this here! ???}

\noindent{\bf \topkmd Discord Discovery.} 
Figure~\ref{MADtkm} depicts the execution time for the \topkmd discord discovery task, and the percentage of recomputed distance profiles for MAD, when varying $k$ and $m$. 
We observe that the pruning power remains high: the percentage of distance profile re-computations averages around $0.05$\%.   


\begin{figure}[tb]
	\centering
	\includegraphics[trim={2cm 16cm 6cm 0cm},scale=0.70]{Chap2_MAD/figures/plots/topkm}
	\caption{\textit{(a)} MAD (\textit{100} lengths)\topkmd discords discovery time on the five datasets. (b) Percentage of \textit{non-valid} partial distance profiles recomputed.}
	\label{MADtkm}
\end{figure}





\noindent{\bf Utility of Variable-Length Discord Discovery.}
%\commentRed{?? message to convey ... if we use a range we can find the anomaly as discord... otherwise, with fixed length we would miss it.}
We applied MAD on a real use case, a data series containing the average number of taxi passengers for each half hour over 75 days at the end of 2014 in New York City~\cite{DBLP:journals/pvldb/RongB17}, depicted in Figure~\ref{taxiDiscords}(a). 
We know that this dataset contains an anomaly that occurred during the daylight savings time end, which took place the $2^{nd}$ of November 2014 at $2$am. At that time, the clock was set back at $1$am.    
Since the recording was not adjusted, two samples (corresponding to a \textit{1} hour recording) are summed up with the two subsequent ones. 
%This error, is directly detectable as the highest peak occurring in the data that we depict in Figure~\ref{taxiDiscords}(a). 

\begin{figure}[tb]
	\centering
	\includegraphics[trim={2cm 12.5cm 10cm 3cm},scale=0.75]{Chap2_MAD/figures/taxiDiscords}
	\caption{\textit{(a)} Number of taxi passengers over 75 days in New York City. \textit{(b)} $Top-1$ $1^{st}$ discord of length \textit{32}. \textit{(c)} $Top-1$ $1^{st}$ discord of length \textit{33}.}
	\label{taxiDiscords}
\end{figure}


We ran the variable-length discord discovery task using the length range $\ell_{min}=20$ and $\ell_{max}=48$, in order to cover subsequences that correspond to recordings between $10- $ and $ 24$ hours. 
Our algorithm correctly identifies the anomaly for subsequence length \emph{32}, shown in Figure~\ref{taxiDiscords}(b). 
Changing the window size does not allow the detection of the anomaly. 
For example, enlarging the window by \textit{just} 1 point, the \topkfirstd discord corresponds to a pattern \emph{before} the abnormality (refer to Figure~\ref{taxiDiscords}(c)).

These results showcase the importance of efficient variable-length discord discovery. 
It permits us to discover rare, or abnormal events with different durations, which can be easily missed in the fixed length discord discovery setting, where the analyst is constrained to examine a single length (or time permitting, a few fixed lengths).
  
\subsection{Exploratory Analysis: Motif and Discord Length Selection}

In this part, we present the results of an experiment we conducted to test the capability of MAD to suggest the most promising length/s for motifs and discords.
%As presented throughout the paper, this is a challenge that firstly involves scalability issues, and also the problem with interpreting the obtained results.

Given a data series, the user may have no clear idea about the motif/discord length. % or even a possible range of lengths. 
Therefore, we present use cases that examine the ability of MAD to perform a wide length-range search, providing the most promising results at the \emph{correct} length.

We used MAD for finding motifs and discords in the length range: $\ell_{min} = 256$ and $\ell_{max} = 4096$. We conducted this experiment in the first $500K$ points of the datasets listed in Table~\ref{datainfo}.
The considered motif/discord length range covers the user studies that have been presented so far in the literature (where knowledge of the exact length was always assumed). 
%We furthermore noted that going beyond the maximum length bound ($4096$), the motif subsequences tend to have very low similarity.
%In general, for a complex time series (i.e., lots of peaks and valleys) the curse of dimensionality ensures that long motifs are not very meaningful. That is to say, as the subsequence length gets longer, the motif distance approaches the discord distance.   

\noindent{\bf Scalability.} The MAD framework completed the motif/discord discovery task within \textit{2 days} (on average), enumerating the motifs and the $Top-1$ discords of each length in the given range. 
Concerning the competitors, we estimated that STOMP, which is the state-of-the-art solution for fixed length motif/discord discovery would take $320$ days for the same experiment (a little bit more than two hours for each of the lengths we tested).
QUICK MOTIF, which has data dependent time performance, takes up to more than \textit{6} days (projection) for all datasets but ECG (which completes in \textit{38} hours).
We note that the variable-length motif discovery competitor (MOEN) never terminates before \textit{24} hours when searching motifs of $600$ different lengths, while in this experiment, the length range is composed of $3841$ different lengths.      
Considering discord discovery, we observed that GrammarViz does not enumerate all the discords in the given length-range, since it selects the length according to the data summarizations. 
Thus, we are obliged to run this technique independently for each length, which would take at least $320$ hours in the best case (projection based on results of Figure~\ref{Gviz_MAD}).



\begin{figure}[tb]
	\centering
	\includegraphics[trim={0cm 10cm 15cm 0cm},scale=0.75]{Chap2_MAD/figures/EEGMotifDiscovery_256}
	\caption{Top-1 motif (of length $256$) in the EEG data set. The subsequences pairs composing this motif have the smallest distance in both the Euclidean distance and length normalized ranking.}
	\label{EEGMotif_small}
\end{figure}

\begin{figure}[tb]
	\centering
	\includegraphics[trim={0cm 11.5cm 0cm 0cm},scale=0.60]{Chap2_MAD/figures/EEGMotifDiscovery}
	\caption{\textit{(a)} Top-1000 motifs according the length normalized distance (top), and the Euclidean Distance (bottom). \textit{(b)} Motif pair of the largest length ($656$) in the length normalized ranking (top) and motif pair of the largest length ($536$) in the Euclidean distance ranking (red/bottom).}
	\label{EEGMotif}
\end{figure}

\noindent{\bf Select the most promising length in Motif Discovery.} Once the search is completed, the MAD framework enumerates the motifs and discords ranking them in a second step, according to the proposed distance normalization strategy.
In Figure~\ref{EEGMotif_small}, we show the results of motif discovery for the EEG dataset.

%\commentRed{This data collection is the results of periodic EEG activity recorder during NREM sleep.
%Following the annotations of the domain experts, these data are characterized by the presence of cyclic sequences of cerebral activation and deactivation~\cite{CAP:dataset}.}
The objective of this experiment is to evaluate the proposed length-normalized correction strategy. 
In this regard, we compare the motifs sorted by using length-normalization, and by Euclidean distances. 

On the top part of Figure~\ref{EEGMotif_small}, we report the distance/length values of the $Top-1000$ motifs ranked by the length-normalized measure (left), which comprise a subset of the results we store in the VALMP structure (Algorithm~\ref{algo1}).
In the right part of the figure, we report the $Top-1000$ motifs ordered by their Euclidean distances.

We observe that the Top-$1$ motif, i.e., the subsequence pair with the smallest distance (marked by the letter A) is the same in both rankings.
We report this motif in the bottom part of Figure~\ref{EEGMotif_small}, which is composed of two quasi-identical patterns in the EEG data series.  

We now evaluate motifs of larger lengths in the same dataset, which may reveal other interesting and similar patterns at different resolutions (lengths). 
In Figure~\ref{EEGMotif}(a), we report again the distance/length values of the $Top-1000$ motifs ranked by their Euclidean distance, which reveal that the longest motif, marked as B, has length $536$.
We observe that this subsequence pair substantially differs from the $Top-1$ motif of Figure~\ref{EEGMotif_small}. 

Subsequently, in Figure~\ref{EEGMotif}(b), we report the longest motif (marked as C) of length $655$ that we found in the $Top-1000$ motif ranking, based on length-normalized distances.
We note that $6\%$ of the length-normalized motifs are longer than those in the $Top-1000$ of the Euclidean ranking.
The example of motif C, which is a longer version of B, shows that this pattern appears much earlier in the sequence than B.
%{\bf ??? it would be nice if we could explain what the motifs we present are ???}
%Discovering motif C gives us two new pieces of information. 
%First, we can consider it as a longer version of the motif B, since it has a very similar shape, though different absolute values towards the last part of the sequences. 
%Second, we note that the first subsequence of the motif C takes place earlier, thus in a very different data series position ($242,918$) respect to the subsequences of motif B.    
If we considered just the $Top-1000$ motifs ranked by their Euclidean distance, we would have missed this insight (motif C appears in the Euclidean distance ranking only in the $Top-4000$ motifs).

%Clearly, this new information can be found in the Euclidean ranking, but at a later stage.
%In fact, as we see from of Figure~\ref{EEGMotif}(c), motif C is highly penalized in the Euclidean ranking, since we need to consider the $Top-4000$ motifs before to find it.
%This fact is due to the highest number of short motifs ($256$ points), that have lower Euclidean distance than the larger motifs, as depicted in the plot of Figure~\ref{EEGMotif}(c).
%Using the length-normalized distances allows us to consider earlier several more motif occurrences of different lengths.    



\begin{figure}[tb]
	\centering
	\includegraphics[trim={0cm 2cm 0cm 0cm},scale=0.38]{Chap2_MAD/figures/GAPDiscord}
	\caption{Four discords of different length in the GAP dataset. Each discord (red subsequence) is coupled with its nearest neighbor (green subsequence). \textit{(a)} The discord, with the highest length-normalized distance to its nearest neighbor has length $274$. \textit{(b)} Discord with the second highest length-normalized distance. \textit{(c)},\textit{(d)} discords with a smaller length-normalized distance to their nearest neighbor.}
	\label{GAPDiscord}
\end{figure}


\noindent{\bf Select the most promising length in Discord Discovery.}
In this part, we show the results of discord discovery performed in the GAP dataset.
We recall that in this case, the discords ranking performed according to their length normalized distances aims to favor smaller discords, which have a high point to point distance. 


In Figure~\ref{GAPDiscord}, we report some of the discords we found in the length range $\ell_{min}=256$ and $\ell_{max}=4096$.
The discord with the highest length-normalized distance, \emph{best} $Top$-$1$ $1^{st}$ discord, is the one depicted in the top-left part of the figure, and has length $274$.
We plot it in red (dark), whereas its nearest neighbor appears in green (light).   
%Note, that this latter is the \emph{best} $Top$-$1$ $1^{st}$ discord of length $274$, which is stored in the $dkm_{\ell_{min},\ell_{max}}$ ranking in Algorithm~\ref{computeDkmAlllength}.
We note that this discord drastically differs from its nearest neighbor: it represents a fluctuating cycle of global power activity, while its nearest neighbor exhibits the expected behavior of two major peaks, in the morning and around noon.
In Figure~\ref{GAPDiscord}(b) we report the $Top$-$2$ $1^{st}$ discord in the length range $256$-$4096$ identified by MAD, which corresponds to the subsequence in that length range with the second highest length-normalized distance to its nearest neighbor. 
Once again, we observe a high degree of dissimilarity between the pattern of this discord and its nearest neighbor.
On the contrary, Figures~\ref{GAPDiscord}(c) and (d)
report the $Top$-$1$ $1^{st}$ discords for two specific lengths (i.e., 280 and 305, respectively). 
These discords correspond to patterns that are not significantly different from their nearest neighbors. 
Therefore, they represent discords that are less interesting than the ones reported by MAD in Figures~\ref{GAPDiscord}(a) and (b), which examines a large range of lengths.

This experiment demonstrates that MAD and the proposed discord ranking allows us to prioritize and select the correct discord length.




\section{Conclusions}
\label{sec:conclusions}

Motif and discord discovery are important problems in data series processing across several domains, and key operations necessary for several analysis tasks.
Even though much effort has been dedicated to these problems, no solution had been proposed for discovering motifs and discords of different lengths.

In this chapter, we propose the first framework for variable-length motif and discord discovery. 
We describe a new distance normalization method, as well as a novel distance lower bounding technique, both of which are necessary for the solution to our problem. 
We experimentally evaluated our algorithm by using five real datasets from diverse domains. 
The results demonstrate the efficiency and scalability of our approach (up to 20x faster than the state of the art), as well as its usefulness.















