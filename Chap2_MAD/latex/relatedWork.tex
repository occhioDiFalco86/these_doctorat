\section{Related Work}
\label{sec:related}


While research on data series similarity measures and data series query-by-content date back to the early 1990s~\cite{DBLP:conf/sofsem/Palpanas16}, \textit{data series motifs} and \textit{data series discords} were both introduced just fifteen and twelve years ago, respectively~\cite{DBLP:conf/kdd/ChiuKL03,DBLP:conf/adma/FuLKL06}.
Following their definition, there was an explosion of interest in their use for diverse applications. 
Analogies between \textit{data series motifs} and sequence \textit{motifs} exist (in DNA), and have been exploited. 
For example, discriminative motifs in bioinformatics~\cite{DBLP:conf/recomb/Sinha02} inspired discriminative data series motifs (i.e., data series shapelets)~\cite{citrusProduction}. 
Likewise, the work of Grabocka et al.~\cite{DBLP:journals/tkdd/GrabockaSS16} on generating idealized motifs is similar to the idea of consensus sequence (or canonical sequence) in molecular biology.
The literature on the general data series motif search is vast; we refer the reader to recent studies~\cite{ZhuZSYFMBK16,YehZUBDDSMK16} and their references.

The QUICK MOTIF~\cite{DBLP:conf/icde/LiUYG15} and STOMP~\cite{ZhuZSYFMBK16} algorithms represent the state of the art for fixed-length motif pair discovery. 
QUICK MOTIF first builds a summarized representation of the data using Piecewise Aggregate Approximation (PAA), and arranges these summaries in Minimum Bounding Rectangles (MBRs) in a Hilbert R-Tree index.
The algorithm then prunes the search space based on the MBRs.
On the other hand, STOMP is based on the computation of the matrix profile, 
%with a time cost of $O(n^2)$, 
in order to discover the best matches for each subsequence. 
The smallest of these matches is the motif pair. 
We observe that both of the above approaches solve a restricted version of our problem: they discover motif sets of cardinality two (i.e., motif pairs) of a fixed, predefined length. 
On the contrary, VALMOD removes these limitations and proposes a general and efficient solution.
Its main contributions are the novel algorithm for examining candidates of various lengths and corresponding lower bounding distance: these techniques help to reuse the computations performed so far, and lead to effective pruning of the vast search space.

We note that there are only three studies that deal with issues of variable length motifs, and attempt to address them~\cite{MinnenIES07,Gao0R16,YingchareonthawornchaiSRR13,DBLP:journals/datamine/GaoL18}. 
While these studies are pioneers in demonstrating the \textit{utility} of variable length motifs, they cannot serve as practical solutions to the task at hand for two reasons: 
(i) they are all approximate, while we need to produce exact results; and (ii) they require setting many parameters (most of which are unintuitive).
Approximate algorithms can be very useful in many contexts, if the amount of error can be bounded, or at least known. 
However, this is not the case for the algorithms in question. 
%While they have shown reasonable results on small, smooth datasets, it is not clear how they would fare in much longer, and much noisier datasets such as the seismological dataset we consider in Section~\ref{sec:seismology}. 
Certain cases, such as when analyzing seismological data, the threat of litigation, or even criminal proceedings~\cite{Bertolaso}, would make any analyst reluctant to use an approximate algorithm.

The other work that explicitly considers variable length motifs is MOEN~\cite{DBLP:journals/kais/MueenC15}. 
Its operation is based on the distance computation of subsequences of increasing length, and a corresponding pruning strategy based on upper and lower bounds of the distance computed for the smaller length subsequences.
Unlike the algorithms discussed above, MOEN is exact and requires few parameters. 
%However, as we shall see in our experimental section, MOEN is not competitive in time-performance with our proposed approach.
% (in fairness, our proposed approach leverages the Matrix Profile, of which the author of~\cite{DBLP:journals/kais/MueenC15} was an inventor).
However, it has been tuned for producing only a single motif pair for each length in the range, and as our evaluation showed, it is not competitive in terms of time-performance with our approach.
This is due to its relatively loose lower bound and sub-optimal search space pruning strategy, which force the algorithm to perform more work than necessary.

Exact discord discovery is a problem that has attracted lots of attention. 
The approaches that have been proposed in the literature can be divided in the following two different categories.
First, the \textit{index-based} solutions, i.e., Haar wavelets~\cite{DBLP:conf/adma/FuLKL06,DBLP:conf/sdm/BuLFKPM07} and SAX~\cite{Keogh2005,Keogh2007,DBLP:conf/edbt/Senin0WOGBCF15}, where series are first discretized and then inserted in an index structure that supports fast similarity search. 
Second, the \textit{sequential scan} solutions~\cite{YehZUBDDSMK16,Liu2009,DBLP:conf/adma/FuLKL06,Parameter-Free_Discord,DBLP:conf/icdm/YankovKR07,ZhuZSYFMBK16}, which consider the direct subsequence pairwise distance computations, and the corresponding search space optimization.

Indexing techniques are based on the discretization of the real valued data series, with several user defined parameters required for this operation. 
In general, selecting and tuning these parameters is not trivial, and the choices made may influence the behavior of the discord discovery algorithm, since it is strictly dependent on the quality of the data representation. 
In this regard, the most recent work in this category, GrammarViz~\cite{DBLP:conf/edbt/Senin0WOGBCF15}, proposes a method of \topkfirstd discord search based on grammar compression of data series represented by discrete SAX coefficients.
These representations are then inserted in a hierarchical structure, which permits us to prune unpromising candidate subsequences.
%{\bf ??? grammarviz is indexed-based? does it use an index structure? ???}
The intuition is that rare patterns are assigned to representations that have high \textit{Kolmogorov complexity}. This means that a rare SAX string is not compressible, due to the lack of repeated terms.
%In this work the search is performed, considering a hierarchical structure, which allows to firstly visit the most rare sequences, performing thus the pruning of the sequences that are well repeated in the dataset, having higher probability not to correspond to any discords.

The state of the art for the sequential scan methods is represented by STOMP, since computing the matrix profile permits to discover, in the same fashion as motifs, the \topkfirstd discords. 
Surprisingly, just one work exists that addresses the problem of $m^{th}$ discord discovery~\cite{DBLP:conf/icdm/YankovKR07}. 
The authors of this work, proposed the Disk Aware discords Discovery algorithm (DAD), which is based on a smart sequential scan performed on disk resident data.
This algorithm is divided into two parts. 
The first is discord candidate selection, where it identifies the sequences, whose nearest neighbor distance is less than a predefined range. 
%A technique to estimate an optimal range is also proposed in the paper. 
%{\bf ??? what does that mean? explain with 1-2 sentences ???}
The second part, which is called refinement, is applied in order to find the exact discords among the candidates. 
%To prune the Euclidean distance computations an early abandoning technique is used.
Despite the good performance that this algorithm exhibits in finding the first discord, when $m$ is greater than one, %and thus we are interested in finding multiple discords, 
it becomes hard to estimate an effective range.
%, which in general becomes large. This, will not permit to scale, 
In turn, this leads to scalability problems, due to the explosion of the number of distances to compute.

%{\bf ??? where are all the rest discord papers (the ones we reference in norma)? ???}

In summary, while there exists a large and growing body of work 
%that both uses motif discovery in diverse domains, or offers enhancements/generalizations of basic motif search, 
on the motif and discord discovery problems, 
this work offers the first scalable, parameter-light, \textit{exact} variable-length algorithm in the literature for solving both these problems. 







